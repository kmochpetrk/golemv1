package com.golemv1.golemv1;

import com.golemv1.golemv1.daos.*;
import com.golemv1.golemv1.entities.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.Assert;

import javax.accessibility.AccessibleRole;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Golemv1Application.class)
@ActiveProfiles("test")
@Transactional
class Golemv1ApplicationTests {

    @Autowired
    private AccUnitDao accUnitDao;

    @Autowired
    private GlAccountDao glAccountDao;

    @Autowired
    private AccRoleDao accRoleDao;

    @Autowired
    private AccUsersDao accUsersDao;

    @Autowired
    private GlWholeWritingDao glWholeWritingDao;

    @Autowired
    private GlWriteDao glWriteDao;

    @Autowired
    private WarehouseGoodsServicesDao warehouseGoodsServicesDao;

    @Autowired
    private WarehouseDao warehouseDao;

    @Autowired
    private GoodsServicesDao goodsServicesDao;

    @Test
    public void contextLoads() {
        AccountingUnit accountingUnit = new AccountingUnit();
        accountingUnit.setDic("123456789");
        accountingUnit.setIco("789456123");
        accountingUnit.setName("GolemV1");
        AccountingUnit saveAccUnit = accUnitDao.save(accountingUnit);
        Assert.isTrue(accUnitDao.count() == 1L, "Chyba poctu acc units");
        GlAccount glAccount = new GlAccount();
        glAccount.setDescription("Bank account");
        glAccount.setGlAccountType(GlAccountType.BALANCE);
        glAccount.setGlAccountSideType(GlAccountSideType.DEBIT);
        glAccount.setAcctNo("221100");
        glAccount.setAccountingUnit(saveAccUnit);
        glAccountDao.save(glAccount);
        Assert.isTrue(glAccountDao.count() == 1L, "Chyba poctu glAccounts units");
        GlAccount next = glAccountDao.findAll().iterator().next();
        Assert.isTrue(next.getAcctNo().equals("221100"), "Equal acctno not true");
        Assert.isTrue(next.getAccountingUnit().getIco().equals("789456123"), "Equal ico not true");
    }

    @Test
    public void testUsersRoles() {
        AccountingUnit accountingUnit = new AccountingUnit();
        accountingUnit.setDic("123456789");
        accountingUnit.setIco("789456123");
        accountingUnit.setName("GolemV1");
        AccountingUnit saveAccUnit = accUnitDao.save(accountingUnit);

        AccRoles accRoles1 = new AccRoles();
        accRoles1.setRoleName("Main_Accountant");
        AccRoles save1 = accRoleDao.save(accRoles1);

        AccRoles accRoles2 = new AccRoles();
        accRoles2.setRoleName("Invoice_Processor");
        AccRoles save2 = accRoleDao.save(accRoles2);

        AccUsers accUsers = new AccUsers();
        accUsers.setFirstName("Petr");
        accUsers.setAccountingUnit(saveAccUnit);
        accUsers.setSurname("Kmoch");
        accUsers.setRoles(List.of(save1, save2));

        AccUsers save = accUsersDao.save(accUsers);
        List<AccRoles> roles = accUsersDao.findAll().iterator().next().getRoles();
        int size = roles.size();
        Assert.isTrue(size == 2, "Error size roles");
        Assert.isTrue(roles.stream().anyMatch(role -> role.getRoleName().equals("Main_Accountant")), "Error role main accountant");
    }


    @Test
    public void testAccounts() {
        AccountingUnit accountingUnit = new AccountingUnit();
        accountingUnit.setDic("123456789");
        accountingUnit.setIco("789456123");
        accountingUnit.setName("GolemV1");
        AccountingUnit saveAccUnit = accUnitDao.save(accountingUnit);
        Assert.isTrue(accUnitDao.count() == 1L, "Chyba poctu acc units");
        GlAccount glAccount = new GlAccount();
        glAccount.setDescription("Bank account");
        glAccount.setGlAccountType(GlAccountType.BALANCE);
        glAccount.setGlAccountSideType(GlAccountSideType.DEBIT);
        glAccount.setAcctNo("221100");
        glAccount.setAccountingUnit(saveAccUnit);
        GlAccount save221 = glAccountDao.save(glAccount);
        Assert.isTrue(glAccountDao.count() == 1L, "Chyba poctu glAccounts units");
        GlAccount next = glAccountDao.findAll().iterator().next();
        Assert.isTrue(next.getAcctNo().equals("221100"), "Equal acctno not true");
        Assert.isTrue(next.getAccountingUnit().getIco().equals("789456123"), "Equal ico not true");

        GlAccount glAccount1 = new GlAccount();
        glAccount1.setDescription("income for good");
        glAccount1.setGlAccountType(GlAccountType.PROFIT_LOSS);
        glAccount1.setGlAccountSideType(GlAccountSideType.CREDIT);
        glAccount1.setAcctNo("601100");
        glAccount1.setAccountingUnit(saveAccUnit);
        GlAccount save601 = glAccountDao.save(glAccount1);


        AccRoles accRoles1 = new AccRoles();
        accRoles1.setRoleName("Main_Accountant");
        AccRoles save1 = accRoleDao.save(accRoles1);

        AccRoles accRoles2 = new AccRoles();
        accRoles2.setRoleName("Invoice_Processor");
        AccRoles save2 = accRoleDao.save(accRoles2);

        AccUsers accUsers = new AccUsers();
        accUsers.setFirstName("Petr");
        accUsers.setAccountingUnit(saveAccUnit);
        accUsers.setSurname("Kmoch");
        accUsers.setRoles(List.of(save1, save2));

        AccUsers save = accUsersDao.save(accUsers);

        GlWholeWrite glWholeWrite = new BankStatement();
        glWholeWrite.setAccountingUnit(accountingUnit);
        glWholeWrite.setAccountingDate(LocalDate.now());
        glWholeWrite.setTaxDate(LocalDate.now());
        glWholeWrite.setWrittenBy(accUsers);
        //glWholeWrite.setGlWholeWriteType(GlWholeWriteType.BANK_STATEMENT);
        GlWholeWrite saveGlWholeWrite = glWholeWritingDao.save(glWholeWrite);

        GlWrite glWrite = new GlWrite();
        glWrite.setGlAccountSideType(GlAccountSideType.DEBIT);
        glWrite.setGlAccount(save221);
        glWrite.setAmount(BigDecimal.valueOf(1000));
        glWrite.setGlWholeWrite(saveGlWholeWrite);
        glWriteDao.save(glWrite);

        GlWrite glWrite2 = new GlWrite();
        glWrite2.setGlAccountSideType(GlAccountSideType.CREDIT);
        glWrite2.setGlAccount(save601);
        glWrite2.setAmount(BigDecimal.valueOf(1000));
        glWrite2.setGlWholeWrite(saveGlWholeWrite);
        glWriteDao.save(glWrite2);

        glWholeWritingDao.detach(glWholeWrite);

        Iterable<GlWholeWrite> all = glWholeWritingDao.findAll();
        Optional<GlWholeWrite> byId = glWholeWritingDao.findById(saveGlWholeWrite.getId());
        Assert.isTrue(byId.get().getGlWrites().size() == 2, "Error size glWrites");
    }

    @Test
    public void testGlWriteWholeBankStatement() {
        AccountingUnit accountingUnit = new AccountingUnit();
        accountingUnit.setDic("123456789");
        accountingUnit.setIco("789456123");
        accountingUnit.setName("GolemV1");
        AccountingUnit saveAccUnit = accUnitDao.save(accountingUnit);
        Assert.isTrue(accUnitDao.count() == 1L, "Chyba poctu acc units");
        GlAccount glAccount = new GlAccount();
        glAccount.setDescription("Bank account");
        glAccount.setGlAccountType(GlAccountType.BALANCE);
        glAccount.setGlAccountSideType(GlAccountSideType.DEBIT);
        glAccount.setAcctNo("221100");
        glAccount.setAccountingUnit(saveAccUnit);
        GlAccount save221 = glAccountDao.save(glAccount);
        Assert.isTrue(glAccountDao.count() == 1L, "Chyba poctu glAccounts units");
        GlAccount next = glAccountDao.findAll().iterator().next();
        Assert.isTrue(next.getAcctNo().equals("221100"), "Equal acctno not true");
        Assert.isTrue(next.getAccountingUnit().getIco().equals("789456123"), "Equal ico not true");

        GlAccount glAccount1 = new GlAccount();
        glAccount1.setDescription("income for good");
        glAccount1.setGlAccountType(GlAccountType.PROFIT_LOSS);
        glAccount1.setGlAccountSideType(GlAccountSideType.CREDIT);
        glAccount1.setAcctNo("601100");
        glAccount1.setAccountingUnit(saveAccUnit);
        GlAccount save601 = glAccountDao.save(glAccount1);


        AccRoles accRoles1 = new AccRoles();
        accRoles1.setRoleName("Main_Accountant");
        AccRoles save1 = accRoleDao.save(accRoles1);

        AccRoles accRoles2 = new AccRoles();
        accRoles2.setRoleName("Invoice_Processor");
        AccRoles save2 = accRoleDao.save(accRoles2);

        AccUsers accUsers = new AccUsers();
        accUsers.setFirstName("Petr");
        accUsers.setAccountingUnit(saveAccUnit);
        accUsers.setSurname("Kmoch");
        accUsers.setRoles(List.of(save1, save2));

        AccUsers saveUser = accUsersDao.save(accUsers);

        GlWholeWrite glWholeWrite = new BankStatement();
        glWholeWrite.setAccountingUnit(accountingUnit);
        glWholeWrite.setAccountingDate(LocalDate.now());
        glWholeWrite.setTaxDate(LocalDate.now());
        glWholeWrite.setWrittenBy(saveUser);
        //glWholeWrite.setGlWholeWriteType(GlWholeWriteType.BANK_STATEMENT);
        //GlWholeWrite saveGlWholeWrite = glWholeWritingDao.save(glWholeWrite);

        GlWrite glWrite = new GlWrite();
        glWrite.setGlAccountSideType(GlAccountSideType.DEBIT);
        glWrite.setGlAccount(save221);
        glWrite.setAmount(BigDecimal.valueOf(1000));
        glWrite.setGlWholeWrite(glWholeWrite);
        //glWriteDao.save(glWrite);

        GlWrite glWrite2 = new GlWrite();
        glWrite2.setGlAccountSideType(GlAccountSideType.CREDIT);
        glWrite2.setGlAccount(save601);
        glWrite2.setAmount(BigDecimal.valueOf(1000));
        glWrite2.setGlWholeWrite(glWholeWrite);
        //glWriteDao.save(glWrite2);

        //glWholeWritingDao.detach(glWholeWrite);

        glWholeWrite.setGlWrites(Set.of(glWrite, glWrite2));
        GlWholeWrite saveGlWholeWrite2 = glWholeWritingDao.save(glWholeWrite);

        glWholeWritingDao.detach(saveGlWholeWrite2);
        glWriteDao.detach(glWrite);
        glWriteDao.detach(glWrite2);

        Iterable<GlWholeWrite> all = glWholeWritingDao.findAll();
        Optional<GlWholeWrite> byId = glWholeWritingDao.findById(saveGlWholeWrite2.getId());
        Assert.isTrue(byId.get().getGlWrites().size() == 2, "Error size glWrites");
    }

    @Test
    public void testGlWriteWholeOutputInvoice() {
        AccountingUnit accountingUnit = new AccountingUnit();
        accountingUnit.setDic("123456789");
        accountingUnit.setIco("789456123");
        accountingUnit.setName("GolemV1");
        AccountingUnit saveAccUnit = accUnitDao.save(accountingUnit);
        Assert.isTrue(accUnitDao.count() == 1L, "Chyba poctu acc units");
        GlAccount glAccount = new GlAccount();
        glAccount.setDescription("Bank account");
        glAccount.setGlAccountType(GlAccountType.BALANCE);
        glAccount.setGlAccountSideType(GlAccountSideType.DEBIT);
        glAccount.setAcctNo("221100");
        glAccount.setAccountingUnit(saveAccUnit);
        GlAccount save221 = glAccountDao.save(glAccount);
        Assert.isTrue(glAccountDao.count() == 1L, "Chyba poctu glAccounts units");
        GlAccount next = glAccountDao.findAll().iterator().next();
        Assert.isTrue(next.getAcctNo().equals("221100"), "Equal acctno not true");
        Assert.isTrue(next.getAccountingUnit().getIco().equals("789456123"), "Equal ico not true");

        GlAccount glAccount1 = new GlAccount();
        glAccount1.setDescription("income for good");
        glAccount1.setGlAccountType(GlAccountType.PROFIT_LOSS);
        glAccount1.setGlAccountSideType(GlAccountSideType.CREDIT);
        glAccount1.setAcctNo("601100");
        glAccount1.setAccountingUnit(saveAccUnit);
        GlAccount save601 = glAccountDao.save(glAccount1);

        GlAccount glAccountGoods = new GlAccount();
        glAccountGoods.setDescription("goods_stocks_rohliky");
        glAccountGoods.setGlAccountType(GlAccountType.BALANCE);
        glAccountGoods.setGlAccountSideType(GlAccountSideType.DEBIT);
        glAccountGoods.setAcctNo("132100");
        glAccountGoods.setAccountingUnit(saveAccUnit);
        GlAccount save132 = glAccountDao.save(glAccountGoods);

        GlAccount glAccountVat = new GlAccount();
        glAccountVat.setDescription("vat");
        glAccountVat.setGlAccountType(GlAccountType.BALANCE);
        glAccountVat.setGlAccountSideType(GlAccountSideType.CREDIT);
        glAccountVat.setAcctNo("343100");
        glAccountVat.setAccountingUnit(saveAccUnit);
        GlAccount save343 = glAccountDao.save(glAccountVat);

        GlAccount glAccountCostsGoods = new GlAccount();
        glAccountCostsGoods.setDescription("goods_stocks_output");
        glAccountCostsGoods.setGlAccountType(GlAccountType.PROFIT_LOSS);
        glAccountCostsGoods.setGlAccountSideType(GlAccountSideType.DEBIT);
        glAccountCostsGoods.setAcctNo("504100");
        glAccountCostsGoods.setAccountingUnit(saveAccUnit);
        GlAccount save504 = glAccountDao.save(glAccountCostsGoods);


        AccRoles accRoles1 = new AccRoles();
        accRoles1.setRoleName("Main_Accountant");
        AccRoles save1 = accRoleDao.save(accRoles1);

        AccRoles accRoles2 = new AccRoles();
        accRoles2.setRoleName("Invoice_Processor");
        AccRoles save2 = accRoleDao.save(accRoles2);

        AccUsers accUsers = new AccUsers();
        accUsers.setFirstName("Petr");
        accUsers.setAccountingUnit(saveAccUnit);
        accUsers.setSurname("Kmoch");
        accUsers.setRoles(List.of(save1, save2));

        AccUsers saveUser = accUsersDao.save(accUsers);

        GoodsServices rohliky = new GoodsServices();
        rohliky.setName("rohliky");
        rohliky.setAccountingUnit(accountingUnit);
        rohliky.setNumberOfUnitsInPacket(1L);
        rohliky.setPacketInputPrice(BigDecimal.valueOf(1.4));
        rohliky.setPacketOutputPrice(BigDecimal.valueOf(1.8));
        rohliky.setVatPercents(BigDecimal.valueOf(19));
        GoodsServices savedRohliky = goodsServicesDao.save(rohliky);

        GoodsServices klempirskePrace = new GoodsServices();
        klempirskePrace.setName("klempirske_prace");
        klempirskePrace.setAccountingUnit(accountingUnit);
        klempirskePrace.setNumberOfUnitsInPacket(1L);
        klempirskePrace.setPacketInputPrice(BigDecimal.valueOf(200));
        klempirskePrace.setPacketOutputPrice(BigDecimal.valueOf(240));
        klempirskePrace.setVatPercents(BigDecimal.valueOf(19));
        GoodsServices savedKlempirskePrace = goodsServicesDao.save(klempirskePrace);

        Warehouse warehouse = new Warehouse();
        warehouse.setAccountingUnit(accountingUnit);
        warehouse.setName("Sklad Hluckova");
        Warehouse savedWarehouse = warehouseDao.save(warehouse);

        //naskladneni start
        WarehouseGoodsServices warehouseGoodsServicesRohliky = new WarehouseGoodsServices();
        warehouseGoodsServicesRohliky.setGoodsServices(savedRohliky);
        warehouseGoodsServicesRohliky.setWarehouse(savedWarehouse);
        final GoodsInputItem goodsInputItem1 = new GoodsInputItem();
        goodsInputItem1.setInputPrice(BigDecimal.valueOf(20));
        goodsInputItem1.setNumberOfPackets(10000L);
        warehouseGoodsServicesRohliky.setGoodsInputs(Set.of(goodsInputItem1));
        WarehouseGoodsServices savedWareGoodsSerRohliky = warehouseGoodsServicesDao.save(warehouseGoodsServicesRohliky);

        WarehouseGoodsServices warehouseGoodsServicesKlempirskePrace = new WarehouseGoodsServices();
        warehouseGoodsServicesKlempirskePrace.setGoodsServices(savedRohliky);
        warehouseGoodsServicesKlempirskePrace.setWarehouse(savedWarehouse);
        final GoodsInputItem goodsInputItem = new GoodsInputItem();
        goodsInputItem.setInputPrice(BigDecimal.valueOf(20));
        goodsInputItem.setNumberOfPackets(Long.valueOf(24*5*22));
        warehouseGoodsServicesKlempirskePrace.setGoodsInputs(Set.of(goodsInputItem));
        WarehouseGoodsServices savedWareGoodsSerKlempirPrace = warehouseGoodsServicesDao.save(warehouseGoodsServicesKlempirskePrace);
        //naskladneni end
        //vyskladneni
        //prodej 10 rohliku na fakturu

        GlWholeWrite glWholeWrite = new OutputInvoice();
        glWholeWrite.setAccountingUnit(accountingUnit);
        glWholeWrite.setAccountingDate(LocalDate.now());
        glWholeWrite.setTaxDate(LocalDate.now());
        glWholeWrite.setWrittenBy(saveUser);
        //glWholeWrite.setGlWholeWriteType(GlWholeWriteType.BANK_STATEMENT);
        //GlWholeWrite saveGlWholeWrite = glWholeWritingDao.save(glWholeWrite);

        GoodsServiceItems goodsServiceItems = new GoodsServiceItems();
        glWholeWrite.setGoodsServiceItems(Set.of(goodsServiceItems));
        goodsServiceItems.setNumberOfUnits(10L);
        goodsServiceItems.setPriceTotal(savedRohliky.getPacketOutputPrice().multiply(BigDecimal.valueOf(10L))); //pocet 10L
        goodsServiceItems.setUnitPrice(savedRohliky.getPacketOutputPrice());
        goodsServiceItems.setVatPercent(savedRohliky.getVatPercents());
        goodsServiceItems.setVatTotal(savedRohliky.getVatPercents().multiply(BigDecimal.valueOf(0.01))
                .multiply(BigDecimal.valueOf(10L)).multiply(savedRohliky.getPacketOutputPrice()));
        goodsServiceItems.setWarehouseGoodsServices(savedWareGoodsSerRohliky);
        goodsServiceItems.setGlWholeWrite(glWholeWrite);

        GlWrite glWrite = new GlWrite();
        glWrite.setGlAccountSideType(GlAccountSideType.DEBIT);
        glWrite.setGlAccount(save221);
        glWrite.setAmount(goodsServiceItems.getPriceTotal().add(goodsServiceItems.getVatTotal()));
        glWrite.setGlWholeWrite(glWholeWrite);
        //glWriteDao.save(glWrite);

        GlWrite glWrite2 = new GlWrite();
        glWrite2.setGlAccountSideType(GlAccountSideType.CREDIT);
        glWrite2.setGlAccount(save601);
        glWrite2.setAmount(goodsServiceItems.getPriceTotal());
        glWrite2.setGlWholeWrite(glWholeWrite);

        GlWrite glWrite3 = new GlWrite();
        glWrite3.setGlAccountSideType(GlAccountSideType.CREDIT);
        glWrite3.setGlAccount(save343);
        glWrite3.setAmount(goodsServiceItems.getVatTotal());
        glWrite3.setGlWholeWrite(glWholeWrite);


        //glWholeWritingDao.detach(glWholeWrite);

        glWholeWrite.setGlWrites(Set.of(glWrite, glWrite2, glWrite3));
        GlWholeWrite saveGlWholeWrite2 = glWholeWritingDao.save(glWholeWrite);

        glWholeWritingDao.detach(saveGlWholeWrite2);
        glWriteDao.detach(glWrite);
        glWriteDao.detach(glWrite2);
        glWriteDao.detach(glWrite3);

        Iterable<GlWholeWrite> all = glWholeWritingDao.findAll();
        Optional<GlWholeWrite> byId = glWholeWritingDao.findById(saveGlWholeWrite2.getId());
        Assert.isTrue(byId.get().getGlWrites().size() == 3, "Error size glWrites");
    }

}
