package com.golemv1.golemv1.mappers;

import com.golemv1.golemv1.daos.*;
import com.golemv1.golemv1.dtos.GlWriteDto;
import com.golemv1.golemv1.dtos.WarehouseGoodsDto;
import com.golemv1.golemv1.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Component
@Transactional
public class MainMapper {

    @Autowired
    private GlAccountDao glAccountDao;

    @Autowired
    private WarehouseGoodsServicesDao warehouseGoodsServicesDao;

    @Autowired
    private WarehouseDao warehouseDao;

    @Autowired
    private GoodsServicesDao goodsServicesDao;

    @Autowired
    private GoodsInputItemDao goodsInputItemDao;

    @Autowired
    private GoodsOutputItemDao goodsOutputItemDao;

    public GlWrite mapGlWriteDto(GlWriteDto glWriteDto) {
        GlWrite glWrite = new GlWrite();
        glWrite.setAmount(glWriteDto.getAmount());
        glWrite.setGlAccount(glAccountDao.findById(glWriteDto.getGlAccountId()).get());
        glWrite.setAnalyticPart(glWriteDto.getAnalyticPart());
        glWrite.setGlAccountSideType(glWriteDto.getGlAccountSideType());
        return glWrite;
    }

    public static Set<GlWriteDto> remapGlWrites(Set<GlWrite> glWrites) {
        Set<GlWriteDto> returnSet = new HashSet<>();
        for (GlWrite glWrite : glWrites) {
            returnSet.add(remapGlWrite(glWrite));
        }
        return returnSet;
    }

    public static GlWriteDto remapGlWrite(GlWrite glWrite) {
        GlWriteDto glWriteDto = new GlWriteDto();
        glWriteDto.setAmount(glWrite.getAmount());
        glWriteDto.setGlAccountId(glWrite.getId());
        glWriteDto.setAcctNo(glWrite.getGlAccount().getAcctNo());
        glWriteDto.setAnalyticPart(glWrite.getAnalyticPart());
        glWriteDto.setGlAccountSideType(glWrite.getGlAccountSideType());
        return glWriteDto;
    }

    public Set<GlWrite> mapGlWrites(List<GlWriteDto> glWrites) {
        Set<GlWrite> returnList = new HashSet<>();
        for (GlWriteDto glWriteDto : glWrites) {
            returnList.add(mapGlWriteDto(glWriteDto));
        }
        return returnList;
    }

    public Set<GoodsServiceItems> mapGoodsServiceItems(List<WarehouseGoodsDto> warehouseGoodsDtos, boolean input, boolean createItem) {
        Set<GoodsServiceItems> returnSet = new HashSet<>();
        for (WarehouseGoodsDto warehouseGoodsDto : warehouseGoodsDtos) {
            returnSet.add(mapWarehouseDto(warehouseGoodsDto, input, warehouseGoodsDto.getWarehouseId() != null));
        }
        return returnSet;
    }

    public GoodsServiceItems mapWarehouseDto(WarehouseGoodsDto warehouseGoodsDto, boolean input, boolean createItem) {
        GoodsServiceItems goodsServiceItems = new GoodsServiceItems();

        GoodsServices goodsServices = null;
        //input - warehouseGoodsDto.getGoodsServiceId() is goodsService, input false warehouseGoodsService
        if (input) {
            goodsServices = goodsServicesDao.findById(warehouseGoodsDto.getGoodsServiceId()).get();
        } else {
            WarehouseGoodsServices wgs = warehouseGoodsServicesDao.findById(warehouseGoodsDto.getGoodsServiceId()).get();
            goodsServices = wgs.getGoodsServices();

        }
        if (createItem) {
            Warehouse warehouse = warehouseDao.findById(warehouseGoodsDto.getWarehouseId()).get();
            WarehouseGoodsServices byWarehouseAndAndGoodsServices =
                    warehouseGoodsServicesDao.findByWarehouseAndGoodsServices(warehouse, goodsServices);

            if (byWarehouseAndAndGoodsServices == null) {
                byWarehouseAndAndGoodsServices = new WarehouseGoodsServices();
                byWarehouseAndAndGoodsServices.setCount(warehouseGoodsDto.getCount());
                byWarehouseAndAndGoodsServices.setWarehouse(warehouse);
                byWarehouseAndAndGoodsServices.setGoodsServices(goodsServices);
                byWarehouseAndAndGoodsServices = warehouseGoodsServicesDao.save(byWarehouseAndAndGoodsServices);
            }

            if (input) {
                GoodsInputItem goodsInputItem = new GoodsInputItem();
                goodsInputItem.setInputPrice(warehouseGoodsDto.getUnitPrice());
                goodsInputItem.setTimeIn(LocalDateTime.now());
                goodsInputItem.setNumberOfPackets(warehouseGoodsDto.getCount());
                goodsInputItem.setWarehouseGoodsServices(byWarehouseAndAndGoodsServices);
                fillGsItem(goodsServiceItems, warehouseGoodsDto);
                goodsInputItemDao.save(goodsInputItem);
            } else {
                GoodsOutputItem goodsOutputItem = new GoodsOutputItem();
                goodsOutputItem.setOutputPrice(warehouseGoodsDto.getUnitPrice());
                goodsOutputItem.setTimeIn(LocalDateTime.now());
                goodsOutputItem.setNumberOfPackets(warehouseGoodsDto.getCount());
                goodsOutputItem.setWarehouseGoodsServices(byWarehouseAndAndGoodsServices);
                fillGsItem(goodsServiceItems, warehouseGoodsDto);
                goodsOutputItemDao.save(goodsOutputItem);
            }
            goodsServiceItems.setWarehouseGoodsServices(byWarehouseAndAndGoodsServices);
        } else {
            fillGsItem(goodsServiceItems, warehouseGoodsDto);
            goodsServiceItems.setGoodsServices(goodsServices);
        }
        return goodsServiceItems;
    }

    private void fillGsItem(GoodsServiceItems goodsServiceItems, WarehouseGoodsDto warehouseGoodsDto) {
        goodsServiceItems.setVatPercent(goodsServiceItems.getVatPercent());
        goodsServiceItems.setVatTotal(warehouseGoodsDto.getVatTotal());
        goodsServiceItems.setUnitPrice(warehouseGoodsDto.getUnitPrice());
        goodsServiceItems.setPriceTotal(warehouseGoodsDto.getPriceTotal());
        goodsServiceItems.setNumberOfUnits(warehouseGoodsDto.getCount());
    }
}
