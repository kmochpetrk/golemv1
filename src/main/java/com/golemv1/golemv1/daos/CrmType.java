package com.golemv1.golemv1.daos;

public enum CrmType {
    SUPPLIER,
    CUSTOMER,
    BOTH
}
