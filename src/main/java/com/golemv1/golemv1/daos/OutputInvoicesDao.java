package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.OutputInvoice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;

public interface OutputInvoicesDao extends PagingAndSortingRepository<OutputInvoice, Long> {
    Page<OutputInvoice> findAllByAccountingUnit(AccountingUnit accountingUnit, Pageable pageable);
    Page<OutputInvoice> findAllByAccountingUnitAndBsIdIsNull(AccountingUnit accountingUnit, Pageable pageable);
    Page<OutputInvoice> findAllByAccountingUnitAndBsIdIsNullAndPayDateIsBefore(AccountingUnit accountingUnit, LocalDate date, Pageable pageable);
}
