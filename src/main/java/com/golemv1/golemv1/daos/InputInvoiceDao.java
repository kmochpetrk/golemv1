package com.golemv1.golemv1.daos;


import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.InputInvoice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;

public interface InputInvoiceDao extends PagingAndSortingRepository<InputInvoice, Long> {
    Page<InputInvoice> findAllByAccountingUnit(AccountingUnit accountingUnit, Pageable pageable);
    Page<InputInvoice> findAllByAccountingUnitAndCommandToPayIsNull(AccountingUnit accountingUnit, Pageable pageable);
    Page<InputInvoice> findAllByAccountingUnitAndCommandToPayIsNotNullAndBsIdIsNull(AccountingUnit accountingUnit, Pageable pageable);
    Page<InputInvoice> findAllByAccountingUnitAndCommandToPayIsNullAndBsIdIsNullAndPayDateIsBefore(AccountingUnit accountingUnit, LocalDate date, Pageable pageable);
}
