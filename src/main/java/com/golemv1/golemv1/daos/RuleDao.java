package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.language.AccRule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RuleDao extends JpaRepository<AccRule, Long> {
}
