package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.GlWholeWrite;

public interface GlWholeWriteRepoCustom {
    void detach(GlWholeWrite glWholeWrite);
}
