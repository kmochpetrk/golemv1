package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.GoodsOutputItem;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GoodsOutputItemDao extends PagingAndSortingRepository<GoodsOutputItem, Long> {
}
