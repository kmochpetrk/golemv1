package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.GlWrite;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class GlWriteRepoCustomImpl implements GlWriteRepoCustom {

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public void detach(GlWrite glWrite) {
        entityManager.flush();
        entityManager.detach(glWrite);
    }
}
