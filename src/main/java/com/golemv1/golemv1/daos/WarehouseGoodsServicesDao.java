package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Set;

public interface WarehouseGoodsServicesDao extends PagingAndSortingRepository<WarehouseGoodsServices, Long> {
    WarehouseGoodsServices findByWarehouseAndGoodsServices(Warehouse warehouse, GoodsServices goodsServices);

    Set<WarehouseGoodsServices> findAllByGoodsServices_AccountingUnit(AccountingUnit accountingUnit);

    Page<WarehouseGoodsServices> findAllByWarehouse(Warehouse warehouseId, Pageable pageable);
}
