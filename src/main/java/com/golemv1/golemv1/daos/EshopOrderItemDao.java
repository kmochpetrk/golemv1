package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.EshopOrderItem;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EshopOrderItemDao extends PagingAndSortingRepository<EshopOrderItem, Long> {
}
