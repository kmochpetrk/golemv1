package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccUsers;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AccUsersDao extends PagingAndSortingRepository<AccUsers, Long> {
}
