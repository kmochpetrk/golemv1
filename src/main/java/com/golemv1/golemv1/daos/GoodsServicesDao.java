package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.GoodsServices;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GoodsServicesDao extends PagingAndSortingRepository<GoodsServices, Long> {
    Page<GoodsServices> findAllByAccountingUnit(AccountingUnit accountingUnit, Pageable pageable);
}
