package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.Crm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Set;

public interface CrmDao extends PagingAndSortingRepository<Crm, Long> {
    Page<Crm> findAllByAccountingUnit(AccountingUnit accountingUnit, Pageable pageable);
    Set<Crm> findAllByIcoEquals(String ico);
    Set<Crm> findAllByNameIgnoreCase(String name);
}
