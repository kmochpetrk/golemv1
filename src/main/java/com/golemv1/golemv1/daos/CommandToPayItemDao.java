package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.CommandToPayItem;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CommandToPayItemDao extends PagingAndSortingRepository<CommandToPayItem, Long> {
}
