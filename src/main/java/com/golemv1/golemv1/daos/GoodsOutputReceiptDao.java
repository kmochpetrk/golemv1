package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.GoodsInputReceipt;
import com.golemv1.golemv1.entities.GoodsOutputReceipt;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GoodsOutputReceiptDao extends PagingAndSortingRepository<GoodsOutputReceipt, Long> {
    Page<GoodsOutputReceipt> findAllByAccountingUnit(AccountingUnit accountingUnit, Pageable pageable);
    Page<GoodsOutputReceipt> findAllByAccountingUnitAndOutputInvoiceNullAndIcrIdNull(AccountingUnit accountingUnit, Pageable pageable);
}
