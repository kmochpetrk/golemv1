package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.InventoryItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface InventoryItemDao extends PagingAndSortingRepository<InventoryItem, Long> {
    Page<InventoryItem> findAllByAccountingUnit(AccountingUnit accountingUnit, Pageable pageable);
}
