package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.GlWholeWrite;
import com.golemv1.golemv1.entities.GlWholeWriteType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface GlWholeWritingDao extends PagingAndSortingRepository<GlWholeWrite, Long>, GlWholeWriteRepoCustom {
    Page<GlWholeWrite> findAllByAccountingUnitAndGlWholeWriteTypeOrderByAccountingDateDesc(AccountingUnit accountingUnit, GlWholeWriteType glWholeWriteType, Pageable pageable);

    Page<GlWholeWrite> findAllByAccountingUnitOrderByAccountingDateDesc(AccountingUnit accountingUnit, Pageable pageable);
    List<GlWholeWrite> findAllByAccountingUnitOrderByAccountingDateDesc(AccountingUnit accountingUnit);
}
