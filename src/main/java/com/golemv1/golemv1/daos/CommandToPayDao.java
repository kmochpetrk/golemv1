package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.BankStatement;
import com.golemv1.golemv1.entities.CommandToPay;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CommandToPayDao extends PagingAndSortingRepository<CommandToPay, Long> {
    Page<CommandToPay> findAllByAccountingUnit(AccountingUnit accountingUnit, Pageable pageable);
    Page<CommandToPay> findAllByAccountingUnitAndPayedByBsIsNull(AccountingUnit accountingUnit, Pageable pageable);
}
