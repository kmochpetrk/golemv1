package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.BankStatement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BankStatementDao extends PagingAndSortingRepository<BankStatement, Long> {
    Page<BankStatement> findAllByAccountingUnit(AccountingUnit accountingUnit, Pageable pageable);
}
