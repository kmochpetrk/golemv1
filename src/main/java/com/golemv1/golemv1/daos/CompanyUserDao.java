package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.CompanyUser;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CompanyUserDao extends PagingAndSortingRepository<CompanyUser, Long> {
    List<CompanyUser> findAllByCompanyAndUsername(String company, String user);
}
