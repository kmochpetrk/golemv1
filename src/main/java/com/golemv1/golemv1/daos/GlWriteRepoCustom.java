package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.GlWholeWrite;
import com.golemv1.golemv1.entities.GlWrite;

public interface GlWriteRepoCustom {
    void detach(GlWrite glWrite);
}
