package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.language.EnglishWord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EnglishWordDao extends JpaRepository<EnglishWord, Long> {

    List<EnglishWord> findAllByWord(String word);

}
