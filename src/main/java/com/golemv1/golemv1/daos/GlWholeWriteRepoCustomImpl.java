package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.GlWholeWrite;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class GlWholeWriteRepoCustomImpl implements GlWholeWriteRepoCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void detach(GlWholeWrite glWholeWrite) {
        entityManager.flush();
        entityManager.detach(glWholeWrite);
    }
}
