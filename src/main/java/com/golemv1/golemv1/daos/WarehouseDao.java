package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.Warehouse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WarehouseDao extends PagingAndSortingRepository<Warehouse, Long> {

    Page<Warehouse> findAllByAccountingUnit(AccountingUnit accountingUnit, Pageable pageable);

    Warehouse findTopByAccountingUnitAndAnalyticAccountNotNullOrderByAnalyticAccountDesc(AccountingUnit accountingUnit);
}

