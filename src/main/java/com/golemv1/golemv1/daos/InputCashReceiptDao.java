package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.InputCashReceipt;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface InputCashReceiptDao extends PagingAndSortingRepository<InputCashReceipt, Long> {
    Page<InputCashReceipt> findAllByAccountingUnit(AccountingUnit accountingUnit, Pageable pageable);
}
