package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.language.AccRuleElement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RuleElementDao extends JpaRepository<AccRuleElement, Long> {
}
