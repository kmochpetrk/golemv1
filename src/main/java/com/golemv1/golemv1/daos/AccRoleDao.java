package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccRoles;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AccRoleDao extends PagingAndSortingRepository<AccRoles, Long> {
}
