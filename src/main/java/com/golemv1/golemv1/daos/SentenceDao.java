package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.language.Sentence;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SentenceDao extends JpaRepository<Sentence, Long> {
}
