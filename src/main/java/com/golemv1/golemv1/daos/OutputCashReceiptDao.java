package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.OutputCashReceipt;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OutputCashReceiptDao extends PagingAndSortingRepository<OutputCashReceipt, Long> {
    Page<OutputCashReceipt> findAllByAccountingUnit(AccountingUnit accountingUnit, Pageable pageable);
}
