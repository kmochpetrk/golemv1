package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.InternalDocument;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface InternalDocumentDao extends PagingAndSortingRepository<InternalDocument, Long> {
}
