package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmployeeDao extends PagingAndSortingRepository<Employee, Long> {
    Page<Employee> findAllByAccountingUnit(AccountingUnit accountingUnit, Pageable pageable);
}
