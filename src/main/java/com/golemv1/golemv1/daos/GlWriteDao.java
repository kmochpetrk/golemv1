package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.GlWrite;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GlWriteDao extends PagingAndSortingRepository<GlWrite, Long>, GlWriteRepoCustom {
}
