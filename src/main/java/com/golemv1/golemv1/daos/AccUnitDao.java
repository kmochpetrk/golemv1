package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccountingUnit;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AccUnitDao extends PagingAndSortingRepository<AccountingUnit, Long> {

    AccountingUnit findByName(String name);
}
