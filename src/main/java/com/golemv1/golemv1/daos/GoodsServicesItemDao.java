package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.GoodsServiceItems;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GoodsServicesItemDao extends PagingAndSortingRepository<GoodsServiceItems, Long> {
}
