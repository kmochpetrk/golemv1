package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.EshopOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Set;

public interface EshopOrderDao  extends PagingAndSortingRepository<EshopOrder, Long> {
    Page<EshopOrder> findAllByCompany(String company, Pageable pageable);
}
