package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.GlAccount;
import com.golemv1.golemv1.entities.GlAccountGroupingType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Set;

public interface GlAccountDao extends PagingAndSortingRepository<GlAccount, Long> {
    Set<GlAccount> findAllByAccountingUnitAndGlAccountGroupingType(AccountingUnit accountingUnit, GlAccountGroupingType glAccountGroupingType);
    Set<GlAccount> findAllByAccountingUnit(AccountingUnit accountingUnit);
    Page<GlAccount> findAllByAccountingUnit(AccountingUnit accountingUnit, Pageable pageable);
}
