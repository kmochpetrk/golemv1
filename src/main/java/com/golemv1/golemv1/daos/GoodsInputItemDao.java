package com.golemv1.golemv1.daos;

import com.golemv1.golemv1.entities.GoodsInputItem;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GoodsInputItemDao extends PagingAndSortingRepository<GoodsInputItem, Long> {
}
