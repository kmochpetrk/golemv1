package com.golemv1.golemv1.entities;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @SequenceGenerator(name = "employee_seq", sequenceName = "employee_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employee_seq")
    private Long id;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "surname")
    private String surname;

    @Column(name = "employee_number")
    private String employeeNumber;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @OneToMany(mappedBy = "employee", fetch = FetchType.EAGER)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Set<MonthlySalary> monthlySalaries;

    @ManyToOne()
    @JoinColumn(name = "fk_acc_unit")
    private AccountingUnit accountingUnit;

    @Column(name = "monthly_gross_salary")
    private BigDecimal monthlyGrossSalary;

    @Column(name = "birth_number")
    private String birthNumber;

    @Column(name = "personal_bank_account")
    private String personalBankAccount;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public AccountingUnit getAccountingUnit() {
        return accountingUnit;
    }

    public void setAccountingUnit(AccountingUnit accountingUnit) {
        this.accountingUnit = accountingUnit;
    }

    public BigDecimal getMonthlyGrossSalary() {
        return monthlyGrossSalary;
    }

    public void setMonthlyGrossSalary(BigDecimal monthlyGrossSalary) {
        this.monthlyGrossSalary = monthlyGrossSalary;
    }

    public String getBirthNumber() {
        return birthNumber;
    }

    public void setBirthNumber(String birthNumber) {
        this.birthNumber = birthNumber;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Set<MonthlySalary> getMonthlySalaries() {
        return monthlySalaries;
    }

    public void setMonthlySalaries(Set<MonthlySalary> monthlySalaries) {
        this.monthlySalaries = monthlySalaries;
    }

    public String getPersonalBankAccount() {
        return personalBankAccount;
    }

    public void setPersonalBankAccount(String personalBankAccount) {
        this.personalBankAccount = personalBankAccount;
    }
}
