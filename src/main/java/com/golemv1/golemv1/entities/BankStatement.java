package com.golemv1.golemv1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@DiscriminatorValue("BANK_STATEMENT")
@Data
public class BankStatement extends GlWholeWrite {
    private boolean income;
    private String bankStatementNumber; //as emitted by bank
    private BigDecimal amount;
    private String variableSymbolFromInvoice;
    private LocalDate issueDate;
    private String documentNumber;
    private String variableSymbolInput;
    private String bankAccountInput;
    private String ourBankAccount;
    private Long cptId;

    @ManyToOne()
    @JoinColumn(name = "fk_out_invoice")
    @JsonIgnore
    private OutputInvoice outputInvoice;

    @ManyToOne()
    @JoinColumn(name = "fk_in_invoice")
    @JsonIgnore
    private InputInvoice inputInvoice;
}
