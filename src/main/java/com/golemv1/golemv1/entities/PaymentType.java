package com.golemv1.golemv1.entities;

public enum PaymentType {
    BANK,
    POST,
    CASH
}
