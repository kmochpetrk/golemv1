package com.golemv1.golemv1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "goods_output_item")
public class GoodsOutputItem {

    @Id
    @SequenceGenerator(name = "goods_output_item_seq", sequenceName = "goods_output_item_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "goods_output_item_seq")
    private Long id;

    @Column(name = "number_of_packets")
    private Long numberOfPackets;

    @Column(name = "output_price")
    private BigDecimal outputPrice;

    @Column(name = "time_in")
    private LocalDateTime timeIn;

    @ManyToOne()
    @JoinColumn(name = "fk_wh_gs")
    @JsonIgnore
    private WarehouseGoodsServices warehouseGoodsServices;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumberOfPackets() {
        return numberOfPackets;
    }

    public void setNumberOfPackets(Long numberOfPackets) {
        this.numberOfPackets = numberOfPackets;
    }

    public BigDecimal getOutputPrice() {
        return outputPrice;
    }

    public void setOutputPrice(BigDecimal outputPrice) {
        this.outputPrice = outputPrice;
    }

    public LocalDateTime getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(LocalDateTime timeIn) {
        this.timeIn = timeIn;
    }

    public void setWarehouseGoodsServices(WarehouseGoodsServices warehouseGoodsServices) {
        this.warehouseGoodsServices = warehouseGoodsServices;
    }

    public WarehouseGoodsServices getWarehouseGoodsServices() {
        return warehouseGoodsServices;
    }
}

