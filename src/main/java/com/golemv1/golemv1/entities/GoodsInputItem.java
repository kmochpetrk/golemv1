package com.golemv1.golemv1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "goods_input_item")
public class GoodsInputItem {

    @Id
    @SequenceGenerator(name = "goods_input_item_seq", sequenceName = "goods_input_item_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "goods_input_item_seq")
    private Long id;

    @Column(name = "number_of_packets")
    private Long numberOfPackets;

    @Column(name = "input_price")
    private BigDecimal inputPrice;

    @Column(name = "time_in")
    private LocalDateTime timeIn;

    @ManyToOne()
    @JoinColumn(name = "fk_wh_gs")
    @JsonIgnore
    private WarehouseGoodsServices warehouseGoodsServices;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumberOfPackets() {
        return numberOfPackets;
    }

    public void setNumberOfPackets(Long numberOfPackets) {
        this.numberOfPackets = numberOfPackets;
    }

    public BigDecimal getInputPrice() {
        return inputPrice;
    }

    public void setInputPrice(BigDecimal inputPrice) {
        this.inputPrice = inputPrice;
    }

    public LocalDateTime getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(LocalDateTime timeIn) {
        this.timeIn = timeIn;
    }

    public void setWarehouseGoodsServices(WarehouseGoodsServices warehouseGoodsServices) {
        this.warehouseGoodsServices = warehouseGoodsServices;
    }

    public WarehouseGoodsServices getWarehouseGoodsServices() {
        return warehouseGoodsServices;
    }
}
