package com.golemv1.golemv1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "eshop_order_item")
@Data
public class EshopOrderItem {
    @Id
    @SequenceGenerator(name = "eshop_order_seq", sequenceName = "eshop_order_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eshop_order_seq")
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "fk_goods_services")
    private GoodsServices goodsServices;

    @Column(name = "ordered_count")
    private Integer orderedCount;

    @Column(name = "total_item_vat")
    private BigDecimal totalItemVat;

    @Column(name = "total_item_price")
    private BigDecimal totalItemPrice;

    @ManyToOne()
    @JoinColumn(name = "fk_warehouse")
    private Warehouse warehouse;

    @Column(name = "unit_output_price")
    private BigDecimal unitOutputPrice;

    @Column(name = "unit_output_price_with_vat")
    private BigDecimal unitOutputPriceWithVat;

    @Column(name = "vat_percent")
    private Integer vatPercent;

    @ManyToOne()
    @JoinColumn(name = "fk_eshop_order")
    @JsonIgnore
    private EshopOrder eshopOrder;
}
