package com.golemv1.golemv1.entities;

public enum RoleType {
    Main_Accountant,
    Invoice_Processor,
    Financial_Accountant
}
