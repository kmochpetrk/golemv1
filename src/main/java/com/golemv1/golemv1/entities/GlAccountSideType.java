package com.golemv1.golemv1.entities;

public enum GlAccountSideType {
    DEBIT,
    CREDIT
}
