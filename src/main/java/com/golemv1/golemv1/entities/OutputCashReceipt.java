package com.golemv1.golemv1.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@DiscriminatorValue("CASH_OUTPUT_RECEIPT")
@Data
public class OutputCashReceipt extends GlWholeWrite{

    @Column(name = "total")
    private BigDecimal total;

    @Column(name = "total_tax")
    private BigDecimal totalTax;

    @Column(name = "cashier_title")
    private String cashierTitle;

    private Long ogrId;

}
