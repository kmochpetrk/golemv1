package com.golemv1.golemv1.entities;

import javax.persistence.*;

@Entity
@Table(name = "company_user")
public class CompanyUser {
    @Id
    @SequenceGenerator(name = "company_user_seq", sequenceName = "company_user_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "company_user_seq")
    private Long id;

    @Column(name = "company")
    private String company;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
