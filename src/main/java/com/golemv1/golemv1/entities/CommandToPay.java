package com.golemv1.golemv1.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "command_to_pay")
public class CommandToPay {
    @Id
    @SequenceGenerator(name = "command_to_pay_seq", sequenceName = "command_to_pay_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "command_to_pay_seq")
    private Long id;

    @Column(name = "document_number")
    private String documentNumber;

    @Column(name = "our_bank_account")
    private String ourBankAccount;

    @OneToMany(mappedBy = "commandToPay", fetch = FetchType.EAGER)
    private Set<InputInvoice> inputInvoices;

    @OneToMany(mappedBy = "commandToPay", fetch = FetchType.EAGER)
    private Set<CommandToPayItem> commandToPayItems;

    private String payedByBs;

    @ManyToOne()
    @JoinColumn(name = "fk_acc_unit")
    private AccountingUnit accountingUnit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<InputInvoice> getInputInvoices() {
        return inputInvoices;
    }

    public void setInputInvoices(Set<InputInvoice> inputInvoices) {
        this.inputInvoices = inputInvoices;
    }

    public String getOurBankAccount() {
        return ourBankAccount;
    }

    public void setOurBankAccount(String ourBankAccount) {
        this.ourBankAccount = ourBankAccount;
    }

    public Set<CommandToPayItem> getCommandToPayItems() {
        return commandToPayItems;
    }

    public void setCommandToPayItems(Set<CommandToPayItem> commandToPayItems) {
        this.commandToPayItems = commandToPayItems;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public AccountingUnit getAccountingUnit() {
        return accountingUnit;
    }

    public void setAccountingUnit(AccountingUnit accountingUnit) {
        this.accountingUnit = accountingUnit;
    }

    public String getPayedByBs() {
        return payedByBs;
    }

    public void setPayedByBs(String payedByBs) {
        this.payedByBs = payedByBs;
    }
}
