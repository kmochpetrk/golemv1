package com.golemv1.golemv1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

@Entity
@Table(name = "acc_warehouse")
@Data
public class Warehouse {
    @Id
    @SequenceGenerator(name = "acc_warehouse_seq", sequenceName = "acc_warehouse_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "acc_warehouse_seq")
    private Long id;

    private String name;

    @ManyToOne()
    @JoinColumn(name = "fk_acc_unit")
    @JsonIgnore
    private AccountingUnit accountingUnit;

    @ManyToOne()
    @JoinColumn(name = "fk_address")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Address address;

    @Column(name = "analytic_account")
    private Long analyticAccount;  //whnumber
}
