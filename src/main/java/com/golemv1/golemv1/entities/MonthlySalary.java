package com.golemv1.golemv1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "monthly_salary")
public class MonthlySalary {

    @Id
    @SequenceGenerator(name = "monthly_salary_seq", sequenceName = "monthly_salary_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "monthly_salary_seq")
    private Long id;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "period")
    private String period;

    @Column(name = "booking_number")
    private String bookingNumber;

    @ManyToOne()
    @JoinColumn(name = "fk_monthly_salary")
    @JsonIgnore
    private Employee employee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(String bookingNumber) {
        this.bookingNumber = bookingNumber;
    }
}
