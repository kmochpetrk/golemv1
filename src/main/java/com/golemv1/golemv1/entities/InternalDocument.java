package com.golemv1.golemv1.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("INTERNAL")
public class InternalDocument extends GlWholeWrite {
}
