package com.golemv1.golemv1.entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "acc_roles")
@Data
public class AccRoles {

    @Id
    @SequenceGenerator(name = "acc_roles_seq", sequenceName = "acc_roles_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "acc_roles_seq")
    @Column(name="acc_roles_id")
    private Long id;

    private String roleName;

    @ManyToOne()
    @JoinColumn(name = "fk_acc_unit")
    private AccountingUnit accountingUnit;

}
