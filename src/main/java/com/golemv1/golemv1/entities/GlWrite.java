package com.golemv1.golemv1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Table(name = "gl_write")
@Entity
@Data
public class GlWrite {

    @Id
    @SequenceGenerator(name = "gl_write_seq", sequenceName = "gl_write_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gl_write_seq")
    private Long id;


    private BigDecimal amount;

    @ManyToOne()
    @JoinColumn(name = "fk_gl_account")
    private GlAccount glAccount;

    @Column(name = "analytic_part")
    private String analyticPart;

    @Column(name = "gl_account_side_type")
    @Enumerated(EnumType.STRING)
    private GlAccountSideType glAccountSideType;

    @ManyToOne()
    @JoinColumn(name = "fk_gl_whole_write")
    @JsonIgnore
    private GlWholeWrite glWholeWrite;

}
