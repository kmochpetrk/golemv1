package com.golemv1.golemv1.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "acc_goods_services")
@Data
public class GoodsServices {

    @Id
    @SequenceGenerator(name = "acc_good_services_seq", sequenceName = "acc_good_services_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "acc_good_services_seq")
    private Long id;

    private String name;
    private String unit;

    @Enumerated(EnumType.STRING)
    private GsType gsType;

    @Column(name="packet_output_price")
    private BigDecimal packetOutputPrice;

    @Column(name="packet_input_price")
    private BigDecimal packetInputPrice;

    @Column(name="vat_in_percent")
    private BigDecimal vatPercents;

    @Column(name = "number_of_units_in_packet")
    private Long numberOfUnitsInPacket;

    private String goodsServicesNumber;

    @ManyToOne()
    @JoinColumn(name = "fk_acc_unit")
    @JsonIgnore
    private AccountingUnit accountingUnit;
}
