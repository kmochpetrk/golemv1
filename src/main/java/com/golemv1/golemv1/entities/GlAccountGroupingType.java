package com.golemv1.golemv1.entities;

import java.util.HashSet;
import java.util.Set;

public enum GlAccountGroupingType {



    PROPERTY_PLANT_EQUIPMENT(GlAccountType.BALANCE, GlAccountSideType.DEBIT),  //042 get
    PROPERTY_PLANT_EQUIPMENT_IN_USE(GlAccountType.BALANCE, GlAccountSideType.DEBIT),  //022 get
    INTANGIBLE_PROPERTY(GlAccountType.BALANCE, GlAccountSideType.DEBIT),
    DEPRECIATION(GlAccountType.BALANCE, GlAccountSideType.CREDIT),
    DEPRECIATION_OF_HOUSES(GlAccountType.BALANCE, GlAccountSideType.CREDIT),
    DEPRECIATION_OF_SOFTWARE(GlAccountType.BALANCE, GlAccountSideType.CREDIT),
    FINANCIAL_INVESTMENT(GlAccountType.BALANCE, GlAccountSideType.DEBIT),
    VAT(GlAccountType.BALANCE, GlAccountSideType.CREDIT),
    TIME_RESOLUTION(GlAccountType.BALANCE, GlAccountSideType.DEBIT),
    INVENTORIES(GlAccountType.BALANCE, GlAccountSideType.DEBIT),
    GOODS(GlAccountType.BALANCE, GlAccountSideType.DEBIT),
    GOODS_ON_THE_WAY(GlAccountType.BALANCE, GlAccountSideType.DEBIT),
    GOODS_GET(GlAccountType.CALCULATED, GlAccountSideType.DEBIT),
    TRADE_OTHER_RECEIVABLES(GlAccountType.BALANCE, GlAccountSideType.DEBIT),  //311 customers
    CASH_AND_EQUIVALENTS(GlAccountType.BALANCE, GlAccountSideType.DEBIT),
    BANK_ACCOUNTS(GlAccountType.BALANCE, GlAccountSideType.DEBIT),
    ISSUED_CAPITAL_EQUITY(GlAccountType.BALANCE, GlAccountSideType.CREDIT),
    SHARE_PREMIUM(GlAccountType.BALANCE, GlAccountSideType.CREDIT),
    RETAINED_EARNINGS_LOSSES(GlAccountType.BALANCE, GlAccountSideType.CREDIT),
    OTHER_EQUITY(GlAccountType.BALANCE, GlAccountSideType.CREDIT),
    BORROWINGS(GlAccountType.BALANCE, GlAccountSideType.CREDIT),
    EMPLOYEES(GlAccountType.BALANCE, GlAccountSideType.CREDIT),
    TRADE_OTHER_PAYABLES(GlAccountType.BALANCE, GlAccountSideType.CREDIT),  //321 suppliers
    PROVISIONS(GlAccountType.BALANCE, GlAccountSideType.CREDIT),
    ACCRUED_DEFERRED_OTHER_LIABILITIES(GlAccountType.BALANCE, GlAccountSideType.CREDIT),
    INCOME_TAX_LIABILITIES(GlAccountType.BALANCE, GlAccountSideType.CREDIT),
    REVENUE_GOODS(GlAccountType.PROFIT_LOSS, GlAccountSideType.CREDIT),
    REVENUE_SERVICES(GlAccountType.PROFIT_LOSS, GlAccountSideType.CREDIT),
    REVENUE_OTHER(GlAccountType.PROFIT_LOSS, GlAccountSideType.CREDIT),
    EXPENSES_GOODS(GlAccountType.PROFIT_LOSS, GlAccountSideType.DEBIT),
    EXPENSES_GAS(GlAccountType.PROFIT_LOSS, GlAccountSideType.DEBIT),
    EXPENSES_EMPLOYEES(GlAccountType.PROFIT_LOSS, GlAccountSideType.DEBIT),
    EXPENSES_SERVICES(GlAccountType.PROFIT_LOSS, GlAccountSideType.DEBIT),
    EXPENSES_SERVICES_ELECTRIC(GlAccountType.PROFIT_LOSS, GlAccountSideType.DEBIT),
    EXPENSES_SERVICES_WATER(GlAccountType.PROFIT_LOSS, GlAccountSideType.DEBIT),
    TAX_FROM_INCOME(GlAccountType.PROFIT_LOSS, GlAccountSideType.DEBIT),
    OTHER_GAINS_LOSSES(GlAccountType.PROFIT_LOSS, GlAccountSideType.CREDIT),
    EXPENSES_OTHER(GlAccountType.PROFIT_LOSS, GlAccountSideType.DEBIT),
    EXPENSES_OTHER_RENT_HOUSES(GlAccountType.PROFIT_LOSS, GlAccountSideType.DEBIT),
    CASH_IN_TRANSIT(GlAccountType.BALANCE, GlAccountSideType.DEBIT),
    AMORTIZATION(GlAccountType.PROFIT_LOSS, GlAccountSideType.DEBIT),
    AMORTIZATION_OF_HOUSES(GlAccountType.PROFIT_LOSS, GlAccountSideType.DEBIT),
    AMORTIZATION_OF_SOFTWARE(GlAccountType.PROFIT_LOSS, GlAccountSideType.DEBIT),
    GAINS_INTEREST(GlAccountType.PROFIT_LOSS, GlAccountSideType.CREDIT);

    private GlAccountGroupingType(GlAccountType glAccountType, GlAccountSideType glAccountSideType) {
        this.glAccountType = glAccountType;
        this.glAccountSideType = glAccountSideType;
    }

    private GlAccountType glAccountType;
    private GlAccountSideType glAccountSideType;

    public static Set<GlAccountGroupingType> getByTypeAndSide(GlAccountType glAccountType, GlAccountSideType glAccountSideType) {
        Set<GlAccountGroupingType> returnSet = new HashSet<>();
        for (GlAccountGroupingType glAccountGroupingType : values()) {
            if (glAccountGroupingType.glAccountSideType.equals(glAccountSideType) &&
                    glAccountGroupingType.glAccountType.equals(glAccountType)) {
                returnSet.add(glAccountGroupingType);
            }
        }
        return returnSet;
    }

}
