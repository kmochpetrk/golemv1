package com.golemv1.golemv1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "acc_warehouse_goods_services")
public class WarehouseGoodsServices {

    @Id
    @SequenceGenerator(name = "acc_wh_goods_ser_seq", sequenceName = "acc_wh_goods_ser_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "acc_wh_goods_ser_seq")
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "fk_warehouse")
    private Warehouse warehouse;

    @ManyToOne()
    @JoinColumn(name = "fk_goods_services")
    private GoodsServices goodsServices;

    @OneToMany(mappedBy = "warehouseGoodsServices")
    private Set<GoodsInputItem> goodsInputs;

    @OneToMany(mappedBy = "warehouseGoodsServices")
    private Set<GoodsOutputItem> goodsOutputs;

    @Transient
    private BigDecimal inputPrice;

    @Transient
    private BigDecimal outputPrice;

    @Transient
    private Long count;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public GoodsServices getGoodsServices() {
        return goodsServices;
    }

    public void setGoodsServices(GoodsServices goodsServices) {
        this.goodsServices = goodsServices;
    }

    public Set<GoodsInputItem> getGoodsInputs() {
        return goodsInputs;
    }

    public void setGoodsInputs(Set<GoodsInputItem> goodsInputs) {
        this.goodsInputs = goodsInputs;
    }

    public BigDecimal getInputPrice() {
        return inputPrice;
    }

    public void setInputPrice(BigDecimal inputPrice) {
        this.inputPrice = inputPrice;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Set<GoodsOutputItem> getGoodsOutputs() {
        return goodsOutputs;
    }

    public void setGoodsOutputs(Set<GoodsOutputItem> goodsOutputs) {
        this.goodsOutputs = goodsOutputs;
    }

    public BigDecimal getOutputPrice() {
        return outputPrice;
    }

    public void setOutputPrice(BigDecimal outputPrice) {
        this.outputPrice = outputPrice;
    }
}
