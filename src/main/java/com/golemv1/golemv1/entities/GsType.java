package com.golemv1.golemv1.entities;

public enum GsType {
    GOODS,
    SERVICE,
    OTHER
}
