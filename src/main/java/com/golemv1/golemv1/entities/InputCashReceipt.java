package com.golemv1.golemv1.entities;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Entity
@DiscriminatorValue("CASH_INPUT_RECEIPT")
@Data
public class InputCashReceipt extends GlWholeWrite {
    @Column(name = "total")
    private BigDecimal total;

    @Column(name = "total_tax")
    private BigDecimal totalTax;

    @Column(name = "cashier_title")
    private String cashierTitle;

    private Long igrId;

}
