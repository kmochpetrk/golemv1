package com.golemv1.golemv1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "command_to_pay_item")
public class CommandToPayItem {
    @Id
    @SequenceGenerator(name = "command_to_pay_item_seq", sequenceName = "command_to_pay_item_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "command_to_pay_item_seq")
    private Long id;

    @Column(name="variable_symbol")
    private String variableSymbol;

    @Column(name = "message_for_receiver")
    private String messageForReceiver;

    @Column(name="other_bank_account")
    private String otherBankAccount;

    @Column(name="specific_symbol")
    private String specificSymbol;

    @Column(name = "constant_symbol")
    private String constantSymbol;

    @Column(name = "amount")
    private BigDecimal amount;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "fk_command_to_pay")
    private CommandToPay commandToPay;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVariableSymbol() {
        return variableSymbol;
    }

    public void setVariableSymbol(String variableSymbol) {
        this.variableSymbol = variableSymbol;
    }

    public String getMessageForReceiver() {
        return messageForReceiver;
    }

    public void setMessageForReceiver(String messageForReceiver) {
        this.messageForReceiver = messageForReceiver;
    }

    public String getOtherBankAccount() {
        return otherBankAccount;
    }

    public void setOtherBankAccount(String otherBankAccount) {
        this.otherBankAccount = otherBankAccount;
    }

    public String getSpecificSymbol() {
        return specificSymbol;
    }

    public void setSpecificSymbol(String specificSymbol) {
        this.specificSymbol = specificSymbol;
    }

    public String getConstantSymbol() {
        return constantSymbol;
    }

    public void setConstantSymbol(String constantSymbol) {
        this.constantSymbol = constantSymbol;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public CommandToPay getCommandToPay() {
        return commandToPay;
    }

    public void setCommandToPay(CommandToPay commandToPay) {
        this.commandToPay = commandToPay;
    }
}
