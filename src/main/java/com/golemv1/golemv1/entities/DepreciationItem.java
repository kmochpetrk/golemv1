package com.golemv1.golemv1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "depreciation_item")
public class DepreciationItem {
    @Id
    @SequenceGenerator(name = "depreciation_item_seq", sequenceName = "depreciation_item_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "depreciation_item_seq")
    private Long id;

    @Column(name = "period")
    private String period;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "booking_number")
    private String bookingNumber;

    @ManyToOne()
    @JoinColumn(name = "fk_inventory_item")
    @JsonIgnore
    private InventoryItem inventoryItem;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public InventoryItem getInventoryItem() {
        return inventoryItem;
    }

    public void setInventoryItem(InventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
    }

    public String getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(String bookingNumber) {
        this.bookingNumber = bookingNumber;
    }
}
