package com.golemv1.golemv1.entities;

public enum GlWholeWriteType {
    INPUT_INVOICE,
    OUTPUT_INVOICE,
    BANK_STATEMENT,
    CASH_INPUT_RECEIPT,
    CASH_OUTPUT_RECEIPT,
    GOODS_INPUT_RECEIPT,
    GOODS_OUTPUT_RECEIPT,
    INTERNAL;

    public static String getValue(GlWholeWriteType glWholeWriteType) {
        return glWholeWriteType.name();
    }
}
