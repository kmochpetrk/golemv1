package com.golemv1.golemv1.entities;


import lombok.Data;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "accounting_unit")
public class AccountingUnit {

    @Id
    @SequenceGenerator(name = "acc_unit_seq", sequenceName = "acc_unit_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "acc_unit_seq")
    private Long id;


    @Column(unique = true)
    private String name;

    private String ico;

    private String dic;

    @Column(nullable = false, columnDefinition = "int default 0")
    private int nextGoodsOutputReceiptNumber;

    @Column(nullable = false, columnDefinition = "int default 0")
    private int nextGoodsInputReceiptNumber;

    @Column(nullable = false, columnDefinition = "int default 0")
    private int inputInvoiceNumber;

    @Column(nullable = false, columnDefinition = "int default 0")
    private int inputCashReceiptNumber;

    @Column(nullable = false, columnDefinition = "int default 0")
    private int outputCashReceiptNumber;

    @Column(nullable = false, columnDefinition = "int default 20")
    private int goodsServiceNumber;

    @Column(nullable = false, columnDefinition = "int default 20")
    private int commandToPayNumber;

    @Version
    private Integer version;

    @Column(nullable = false, columnDefinition = "int default 0")
    private int outputInvoiceNumber;

    @Column(nullable = false, columnDefinition = "int default 0")
    private int bankStatementNumber;

    @Column(nullable = false, columnDefinition = "int default 0")
    private String bankAccount;

    @Column(nullable = false, columnDefinition = "int default 0")
    private int inventoryNumber;

    @Column(nullable = false, columnDefinition = "int default 0")
    private int employeeNumber;

    @Column(name = "internal_document_number", nullable = false, columnDefinition = "int default 0")
    private int internalDocumentNumber;

    @ManyToOne()
    @JoinColumn(name = "fk_address")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Address primaryAddress;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIco() {
        return ico;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }

    public String getDic() {
        return dic;
    }

    public void setDic(String dic) {
        this.dic = dic;
    }

    public int getNextGoodsOutputReceiptNumber() {
        return nextGoodsOutputReceiptNumber;
    }

    public void setNextGoodsOutputReceiptNumber(int nextGoodsOutputReceiptNumber) {
        this.nextGoodsOutputReceiptNumber = nextGoodsOutputReceiptNumber;
    }

    public int getNextGoodsInputReceiptNumber() {
        return nextGoodsInputReceiptNumber;
    }

    public void setNextGoodsInputReceiptNumber(int nextGoodsInputReceiptNumber) {
        this.nextGoodsInputReceiptNumber = nextGoodsInputReceiptNumber;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public int getOutputInvoiceNumber() {
        return outputInvoiceNumber;
    }

    public void setOutputInvoiceNumber(int outputInvoiceNumber) {
        this.outputInvoiceNumber = outputInvoiceNumber;
    }

    public Address getPrimaryAddress() {
        return primaryAddress;
    }

    public void setPrimaryAddress(Address primaryAddress) {
        this.primaryAddress = primaryAddress;
    }

    public int getInputInvoiceNumber() {
        return inputInvoiceNumber;
    }

    public void setInputInvoiceNumber(int inputInvoiceNumber) {
        this.inputInvoiceNumber = inputInvoiceNumber;
    }

    public int getGoodsServiceNumber() {
        return goodsServiceNumber;
    }

    public void setGoodsServiceNumber(int goodsServiceNumber) {
        this.goodsServiceNumber = goodsServiceNumber;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public int getBankStatementNumber() {
        return bankStatementNumber;
    }

    public void setBankStatementNumber(int bankStatementNumber) {
        this.bankStatementNumber = bankStatementNumber;
    }

    public int getInputCashReceiptNumber() {
        return inputCashReceiptNumber;
    }

    public void setInputCashReceiptNumber(int inputCashReceiptNumber) {
        this.inputCashReceiptNumber = inputCashReceiptNumber;
    }

    public int getOutputCashReceiptNumber() {
        return outputCashReceiptNumber;
    }

    public void setOutputCashReceiptNumber(int outputCashReceiptNumber) {
        this.outputCashReceiptNumber = outputCashReceiptNumber;
    }

    public int getInternalDocumentNumber() {
        return internalDocumentNumber;
    }

    public void setInternalDocumentNumber(int internalDocumentNumber) {
        this.internalDocumentNumber = internalDocumentNumber;
    }

    public int getCommandToPayNumber() {
        return commandToPayNumber;
    }

    public void setCommandToPayNumber(int commandToPayNumber) {
        this.commandToPayNumber = commandToPayNumber;
    }

    public int getInventoryNumber() {
        return inventoryNumber;
    }

    public void setInventoryNumber(int inventoryNumber) {
        this.inventoryNumber = inventoryNumber;
    }

    public int getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(int employeeNumber) {
        this.employeeNumber = employeeNumber;
    }
}
