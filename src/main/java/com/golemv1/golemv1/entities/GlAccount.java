package com.golemv1.golemv1.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "gl_account")
@Data
public class GlAccount {

    @Id
    @SequenceGenerator(name = "gl_acctno_seq", sequenceName = "gl_acctno_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gl_acctno_seq")
    private Long id;

    //@NotEmpty
    @Column(name = "acct_no")
    private String acctNo;

    //@NotNull
    @Column(name = "gl_account_grouping_type")
    @Enumerated(EnumType.STRING)
    private GlAccountGroupingType glAccountGroupingType;

    //@NotNull
    @Column(name = "gl_account_type")
    @Enumerated(EnumType.STRING)
    private GlAccountType glAccountType;

    //@NotNull
    @Column(name = "gl_account_side_type")
    @Enumerated(EnumType.STRING)
    private GlAccountSideType glAccountSideType;

    //@NotEmpty
    @Column(name = "description")
    private String description;

    @Column(name = "additional_info")
    private String additionalInfo;

    @ManyToOne()
    @JoinColumn(name = "fk_acc_unit")
    private AccountingUnit accountingUnit;

}
