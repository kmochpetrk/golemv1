package com.golemv1.golemv1.entities;

import com.golemv1.golemv1.daos.CrmType;
import lombok.Data;
import org.hibernate.ScrollableResults;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

@Entity
@Table(name = "crm")
@Data
public class Crm {

    @Id
    @SequenceGenerator(name = "crm_seq", sequenceName = "crm_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "crm_seq")
    private Long id;

    private String ico;
    private String dic;

    private String name;
    private String phone;

    @Enumerated(EnumType.STRING)
    private CrmType crmType;

    private String email;

    @Enumerated(EnumType.STRING)
    private ContactType contactType;

    private String bankAccount;

    @ManyToOne()
    @JoinColumn(name = "fk_address")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Address primaryAddress;

    @ManyToOne()
    @JoinColumn(name = "fk_acc_unit")
    private AccountingUnit accountingUnit;
}
