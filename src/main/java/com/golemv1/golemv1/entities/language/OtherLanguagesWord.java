package com.golemv1.golemv1.entities.language;

import com.golemv1.golemv1.model.language.OtherLanguagesType;

import javax.persistence.*;

@Entity
public class OtherLanguagesWord {

    @Id
    @SequenceGenerator(name = "other_word_seq", sequenceName = "other_word_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "other_word_seq")
    private Long id;

    private String word;

    @Enumerated(EnumType.STRING)
    private OtherLanguagesType otherLanguagesType;

    @ManyToOne()
    @JoinColumn(name = "fk_eng_word")
    private EnglishWord englishWord;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public OtherLanguagesType getOtherLanguagesType() {
        return otherLanguagesType;
    }

    public void setOtherLanguagesType(OtherLanguagesType otherLanguagesType) {
        this.otherLanguagesType = otherLanguagesType;
    }

    public EnglishWord getEnglishWord() {
        return englishWord;
    }

    public void setEnglishWord(EnglishWord englishWord) {
        this.englishWord = englishWord;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
