package com.golemv1.golemv1.entities.language;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.golemv1.golemv1.entities.GlWholeWriteType;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class AccRule {
    @Id
    @SequenceGenerator(name = "acc_rule_seq", sequenceName = "acc_rule_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "acc_rule_seq")
    private Long id;

    @OneToMany(mappedBy = "accRule")
    private List<AccRuleElement> ruleElements;

    @OneToMany(mappedBy = "accRule")
    @JsonIgnore
    private Set<Sentence> sentenceSet;

    @Enumerated(EnumType.STRING)
    private GlWholeWriteType glWholeWriteType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<AccRuleElement> getRuleElements() {
        return ruleElements;
    }

    public void setRuleElements(List<AccRuleElement> ruleElements) {
        this.ruleElements = ruleElements;
    }

    public Set<Sentence> getSentenceSet() {
        return sentenceSet;
    }

    public void setSentenceSet(Set<Sentence> sentenceSet) {
        this.sentenceSet = sentenceSet;
    }

    public GlWholeWriteType getGlWholeWriteType() {
        return glWholeWriteType;
    }

    public void setGlWholeWriteType(GlWholeWriteType glWholeWriteType) {
        this.glWholeWriteType = glWholeWriteType;
    }


}
