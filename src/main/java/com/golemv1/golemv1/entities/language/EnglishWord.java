package com.golemv1.golemv1.entities.language;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class EnglishWord {

    @Id
    @SequenceGenerator(name = "eng_word_seq", sequenceName = "eng_word_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eng_word_seq")
    @Column(name="eng_word_id")
    private Long id;

    @ManyToMany()
    @JoinTable(
            name = "sentences_words",
            joinColumns = @JoinColumn(name = "eng_word_id"),
            inverseJoinColumns = @JoinColumn(name = "sentence_id"),
            uniqueConstraints = {}
    )
    private List<Sentence> sentences;

    private String word;

    @OneToMany(mappedBy = "englishWord", cascade = CascadeType.ALL)
    private Set<OtherLanguagesWord> otherLanguagesWordSet;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Set<OtherLanguagesWord> getOtherLanguagesWordSet() {
        return otherLanguagesWordSet;
    }

    public void setOtherLanguagesWordSet(Set<OtherLanguagesWord> otherLanguagesWordSet) {
        this.otherLanguagesWordSet = otherLanguagesWordSet;
    }

    public List<Sentence> getSentences() {
        return sentences;
    }

    public void setSentences(List<Sentence> sentences) {
        this.sentences = sentences;
    }
}
