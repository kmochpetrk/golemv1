package com.golemv1.golemv1.entities.language;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

@Entity
public class Sentence {
    @Id
    @SequenceGenerator(name = "sentence_seq", sequenceName = "sentence_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sentence_seq")
    @Column(name="sentence_id")
    private Long id;

    @ManyToMany()
    @JoinTable(
            name = "sentences_words",
            joinColumns = @JoinColumn(name = "sentence_id"),
            inverseJoinColumns = @JoinColumn(name = "eng_word_id"),
            uniqueConstraints = {}
    )
    private List<EnglishWord> words;

    @ManyToOne()
    @JoinColumn(name = "fk_rule")
    private AccRule accRule;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<EnglishWord> getWords() {
        return words;
    }

    public void setWords(List<EnglishWord> words) {
        this.words = words;
    }

    public AccRule getAccRule() {
        return accRule;
    }

    public void setAccRule(AccRule accRule) {
        this.accRule = accRule;
    }
}
