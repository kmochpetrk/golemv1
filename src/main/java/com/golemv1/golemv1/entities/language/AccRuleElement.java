package com.golemv1.golemv1.entities.language;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.golemv1.golemv1.entities.GlAccountGroupingType;
import com.golemv1.golemv1.entities.GlAccountSideType;
import com.golemv1.golemv1.entities.GlWholeWriteType;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class AccRuleElement {
    @Id
    @SequenceGenerator(name = "acc_relement_seq", sequenceName = "acc_relement_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "acc_relement_seq")
    private Long id;

    private BigDecimal percents;

    @Enumerated(EnumType.STRING)
    private GlAccountSideType glAccountSideType;

    @Enumerated(EnumType.STRING)
    private GlAccountGroupingType accountGroupingType;

    @ManyToOne()
    @JoinColumn(name = "fk_rule")
    @JsonIgnore
    private AccRule accRule;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPercents() {
        return percents;
    }

    public void setPercents(BigDecimal percents) {
        this.percents = percents;
    }

    public GlAccountSideType getGlAccountSideType() {
        return glAccountSideType;
    }

    public void setGlAccountSideType(GlAccountSideType glAccountSideType) {
        this.glAccountSideType = glAccountSideType;
    }

    public GlAccountGroupingType getAccountGroupingType() {
        return accountGroupingType;
    }

    public void setAccountGroupingType(GlAccountGroupingType accountGroupingType) {
        this.accountGroupingType = accountGroupingType;
    }

    public AccRule getAccRule() {
        return accRule;
    }

    public void setAccRule(AccRule accRule) {
        this.accRule = accRule;
    }


}
