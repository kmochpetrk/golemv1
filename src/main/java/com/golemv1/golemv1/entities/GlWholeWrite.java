package com.golemv1.golemv1.entities;

import lombok.Data;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Table(name = "gl_whole_write")
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="gl_whole_write_type",
        discriminatorType = DiscriminatorType.STRING)
public abstract class GlWholeWrite {

    @Id
    @SequenceGenerator(name = "gl_whole_write_seq", sequenceName = "gl_whole_write_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gl_whole_write_seq")
    private Long id;

    @OneToMany(mappedBy = "glWholeWrite", fetch = FetchType.EAGER)
    //@Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Set<GlWrite> glWrites;

    @Column(name = "gl_whole_write_type", insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private GlWholeWriteType glWholeWriteType;


    @Column(name = "accounting_date")
    private LocalDate accountingDate;

    @Column(name = "tax_date")
    private LocalDate taxDate;

    private String variableSymbol;

    @ManyToOne()
    @JoinColumn(name = "fk_acc_users")
    private AccUsers writtenBy;

    @ManyToOne()
    @JoinColumn(name = "fk_crm")
    private Crm crm;

    @ManyToOne()
    @JoinColumn(name = "fk_acc_unit")
    private AccountingUnit accountingUnit;

    @OneToMany(mappedBy = "glWholeWrite", fetch = FetchType.EAGER)
    //@Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Set<GoodsServiceItems> goodsServiceItems;

    @Column(name = "document_number")
    private String documentNumber;


    @Column(name = "description_of_case")
    private String descriptionOfCase;

    private Boolean cancelled = false;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<GlWrite> getGlWrites() {
        return glWrites;
    }

    public void setGlWrites(Set<GlWrite> glWrites) {
        this.glWrites = glWrites;
    }

    public LocalDate getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(LocalDate accountingDate) {
        this.accountingDate = accountingDate;
    }

    public LocalDate getTaxDate() {
        return taxDate;
    }

    public void setTaxDate(LocalDate taxDate) {
        this.taxDate = taxDate;
    }

    public AccUsers getWrittenBy() {
        return writtenBy;
    }

    public void setWrittenBy(AccUsers writtenBy) {
        this.writtenBy = writtenBy;
    }

    public AccountingUnit getAccountingUnit() {
        return accountingUnit;
    }

    public void setAccountingUnit(AccountingUnit accountingUnit) {
        this.accountingUnit = accountingUnit;
    }

    public Set<GoodsServiceItems> getGoodsServiceItems() {
        return goodsServiceItems;
    }

    public void setGoodsServiceItems(Set<GoodsServiceItems> goodsServiceItems) {
        this.goodsServiceItems = goodsServiceItems;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Crm getCrm() {
        return crm;
    }

    public void setCrm(Crm crm) {
        this.crm = crm;
    }

    public Boolean getCancelled() {
        return cancelled;
    }

    public void setCancelled(Boolean cancelled) {
        this.cancelled = cancelled;
    }

    public String getDescriptionOfCase() {
        return descriptionOfCase;
    }

    public void setDescriptionOfCase(String descriptionOfCase) {
        this.descriptionOfCase = descriptionOfCase;
    }
    public String getVariableSymbol() {
        return variableSymbol;
    }

    public void setVariableSymbol(String variableSymbol) {
        this.variableSymbol = variableSymbol;
    }

    public GlWholeWriteType getGlWholeWriteType() {
        return glWholeWriteType;
    }

    public void setGlWholeWriteType(GlWholeWriteType glWholeWriteType) {
        this.glWholeWriteType = glWholeWriteType;
    }
}
