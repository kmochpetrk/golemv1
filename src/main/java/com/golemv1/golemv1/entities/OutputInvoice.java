package com.golemv1.golemv1.entities;


import lombok.Data;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Entity
@DiscriminatorValue("OUTPUT_INVOICE")
public class OutputInvoice extends GlWholeWrite {

    @Column(name = "total")
    private BigDecimal total;

    @Column(name = "total_tax")
    private BigDecimal totalTax;

    //@Enumerated(EnumType.STRING)
    private String paymentType;
    private String transportType;

    @Column(name = "pay_date")
    private LocalDate payDate;

    @OneToMany(mappedBy = "outputInvoice")
    private Set<GoodsOutputReceipt> goodsOutputReceipts;

//    @OneToMany(mappedBy = "outputInvoice")
//    private Set<BankStatement> bankStatements;

    private Long bsId;

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(BigDecimal totalTax) {
        this.totalTax = totalTax;
    }

    public Set<GoodsOutputReceipt> getGoodsOutputReceipts() {
        return goodsOutputReceipts;
    }

    public void setGoodsOutputReceipts(Set<GoodsOutputReceipt> goodsOutputReceipts) {
        this.goodsOutputReceipts = goodsOutputReceipts;
    }

    public LocalDate getPayDate() {
        return payDate;
    }

    public void setPayDate(LocalDate payDate) {
        this.payDate = payDate;
    }

//    public Set<BankStatement> getBankStatements() {
//        return bankStatements;
//    }
//
//    public void setBankStatements(Set<BankStatement> bankStatements) {
//        this.bankStatements = bankStatements;
//    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public Long getBsId() {
        return bsId;
    }

    public void setBsId(Long bsId) {
        this.bsId = bsId;
    }
}
