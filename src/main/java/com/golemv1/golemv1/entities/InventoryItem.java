package com.golemv1.golemv1.entities;

import com.golemv1.golemv1.model.DepreciationType;
import com.golemv1.golemv1.model.PropertyKind;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "inventory_item")
public class InventoryItem {

    @Id
    @SequenceGenerator(name = "inventory_item_seq", sequenceName = "inventory_item_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "inventory_item_seq")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "inventory_number")
    private String inventoryNumber;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @ManyToOne()
    @JoinColumn(name = "fk_acc_unit")
    private AccountingUnit accountingUnit;

    @Column(name = "depreciation_class")
    private String depreciationClass;

    @Column(name = "price")
    private BigDecimal price;

    @Enumerated(EnumType.STRING)
    private DepreciationType depreciationType;

    @Enumerated(EnumType.STRING)
    private PropertyKind propertyKind;

    @OneToMany(mappedBy = "inventoryItem", fetch = FetchType.EAGER)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Set<DepreciationItem> depreciationItems;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInventoryNumber() {
        return inventoryNumber;
    }

    public void setInventoryNumber(String inventoryNumber) {
        this.inventoryNumber = inventoryNumber;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public AccountingUnit getAccountingUnit() {
        return accountingUnit;
    }

    public void setAccountingUnit(AccountingUnit accountingUnit) {
        this.accountingUnit = accountingUnit;
    }

    public DepreciationType getDepreciationType() {
        return depreciationType;
    }

    public void setDepreciationType(DepreciationType depreciationType) {
        this.depreciationType = depreciationType;
    }

    public Set<DepreciationItem> getDepreciationItems() {
        return depreciationItems;
    }

    public void setDepreciationItems(Set<DepreciationItem> depreciationItems) {
        this.depreciationItems = depreciationItems;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepreciationClass() {
        return depreciationClass;
    }

    public void setDepreciationClass(String depreciationClass) {
        this.depreciationClass = depreciationClass;
    }

    public PropertyKind getPropertyKind() {
        return propertyKind;
    }

    public void setPropertyKind(PropertyKind propertyKind) {
        this.propertyKind = propertyKind;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
