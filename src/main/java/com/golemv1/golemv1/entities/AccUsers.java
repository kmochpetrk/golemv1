package com.golemv1.golemv1.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "acc_users")
@Data
public class AccUsers {
    @Id
    @SequenceGenerator(name = "users_seq", sequenceName = "users_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_seq")
    @Column(name="acc_users_id")
    private Long id;

    private String firstName;
    private String surname;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "acc_users_roles",
            joinColumns = @JoinColumn(name = "acc_users_id"),
            inverseJoinColumns = @JoinColumn(name = "acc_roles_id")
    )
    private List<AccRoles> roles;

    @ManyToOne()
    @JoinColumn(name = "fk_acc_unit")
    private AccountingUnit accountingUnit;
}
