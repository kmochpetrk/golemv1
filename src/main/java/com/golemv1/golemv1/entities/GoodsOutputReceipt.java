package com.golemv1.golemv1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("GOODS_OUTPUT_RECEIPT")
@Data
public class GoodsOutputReceipt extends GlWholeWrite {

    @ManyToOne()
    @JoinColumn(name = "fk_out_invoice")
    @JsonIgnore
    private OutputInvoice outputInvoice;

    private Long icrId;

}
