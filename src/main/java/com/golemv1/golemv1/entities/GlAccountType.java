package com.golemv1.golemv1.entities;

public enum GlAccountType {
    BALANCE,
    PROFIT_LOSS,
    CALCULATED //means after close 0 remains
}
