package com.golemv1.golemv1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Entity
@DiscriminatorValue("INPUT_INVOICE")
public class InputInvoice extends GlWholeWrite{
    @Column(name = "total")
    private BigDecimal total;

    @Column(name = "total_tax")
    private BigDecimal totalTax;

    //@Enumerated(EnumType.STRING)
    private String paymentType;
    private String transportType;
    private String bankAccount;

    @Column(name = "pay_date")
    private LocalDate payDate;

    @OneToMany(mappedBy = "inputInvoice")
    private Set<GoodsInputReceipt> goodsInputReceipts;

//    @OneToMany(mappedBy = "inputInvoice")
//    private Set<BankStatement> bankStatements;

    private Long bsId;

    @ManyToOne
    @JoinColumn(name = "fk_command_to_pay")
    @JsonIgnore
    private CommandToPay commandToPay;

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(BigDecimal totalTax) {
        this.totalTax = totalTax;
    }

    public Set<GoodsInputReceipt> getGoodsInputReceipts() {
        return goodsInputReceipts;
    }

    public void setGoodsInputReceipts(Set<GoodsInputReceipt> goodsInputReceipts) {
        this.goodsInputReceipts = goodsInputReceipts;
    }

    public LocalDate getPayDate() {
        return payDate;
    }

    public void setPayDate(LocalDate payDate) {
        this.payDate = payDate;
    }

    public CommandToPay getCommandToPay() {
        return commandToPay;
    }

    public void setCommandToPay(CommandToPay commandToPay) {
        this.commandToPay = commandToPay;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public Long getBsId() {
        return bsId;
    }

    public void setBsId(Long bsId) {
        this.bsId = bsId;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }
}
