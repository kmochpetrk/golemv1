package com.golemv1.golemv1.entities;

import lombok.Data;
import org.springframework.data.repository.cdi.Eager;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "eshop_order")
public class EshopOrder {
    @Id
    @SequenceGenerator(name = "eshop_order_seq", sequenceName = "eshop_order_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eshop_order_seq")
    private Long id;

    @Column(name = "payment_type")
    private String paymentType;

    @Column(name = "delivery_type")
    private String deliveryType;

    @Column(name = "total_vat")
    private BigDecimal totalVat;

    @Column(name = "total_price")
    private BigDecimal totalPrice;

    @Column(name = "oi_number")
    private String OINumber;

    @Column(name = "our_bank_account")
    private String ourBankAccount;

    @Column(name = "company")
    private String company;

    @OneToMany(mappedBy = "eshopOrder", fetch = FetchType.EAGER)
    private Set<EshopOrderItem> ordersItems;

    @ManyToOne()
    @JoinColumn(name = "fk_crm")
    private Crm crm;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public BigDecimal getTotalVat() {
        return totalVat;
    }

    public void setTotalVat(BigDecimal totalVat) {
        this.totalVat = totalVat;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getOINumber() {
        return OINumber;
    }

    public void setOINumber(String OINumber) {
        this.OINumber = OINumber;
    }

    public String getOurBankAccount() {
        return ourBankAccount;
    }

    public void setOurBankAccount(String ourBankAccount) {
        this.ourBankAccount = ourBankAccount;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Set<EshopOrderItem> getOrdersItems() {
        return ordersItems;
    }

    public void setOrdersItems(Set<EshopOrderItem> ordersItems) {
        this.ordersItems = ordersItems;
    }

    public Crm getCrm() {
        return crm;
    }

    public void setCrm(Crm crm) {
        this.crm = crm;
    }
}
