package com.golemv1.golemv1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("GOODS_INPUT_RECEIPT")
@Data
public class GoodsInputReceipt extends GlWholeWrite {

    @ManyToOne()
    @JoinColumn(name = "fk_in_invoice")
    @JsonIgnore
    private InputInvoice inputInvoice;

    private Long ocrId;

}
