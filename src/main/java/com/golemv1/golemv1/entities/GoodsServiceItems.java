package com.golemv1.golemv1.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "acc_goods_services_items")
@Data
public class GoodsServiceItems {

    @Id
    @SequenceGenerator(name = "acc_goods_services_items_seq", sequenceName = "acc_goods_services_items_seq", allocationSize = 3)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "acc_goods_services_items_seq")
    private Long id;


    @ManyToOne()
    @JoinColumn(name = "fk_goods_services_warehouse")
    private WarehouseGoodsServices warehouseGoodsServices;  // in out goods receipt

    @ManyToOne()
    @JoinColumn(name = "fk_goods_services")     // invoice
    private GoodsServices goodsServices;

    @Column(name = "unit_price")
    private BigDecimal unitPrice;

    @Column(name = "number_of_units")
    private Long numberOfUnits;

    @Column(name = "vat_percent")
    private BigDecimal vatPercent;

    @Column(name = "vat_total")
    private BigDecimal vatTotal;

    @Column(name = "price_total")
    private BigDecimal priceTotal;

    @ManyToOne()
    @JoinColumn(name = "fk_whole_write")
    @JsonIgnore
    private GlWholeWrite glWholeWrite;
}
