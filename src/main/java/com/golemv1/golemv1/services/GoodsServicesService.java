package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.GoodsServicesDao;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.GoodsServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Slf4j
@Transactional
public class GoodsServicesService {

    @Autowired
    private GoodsServicesDao goodsServicesDao;

    @Autowired
    private AccUnitDao accUnitDao;

    public GoodsServices createGoodsServices(GoodsServices goodsServices, String company) {
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        goodsServices.setAccountingUnit(accountingUnit);
        return goodsServicesDao.save(goodsServices);
    }

    public GoodsServices updateGoodsServices(GoodsServices goodsServices) {
        return goodsServicesDao.save(goodsServices);
    }

    public GoodsServices getById(Long id) {
        return goodsServicesDao.findById(id).get();
    }

    public void deleteById(Long id) {
        goodsServicesDao.deleteById(id);
    }

    public Integer getNextNumber(String company) {
        AccountingUnit byName = accUnitDao.findByName(company);
        byName.setGoodsServiceNumber(byName.getGoodsServiceNumber() + 1);
        AccountingUnit save = accUnitDao.save(byName);
        return Integer.valueOf(save.getGoodsServiceNumber());
    }
}
