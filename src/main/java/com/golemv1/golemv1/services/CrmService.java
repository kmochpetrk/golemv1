package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.CrmDao;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.Crm;
import com.golemv1.golemv1.entities.GlAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class CrmService {

    @Autowired
    private CrmDao crmDao;

    @Autowired
    private AccUnitDao accUnitDao;

    public Crm createCrm(Crm crm, String company) {
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        crm.setAccountingUnit(accountingUnit);
        return crmDao.save(crm);
    }

    public Crm updateCrm(Crm crm, String company) {
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        crm.setAccountingUnit(accountingUnit);
        return crmDao.save(crm);
    }

    public Crm getById(Long id) {
        return crmDao.findById(id).get();
    }

    public void deleteById(Long id) {
        crmDao.deleteById(id);
    }
}
