package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.*;
import com.golemv1.golemv1.dtos.OutputCashReceiptDto;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.GoodsInputReceipt;
import com.golemv1.golemv1.entities.OutputCashReceipt;
import com.golemv1.golemv1.mappers.MainMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;

@Service
@Transactional
public class OutputCashReceiptService {

    @Autowired
    private OutputCashReceiptDao outputCashReceiptDao;

    @Autowired
    private MainMapper mainMapper;

    @Autowired
    private AccUnitDao accUnitDao;

    @Autowired
    private GlWriteDao glWriteDao;

    @Autowired
    private GoodsServicesItemDao goodsServicesItemDao;

    @Autowired
    private GoodsInputReceiptDao goodsInputReceiptDao;

    public int getNextNumber(String company) {
        AccountingUnit byName = accUnitDao.findByName(company);
        byName.setOutputCashReceiptNumber(byName.getOutputCashReceiptNumber() + 1);
        AccountingUnit save = accUnitDao.save(byName);
        return Integer.valueOf(save.getOutputCashReceiptNumber());
    }

    public OutputCashReceipt createOutputCashReceipt(OutputCashReceiptDto outputCashReceiptDto, String company) {
        final AccountingUnit accountingUnit = accUnitDao.findByName(company);
        final OutputCashReceipt outputCashReceipt = new OutputCashReceipt();
        outputCashReceipt.setAccountingUnit(accountingUnit);
        outputCashReceipt.setDocumentNumber(outputCashReceiptDto.getDocumentNumber());
        outputCashReceipt.setGlWrites(mainMapper.mapGlWrites(outputCashReceiptDto.getGlWrites()));
        outputCashReceipt.setAccountingDate(LocalDate.now());
        outputCashReceipt.setDescriptionOfCase("output cash receipt");
        outputCashReceipt.setOgrId(outputCashReceiptDto.getGORIds() != null && !outputCashReceiptDto.getGORIds().isEmpty() ? outputCashReceiptDto.getGORIds().iterator().next() : null);
        outputCashReceipt.setCashierTitle(outputCashReceiptDto.getCashierTitle());
        outputCashReceipt.setGoodsServiceItems(mainMapper.mapGoodsServiceItems(outputCashReceiptDto.getWarehouseGoodsDtos(), true, false));
        OutputCashReceipt outputCashReceiptSaved = outputCashReceiptDao.save(outputCashReceipt);
        if (outputCashReceiptDto.getGORIds() != null && !outputCashReceiptDto.getGORIds().isEmpty()) {
            Long next = outputCashReceiptDto.getGORIds().iterator().next();
            GoodsInputReceipt byId = goodsInputReceiptDao.findById(next).get();
            byId.setOcrId(outputCashReceiptSaved.getId());
            goodsInputReceiptDao.save(byId);
        }
        outputCashReceipt.getGlWrites().forEach(item -> {
            item.setGlWholeWrite(outputCashReceiptSaved);
            glWriteDao.save(item);
        });
        outputCashReceipt.getGoodsServiceItems().forEach(item -> {
            item.setGlWholeWrite(outputCashReceiptSaved);
            goodsServicesItemDao.save(item);
        });
        return outputCashReceiptSaved;
    }
}
