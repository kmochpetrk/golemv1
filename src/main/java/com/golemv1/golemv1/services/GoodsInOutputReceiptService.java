package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.*;
import com.golemv1.golemv1.dtos.GoodsInputReceiptDto;
import com.golemv1.golemv1.dtos.GoodsOutputReceiptDto;
import com.golemv1.golemv1.dtos.InputInvoiceDto;
import com.golemv1.golemv1.dtos.OutputInvoiceDto;
import com.golemv1.golemv1.entities.*;
import com.golemv1.golemv1.mappers.MainMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class GoodsInOutputReceiptService {
    @Autowired
    private GoodsInputReceiptDao goodsInputReceiptDao;

    @Autowired
    private GoodsServicesDao goodsServicesDao;

    @Autowired
    private WarehouseGoodsServicesDao warehouseGoodsServicesDao;

    @Autowired
    private GlWholeWritingDao glWholeWritingDao;

    @Autowired
    private GlWriteDao glWriteDao;

    @Autowired
    private MainMapper mainMapper;

    @Autowired
    private AccUnitDao accUnitDao;

    @Autowired
    private GoodsServicesItemDao goodsServicesItemDao;

    @Autowired
    private GoodsOutputReceiptDao goodsOutputReceiptDao;

    @Autowired
    private OutputInvoicesDao outputInvoicesDao;

    @Autowired
    private CrmDao crmDao;

    @Autowired
    private InputInvoiceDao inputInvoicesDao;

    public GoodsInputReceipt createGoodsInputReceipt(GoodsInputReceiptDto goodsInputReceiptDto, String company) {
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        final GoodsInputReceipt goodsInputReceipt = new GoodsInputReceipt();
        goodsInputReceipt.setDocumentNumber(goodsInputReceiptDto.getDocumentNumber());
        goodsInputReceipt.setCrm(crmDao.findById(goodsInputReceiptDto.getCrmId()).get());
        goodsInputReceipt.setAccountingUnit(accountingUnit);
        goodsInputReceipt.setDescriptionOfCase("Goods input");
        goodsInputReceipt.setGlWrites(mainMapper.mapGlWrites(goodsInputReceiptDto.getGlWrites()));
        goodsInputReceipt.setAccountingDate(LocalDate.now());
        goodsInputReceipt.setGoodsServiceItems(mainMapper.mapGoodsServiceItems(goodsInputReceiptDto.getWarehouseGoodsDtos(), true, true));
        GoodsInputReceipt save = goodsInputReceiptDao.save(goodsInputReceipt);
        goodsInputReceipt.getGlWrites().forEach(item -> {
            item.setGlWholeWrite(save);
            glWriteDao.save(item);
        });
        goodsInputReceipt.getGoodsServiceItems().forEach(item -> {
            item.setGlWholeWrite(save);
            goodsServicesItemDao.save(item);
        });
        return save;
    }

    public GoodsOutputReceipt createGoodsOutputReceipt(GoodsOutputReceiptDto goodsOutputReceiptDto, String company) {
        final AccountingUnit accountingUnit = accUnitDao.findByName(company);
        final GoodsOutputReceipt goodsOutputReceipt = new GoodsOutputReceipt();
        goodsOutputReceipt.setAccountingUnit(accountingUnit);
        goodsOutputReceipt.setDocumentNumber(goodsOutputReceiptDto.getDocumentNumber());
        goodsOutputReceipt.setCrm(crmDao.findById(goodsOutputReceiptDto.getCrmId()).get());
        goodsOutputReceipt.setDescriptionOfCase("Goods output");
        goodsOutputReceipt.setGlWrites(mainMapper.mapGlWrites(goodsOutputReceiptDto.getGlWrites()));
        goodsOutputReceipt.setAccountingDate(LocalDate.now());
        goodsOutputReceipt.setGoodsServiceItems(mainMapper.mapGoodsServiceItems(goodsOutputReceiptDto.getWarehouseGoodsDtos(), false, true));
        GoodsOutputReceipt save = goodsOutputReceiptDao.save(goodsOutputReceipt);
        goodsOutputReceipt.getGlWrites().forEach(item -> {
            item.setGlWholeWrite(save);
            glWriteDao.save(item);
        });
        goodsOutputReceipt.getGoodsServiceItems().forEach(item -> {
            item.setGlWholeWrite(save);
            goodsServicesItemDao.save(item);
        });
        return save;
    }

    public OutputInvoice createOutputInvoice(OutputInvoiceDto outputInvoiceDto, String company) {
        final AccountingUnit accountingUnit = accUnitDao.findByName(company);
        final OutputInvoice outputInvoice = new OutputInvoice();
        outputInvoice.setAccountingUnit(accountingUnit);
        outputInvoice.setPaymentType(outputInvoiceDto.getPaymentType());
        outputInvoice.setTransportType(outputInvoiceDto.getTransportType());
        outputInvoice.setCrm(crmDao.findById(outputInvoiceDto.getCrmId()).get());
        outputInvoice.setDocumentNumber(outputInvoiceDto.getDocumentNumber());
        outputInvoice.setGlWrites(mainMapper.mapGlWrites(outputInvoiceDto.getGlWrites()));
        outputInvoice.setDescriptionOfCase("output invoice goods services");
        outputInvoice.setAccountingDate(LocalDate.now());
        outputInvoice.setPayDate(outputInvoiceDto.getPayDate());
        outputInvoice.setGoodsServiceItems(mainMapper.mapGoodsServiceItems(outputInvoiceDto.getWarehouseGoodsDtos(), true, false));
        if (outputInvoiceDto.getGORIds() != null) {
            for (Long id : outputInvoiceDto.getGORIds()) {
                GoodsOutputReceipt goodsOutputReceipt = goodsOutputReceiptDao.findById(id).get();
                if (outputInvoice.getGoodsOutputReceipts() == null) {
                    outputInvoice.setGoodsOutputReceipts(new HashSet<>());
                }
                outputInvoice.getGoodsOutputReceipts().add(goodsOutputReceipt);
            }
        }
        OutputInvoice outputInvoiceSaved = outputInvoicesDao.save(outputInvoice);
        outputInvoice.getGlWrites().forEach(item -> {
            item.setGlWholeWrite(outputInvoiceSaved);
            glWriteDao.save(item);
        });
        outputInvoice.getGoodsServiceItems().forEach(item -> {
            item.setGlWholeWrite(outputInvoiceSaved);
            goodsServicesItemDao.save(item);
        });
        Optional.ofNullable(outputInvoice.getGoodsOutputReceipts()).orElse(new HashSet<>()).forEach(item -> {
            item.setOutputInvoice(outputInvoiceSaved);
            goodsOutputReceiptDao.save(item);
        });
        return outputInvoiceSaved;
    }

    public GoodsInputReceipt updateGoodsInputReceipt(GoodsInputReceipt goodsInputReceipt) {
        return goodsInputReceiptDao.save(goodsInputReceipt);
    }

    public GoodsInputReceipt getById(Long id) {
        return goodsInputReceiptDao.findById(id).get();
    }

    public void deleteById(Long id) {
        goodsInputReceiptDao.deleteById(id);
    }

    public Page<GoodsInputReceipt> findAllByAccountingUnit(AccountingUnit byName, Pageable pageable) {
        return goodsInputReceiptDao.findAllByAccountingUnit(byName, pageable);
    }

    public Page<GoodsOutputReceipt> findAllGoodsOutputReceipts(Pageable pageable, String company) {
        AccountingUnit byName = accUnitDao.findByName(company);
        return goodsOutputReceiptDao.findAllByAccountingUnit(byName, pageable);
    }

    public Page<GoodsOutputReceipt> findAllNotInvoicedGoodsOutputReceipts(Pageable pageable, String company) {
        AccountingUnit byName = accUnitDao.findByName(company);
        return goodsOutputReceiptDao.findAllByAccountingUnitAndOutputInvoiceNullAndIcrIdNull(byName, pageable);
    }

    public Page<GoodsInputReceipt> findAllNotInvoicedGoodsInputReceipts(Pageable pageable, String company) {
        AccountingUnit byName = accUnitDao.findByName(company);
        return goodsInputReceiptDao.findAllByAccountingUnitAndInputInvoiceNullAndOcrIdNull(byName, pageable);
    }

    //@Transactional(propagation = Propagation.REQUIRES_NEW)
    public Integer getNextNumberOutputGoodsReceipt(String company) {
        AccountingUnit byName = accUnitDao.findByName(company);
        byName.setNextGoodsOutputReceiptNumber(byName.getNextGoodsOutputReceiptNumber() + 1);
        AccountingUnit save = accUnitDao.save(byName);
        return Integer.valueOf(save.getNextGoodsOutputReceiptNumber());
    }

    public Integer getNextNumberInputGoodsReceipt(String company) {
        AccountingUnit byName = accUnitDao.findByName(company);
        byName.setNextGoodsInputReceiptNumber(byName.getNextGoodsInputReceiptNumber() + 1);
        AccountingUnit save = accUnitDao.save(byName);
        return Integer.valueOf(save.getNextGoodsInputReceiptNumber());
    }

    public InputInvoice createInputInvoice(InputInvoiceDto inputInvoiceDto, String company) {
        final AccountingUnit accountingUnit = accUnitDao.findByName(company);
        final InputInvoice inputInvoice = new InputInvoice();
        inputInvoice.setAccountingUnit(accountingUnit);
        inputInvoice.setVariableSymbol(inputInvoiceDto.getVariableSymbol());
        inputInvoice.setCrm(crmDao.findById(inputInvoiceDto.getCrmId()).get());
        inputInvoice.setDocumentNumber(inputInvoiceDto.getDocumentNumber());
        inputInvoice.setPaymentType(inputInvoiceDto.getPaymentType());
        inputInvoice.setBankAccount(inputInvoiceDto.getBankAccount());
        inputInvoice.setTransportType(inputInvoiceDto.getTransportType());
        inputInvoice.setDescriptionOfCase("input invoice goods services");
        inputInvoice.setGlWrites(mainMapper.mapGlWrites(inputInvoiceDto.getGlWrites()));
        inputInvoice.setAccountingDate(LocalDate.now());
        inputInvoice.setPayDate(inputInvoiceDto.getPayDate());
        inputInvoice.setGoodsServiceItems(mainMapper.mapGoodsServiceItems(inputInvoiceDto.getWarehouseGoodsDtos(), true, false));
        if (inputInvoiceDto.getGorIds() != null) {
            for (Long id : inputInvoiceDto.getGorIds()) {
                GoodsInputReceipt goodsInputReceipt = goodsInputReceiptDao.findById(id).get();
                if (inputInvoice.getGoodsInputReceipts() == null) {
                    inputInvoice.setGoodsInputReceipts(new HashSet<>());
                }
                inputInvoice.getGoodsInputReceipts().add(goodsInputReceipt);
            }
        }
        InputInvoice inputInvoiceSaved = inputInvoicesDao.save(inputInvoice);
        inputInvoice.getGlWrites().forEach(item -> {
            item.setGlWholeWrite(inputInvoiceSaved);
            glWriteDao.save(item);
        });
        inputInvoice.getGoodsServiceItems().forEach(item -> {
            item.setGlWholeWrite(inputInvoiceSaved);
            goodsServicesItemDao.save(item);
        });
        Optional.ofNullable(inputInvoice.getGoodsInputReceipts()).orElse(new HashSet<>()).forEach(item -> {
            item.setInputInvoice(inputInvoiceSaved);
            goodsInputReceiptDao.save(item);
        });
        return inputInvoiceSaved;
    }
}
