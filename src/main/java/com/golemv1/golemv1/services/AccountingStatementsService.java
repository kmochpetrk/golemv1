package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.GlAccountDao;
import com.golemv1.golemv1.daos.GlWholeWritingDao;
import com.golemv1.golemv1.daos.GlWriteDao;
import com.golemv1.golemv1.dtos.AccountingStatementItemDto;
import com.golemv1.golemv1.entities.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class AccountingStatementsService {

    @Autowired
    private GlAccountDao glAccountDao;

    @Autowired
    private AccUnitDao accUnitDao;

    @Autowired
    private GlWholeWritingDao glWholeWritingDao;

    public List<AccountingStatementItemDto> getBalanceSheet(String company) {
        AccountingUnit accountingUnit = accUnitDao.findByName(company);

        List<GlWholeWrite> allByAccountingUnitEquals = glWholeWritingDao.findAllByAccountingUnitOrderByAccountingDateDesc(accountingUnit).stream()
                .filter(a -> (a.getCancelled() == null || !a.getCancelled())).collect(Collectors.toList());
        List<GlWrite> collect1 = allByAccountingUnitEquals.stream().flatMap(e -> e.getGlWrites().stream().filter(f
                -> (f.getGlAccount().getGlAccountType() == GlAccountType.BALANCE ||
                f.getGlAccount().getGlAccountType() == GlAccountType.CALCULATED))).collect(Collectors.toList());

        List<GlWrite> collect2 = allByAccountingUnitEquals.stream().flatMap(e -> e.getGlWrites().stream().filter(f
                -> (f.getGlAccount().getGlAccountType() == GlAccountType.BALANCE ||
                f.getGlAccount().getGlAccountType() == GlAccountType.CALCULATED))).collect(Collectors.toList());

        List<AccountingStatementItemDto> resultWhole = new ArrayList<>();

        Map<String, BigDecimal> collectDedit = collect1.stream().filter(u -> u.getGlAccountSideType() == GlAccountSideType.DEBIT)
                .collect(Collectors.groupingBy(a -> a.getGlAccount().getAcctNo(),
                        Collectors.mapping(d -> d.getAmount() != null ? d.getAmount() : BigDecimal.ZERO, Collectors.reducing(BigDecimal.ZERO, BigDecimal::add))));
        Map<String, BigDecimal> collectCredit = collect2.stream().filter(u -> u.getGlAccountSideType() == GlAccountSideType.CREDIT)
                .collect(Collectors.groupingBy(a -> a.getGlAccount().getAcctNo(),
                        Collectors.mapping(d -> d.getAmount() != null ? d.getAmount() : BigDecimal.ZERO, Collectors.reducing(BigDecimal.ZERO, BigDecimal::add))));


        List<GlAccount> allAccounts = glAccountDao.findAllByAccountingUnit(accountingUnit).stream()
                .filter(a -> (a.getGlAccountType() == GlAccountType.CALCULATED || a.getGlAccountType() == GlAccountType.BALANCE)).collect(Collectors.toList());

        allAccounts.stream().forEach(a -> {
            resultWhole.add(new AccountingStatementItemDto(a.getAcctNo(), collectDedit.get(a.getAcctNo()), collectCredit.get(a.getAcctNo())));
        });

        return resultWhole;
    }

    public List<AccountingStatementItemDto> getProfitAndLoss(String company) {
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        List<GlWholeWrite> allByAccountingUnitEquals = glWholeWritingDao.findAllByAccountingUnitOrderByAccountingDateDesc(accountingUnit)
                .stream().filter(a -> (a.getCancelled() == null || !a.getCancelled())).collect(Collectors.toList());
        List<GlWrite> collect1 = allByAccountingUnitEquals.stream().flatMap(e -> e.getGlWrites().stream().filter(f
                -> (f.getGlAccount().getGlAccountType() == GlAccountType.PROFIT_LOSS))).collect(Collectors.toList());

        List<GlWrite> collect2 = allByAccountingUnitEquals.stream().flatMap(e -> e.getGlWrites().stream().filter(f
                -> (f.getGlAccount().getGlAccountType() == GlAccountType.PROFIT_LOSS))).collect(Collectors.toList());

        List<AccountingStatementItemDto> resultWhole = new ArrayList<>();

        Map<String, BigDecimal> collectDedit = collect1.stream().filter(u -> u.getGlAccountSideType() == GlAccountSideType.DEBIT)
                .collect(Collectors.groupingBy(a -> a.getGlAccount().getAcctNo(),
                        Collectors.mapping(d -> d.getAmount() != null ? d.getAmount() : BigDecimal.ZERO, Collectors.reducing(BigDecimal.ZERO, BigDecimal::add))));
        Map<String, BigDecimal> collectCredit = collect2.stream().filter(u -> u.getGlAccountSideType() == GlAccountSideType.CREDIT)
                .collect(Collectors.groupingBy(a -> a.getGlAccount().getAcctNo(),
                        Collectors.mapping(d -> d.getAmount() != null ? d.getAmount() : BigDecimal.ZERO, Collectors.reducing(BigDecimal.ZERO, BigDecimal::add))));


        List<GlAccount> allAccounts = glAccountDao.findAllByAccountingUnit(accountingUnit).stream()
                .filter(a -> (a.getGlAccountType() == GlAccountType.PROFIT_LOSS)).collect(Collectors.toList());

        allAccounts.stream().forEach(a -> {
            resultWhole.add(new AccountingStatementItemDto(a.getAcctNo(), collectDedit.get(a.getAcctNo()), collectCredit.get(a.getAcctNo())));
        });

        return resultWhole;
    }
}
