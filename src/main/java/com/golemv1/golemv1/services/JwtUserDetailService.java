package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.CompanyUserDao;
import com.golemv1.golemv1.entities.CompanyUser;
import com.golemv1.golemv1.entities.RoleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class JwtUserDetailService implements UserDetailsService {

    @Autowired
    private CompanyUserDao companyUserDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<GrantedAuthority> authorityList = AuthorityUtils.createAuthorityList(RoleType.Financial_Accountant.name(), RoleType.Main_Accountant.name());
        //todo load by username from repo
        String[] split = username.split(":");
        List<CompanyUser> allByCompanyUser = companyUserDao.findAllByCompanyAndUsername(split[0], split[1]);
        if (allByCompanyUser != null && !allByCompanyUser.isEmpty()) {
            return new User(username, allByCompanyUser.get(0).getPassword(), authorityList);
        }
        UserDetails userDetails = new User("golemv1:petrkmoch", "password", authorityList);
        return userDetails;
    }
}
