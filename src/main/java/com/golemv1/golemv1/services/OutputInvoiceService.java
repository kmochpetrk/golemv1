package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.OutputInvoicesDao;
import com.golemv1.golemv1.entities.AccountingUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class OutputInvoiceService {

    @Autowired
    private OutputInvoicesDao outputInvoicesDao;

    @Autowired
    private AccUnitDao accUnitDao;

    public Integer getNextNumber(String company) {
        AccountingUnit byName = accUnitDao.findByName(company);
        byName.setOutputInvoiceNumber(byName.getOutputInvoiceNumber() + 1);
        AccountingUnit save = accUnitDao.save(byName);
        return Integer.valueOf(save.getOutputInvoiceNumber());
    }
}
