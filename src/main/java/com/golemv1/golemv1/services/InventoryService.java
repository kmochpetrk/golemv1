package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.GlWriteDao;
import com.golemv1.golemv1.daos.InternalDocumentDao;
import com.golemv1.golemv1.daos.InventoryItemDao;
import com.golemv1.golemv1.dtos.AmortizationRequestDto;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.DepreciationItem;
import com.golemv1.golemv1.entities.InternalDocument;
import com.golemv1.golemv1.entities.InventoryItem;
import com.golemv1.golemv1.mappers.MainMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service
@Transactional
public class InventoryService {

    @Autowired
    private InventoryItemDao inventoryItemDao;

    @Autowired
    private AccUnitDao accUnitDao;

    @Autowired
    private InternalDocumentService internalDocumentService;

    @Autowired
    private MainMapper mainMapper;

    @Autowired
    private InternalDocumentDao internalDocumentDao;

    @Autowired
    private GlWriteDao glWriteDao;

    public Page<InventoryItem> getInventoryItems(AccountingUnit accountingUnit, Pageable pageable) {
        return inventoryItemDao.findAllByAccountingUnit(accountingUnit, pageable);
    }

    public InventoryItem createInventory(InventoryItem inventoryItem, String company) {
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        inventoryItem.setAccountingUnit(accountingUnit);
        return inventoryItemDao.save(inventoryItem);
    }

    public int getNextNumber(String company) {
        AccountingUnit byName = accUnitDao.findByName(company);
        byName.setInventoryNumber(byName.getInventoryNumber() + 1);
        AccountingUnit save = accUnitDao.save(byName);
        return Integer.valueOf(save.getInventoryNumber());
    }

    public InventoryItem processAmortization(AmortizationRequestDto amortizationRequestDto, String company) {
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        InternalDocument internalDocument = new InternalDocument();
        internalDocument.setDocumentNumber("ID-" + internalDocumentService.getNextNumber(company));
        internalDocument.setAccountingDate(LocalDate.now());
        internalDocument.setAccountingUnit(accountingUnit);
        internalDocument.setTaxDate(LocalDate.now());
        internalDocument.setGlWrites(mainMapper.mapGlWrites(amortizationRequestDto.getAccounting()));

        final InternalDocument savedInternalDoc = internalDocumentDao.save(internalDocument);

        internalDocument.getGlWrites().forEach(item -> {
            item.setGlWholeWrite(savedInternalDoc);
            glWriteDao.save(item);
        });

        final List<InventoryItem> savedInventoryItems = new ArrayList<>();

        amortizationRequestDto.getDepreciations().forEach(a -> {
            InventoryItem inventoryItem = inventoryItemDao.findById(a.getInventoryItemId()).get();
            if (inventoryItem.getDepreciationItems() == null) {
                inventoryItem.setDepreciationItems(new HashSet<>());
            }

            DepreciationItem depreciationItem = new DepreciationItem();
            depreciationItem.setAmount(a.getAmount());
            depreciationItem.setBookingNumber(savedInternalDoc.getDocumentNumber());
            depreciationItem.setInventoryItem(inventoryItem);
            depreciationItem.setPeriod(a.getPeriod());
            inventoryItem.getDepreciationItems().add(depreciationItem);
            InventoryItem save = inventoryItemDao.save(inventoryItem);
            savedInventoryItems.add(save);

        });
        return savedInventoryItems.get(0);
    }
}
