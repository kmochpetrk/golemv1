package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.EshopOrderDao;
import com.golemv1.golemv1.dtos.InternalOrderDto;
import com.golemv1.golemv1.dtos.InternalOrderItemDto;
import com.golemv1.golemv1.entities.EshopOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;

@Service
@Transactional
public class InternalOrderService {

    @Autowired
    private EshopOrderDao eshopOrderDao;

    public Page<InternalOrderDto> getOrders(Pageable pageable, String company) {
        Page<InternalOrderDto> internalOrderDtoStream = eshopOrderDao.findAllByCompany(company, pageable).map(InternalOrderService::convert);
        return internalOrderDtoStream;
    }

    public static InternalOrderDto convert(EshopOrder a) {
        InternalOrderDto internalOrderDto = new InternalOrderDto();
        internalOrderDto.setCompany(a.getCompany());
        internalOrderDto.setCrmIco(a.getCrm().getIco());
        internalOrderDto.setCrmId(a.getCrm().getId());
        internalOrderDto.setCrmName(a.getCrm().getName());
        internalOrderDto.setAddress(a.getCrm().getPrimaryAddress());
        internalOrderDto.setDeliveryType(a.getDeliveryType());
        internalOrderDto.setId(a.getId());
        internalOrderDto.setOinumber(a.getOINumber());
        internalOrderDto.setOurBankAccount(a.getOurBankAccount());
        internalOrderDto.setTotalPrice(a.getTotalPrice());
        internalOrderDto.setTotalVat(a.getTotalVat());
        internalOrderDto.setPaymentType(a.getPaymentType());
        internalOrderDto.setOrdersItems(new HashSet<>());
        a.getOrdersItems().stream().forEach(b -> {
            InternalOrderItemDto internalOrderItemDto = new InternalOrderItemDto();
            internalOrderItemDto.setGoodsServicesId(b.getGoodsServices().getId());
            internalOrderItemDto.setGoodsServicesName(b.getGoodsServices().getName());
            internalOrderItemDto.setGoodsServicesNumber(b.getGoodsServices().getGoodsServicesNumber());
            internalOrderItemDto.setId(b.getId());
            internalOrderItemDto.setOrderedCount(b.getOrderedCount());
            internalOrderItemDto.setTotalItemPrice(b.getTotalItemPrice());
            internalOrderItemDto.setTotalItemVat(b.getTotalItemVat());
            internalOrderItemDto.setUnitOutputPrice(b.getUnitOutputPrice());
            internalOrderItemDto.setUnitOutputPriceWithVat(b.getUnitOutputPriceWithVat());
            internalOrderItemDto.setVatPercent(b.getVatPercent());
            internalOrderItemDto.setWarehouseAddress(b.getWarehouse().getAddress() != null ? b.getWarehouse().getAddress().getStreet() + " "
                    + b.getWarehouse().getAddress().getDescNumber() + ", "
                    + b.getWarehouse().getAddress().getCity() + ", " + b.getWarehouse().getAddress().getZipCode() : "");
            internalOrderItemDto.setWarehouseId(b.getWarehouse().getId());
            internalOrderItemDto.setWarehouseName(b.getWarehouse().getName());
            internalOrderDto.getOrdersItems().add(internalOrderItemDto);
        });
        return internalOrderDto;
    }
}
