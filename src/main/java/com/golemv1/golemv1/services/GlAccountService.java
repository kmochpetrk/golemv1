package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.GlAccountDao;
import com.golemv1.golemv1.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class GlAccountService {

    @Autowired
    private GlAccountDao glAccountDao;

    @Autowired
    private AccUnitDao accUnitDao;

    public Page<GlAccount> getAllAccounts(Pageable pageable, AccountingUnit accountingUnit) {
        return glAccountDao.findAllByAccountingUnit(accountingUnit, pageable);
    }

    public void createAccounts() {
        GlAccount glAccount = new GlAccount();
        glAccount.setAcctNo("221100");
        glAccount.setGlAccountSideType(GlAccountSideType.DEBIT);
        glAccount.setGlAccountType(GlAccountType.BALANCE);
        glAccount.setDescription("Bank account");
        glAccountDao.save(glAccount);

        GlAccount glAccount1 = new GlAccount();
        glAccount1.setAcctNo("601100");
        glAccount1.setGlAccountSideType(GlAccountSideType.DEBIT);
        glAccount1.setGlAccountType(GlAccountType.BALANCE);
        glAccount1.setDescription("gains from goods sales");
        glAccountDao.save(glAccount1);
    }

    public GlAccount createAccount(GlAccount glAccount, String company) {
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        glAccount.setAccountingUnit(accountingUnit);
        return glAccountDao.save(glAccount);
    }

    public GlAccount updateAccount(GlAccount glAccount) {
        return glAccountDao.save(glAccount);
    }

    public GlAccount getById(Long id) {
        return glAccountDao.findById(id).get();
    }

    public void deleteById(Long id) {
        glAccountDao.deleteById(id);
    }

    public Set<GlAccount> getByGroup(String company, GlAccountGroupingType glAccountGroupingType) {
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        Set<GlAccount> allByAccountingUnitAndGlAccountGroupingType = glAccountDao.findAllByAccountingUnitAndGlAccountGroupingType(accountingUnit, glAccountGroupingType);
        return allByAccountingUnitAndGlAccountGroupingType;
    }
}
