package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.*;
import com.golemv1.golemv1.entities.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@Slf4j
public class CompanyUserService {

    public static final String DEFAULT_ICO_DIC = "default";
    @Autowired
    private CompanyUserDao companyUserDao;

    @Autowired
    private AccUnitDao accUnitDao;
    
    @Autowired
    private CrmDao crmDao;

    @Autowired
    private GlAccountDao glAccountDao;

    public CompanyUser createCompanyUser(CompanyUser companyUser) {
        CompanyUser save = companyUserDao.save(companyUser);
        // create accountingUnit
        AccountingUnit accountingUnit = new AccountingUnit();
        accountingUnit.setName(companyUser.getCompany());
        accountingUnit.setDic(DEFAULT_ICO_DIC);
        accountingUnit.setIco(DEFAULT_ICO_DIC);
        AccountingUnit savedAccountingUnit = accUnitDao.save(accountingUnit);
        Crm crm = new Crm();
        crm.setName(companyUser.getCompany());
        crm.setAccountingUnit(savedAccountingUnit);
        crm.setCrmType(CrmType.BOTH);
        crm.setDic(DEFAULT_ICO_DIC);
        crm.setIco(DEFAULT_ICO_DIC);
        Address address = new Address();
        address.setZipCode("19900");
        address.setStreet("Hluckova");
        address.setDescNumber("22");
        address.setCity("Prague");
        crm.setPrimaryAddress(address);
        crmDao.save(crm);

        // 504100
        createAccount("504100", GlAccountSideType.DEBIT, GlAccountType.PROFIT_LOSS, GlAccountGroupingType.EXPENSES_GOODS,
                accountingUnit, "Expenses goods", "");

        // 131100
        createAccount("131100", GlAccountSideType.DEBIT, GlAccountType.CALCULATED, GlAccountGroupingType.GOODS_GET,
                accountingUnit, "goods buy", "");

        // 132100
        createAccount("132100", GlAccountSideType.DEBIT, GlAccountType.BALANCE, GlAccountGroupingType.GOODS,
                accountingUnit, "goods", "");

        // 521100
        createAccount("521100", GlAccountSideType.DEBIT, GlAccountType.PROFIT_LOSS, GlAccountGroupingType.EXPENSES_EMPLOYEES,
                accountingUnit, "expenses employees", "");

        // 311100
        createAccount("311100", GlAccountSideType.DEBIT, GlAccountType.BALANCE, GlAccountGroupingType.TRADE_OTHER_RECEIVABLES,
                accountingUnit, "trade receivables", "");

        // 321100
        createAccount("321100", GlAccountSideType.CREDIT, GlAccountType.BALANCE, GlAccountGroupingType.TRADE_OTHER_PAYABLES,
                accountingUnit, "trade payables", "");

        // 221100
        createAccount("221100", GlAccountSideType.DEBIT, GlAccountType.BALANCE, GlAccountGroupingType.BANK_ACCOUNTS,
                accountingUnit, "bank account Bank_1", "123456789/9999");

        // 201100
        createAccount("201100", GlAccountSideType.DEBIT, GlAccountType.BALANCE, GlAccountGroupingType.CASH_AND_EQUIVALENTS,
                accountingUnit, "cashier a", "Cashier_1");

        // 201100
        createAccount("201100", GlAccountSideType.DEBIT, GlAccountType.BALANCE, GlAccountGroupingType.CASH_AND_EQUIVALENTS,
                accountingUnit, "cashier a", "Cashier_1");

        // 604100
        createAccount("604100", GlAccountSideType.CREDIT, GlAccountType.PROFIT_LOSS, GlAccountGroupingType.REVENUE_GOODS,
                accountingUnit, "revenue goods", "");

        // 042100
        createAccount("042100", GlAccountSideType.DEBIT, GlAccountType.CALCULATED, GlAccountGroupingType.PROPERTY_PLANT_EQUIPMENT,
                accountingUnit, "plant equipment buy", "");

        // 022100
        createAccount("022100", GlAccountSideType.DEBIT, GlAccountType.BALANCE, GlAccountGroupingType.PROPERTY_PLANT_EQUIPMENT_IN_USE,
                accountingUnit, "plant equipment", "");

        // 551100
        createAccount("551100", GlAccountSideType.DEBIT, GlAccountType.PROFIT_LOSS, GlAccountGroupingType.AMORTIZATION,
                accountingUnit, "tangible amortization", "");

        // 081100
        createAccount("081100", GlAccountSideType.CREDIT, GlAccountType.BALANCE, GlAccountGroupingType.DEPRECIATION,
                accountingUnit, "tangible depreciation", "");

        // 502100
        createAccount("502100", GlAccountSideType.DEBIT, GlAccountType.PROFIT_LOSS, GlAccountGroupingType.EXPENSES_SERVICES_WATER,
                accountingUnit, "expenses water", "");

        // 502200
        createAccount("502200", GlAccountSideType.DEBIT, GlAccountType.PROFIT_LOSS, GlAccountGroupingType.EXPENSES_GAS,
                accountingUnit, "expenses gas", "");

        // 502300
        createAccount("502300", GlAccountSideType.DEBIT, GlAccountType.PROFIT_LOSS, GlAccountGroupingType.EXPENSES_SERVICES_ELECTRIC,
                accountingUnit, "expenses electric", "");

        // 343100
        createAccount("343100", GlAccountSideType.CREDIT, GlAccountType.BALANCE, GlAccountGroupingType.VAT,
                accountingUnit, "vat", "");

        // 331100
        createAccount("331100", GlAccountSideType.CREDIT, GlAccountType.BALANCE, GlAccountGroupingType.EMPLOYEES,
                accountingUnit, "Employees", "");
        return save;
    }


    private GlAccount createAccount(String acctNo, GlAccountSideType glAccountSideType,
                               GlAccountType glAccountType, GlAccountGroupingType glAccountGroupingType,
                               AccountingUnit accountingUnit, String description, String additionalInfo) {
        final GlAccount glAccount = new GlAccount();
        glAccount.setAccountingUnit(accountingUnit);
        glAccount.setAcctNo(acctNo);
        glAccount.setGlAccountSideType(glAccountSideType);
        glAccount.setGlAccountType(glAccountType);
        glAccount.setDescription(description);
        glAccount.setAdditionalInfo(additionalInfo);
        glAccount.setGlAccountGroupingType(glAccountGroupingType);
        return glAccountDao.save(glAccount);
    }
}
