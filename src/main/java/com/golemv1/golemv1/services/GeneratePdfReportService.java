package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.CrmDao;
import com.golemv1.golemv1.daos.GoodsServicesDao;
import com.golemv1.golemv1.dtos.InputCashReceiptDto;
import com.golemv1.golemv1.dtos.WarehouseGoodsDto;
import com.golemv1.golemv1.entities.*;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
@Slf4j
public class GeneratePdfReportService {

    @Autowired
    private CrmDao crmDao;

    @Autowired
    private GoodsServicesDao goodsServicesDao;

    public ByteArrayInputStream outputInvoice(OutputInvoice outputInvoice, String ourCompany) throws DocumentException {


        Set<Crm> ourCrm = crmDao.findAllByNameIgnoreCase(ourCompany);
        Crm oneOurCrm = ourCrm.iterator().next();

        Crm otherCrm = outputInvoice.getCrm();

        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {

            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(80);
            table.setWidths(new int[]{1, 1});
            table.setSpacingBefore(10);

            Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

            PdfPCell pdfLeftIssuer = createPdfCell(oneOurCrm, "Issuer");
            PdfPCell pdfRightCustomer = createPdfCell(otherCrm, "Customer");

//            Phrase phraseLeft = new Phrase();
//            phraseLeft.add(listLeft);
//
//            Phrase phraseRight = new Phrase();
//            phraseRight.add(listRight);

            table.addCell(pdfLeftIssuer);
            table.addCell(pdfRightCustomer);


//            hcell = new PdfPCell(new Phrase("Amount", headFont));
//            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            table.addCell(hcell);
//
//            hcell = new PdfPCell(new Phrase("type", headFont));
//            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            table.addCell(hcell);

//            for (GlWrite glWrite : outputInvoice.getGlWrites()) {
//
//                PdfPCell cell;
//
//                cell = new PdfPCell(new Phrase(glWrite.getGlAccount().getAcctNo()));
//                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                table.addCell(cell);
//
//                cell = new PdfPCell(new Phrase(glWrite.getAmount().toString()));
//                cell.setPaddingLeft(5);
//                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//                table.addCell(cell);
//
//                cell = new PdfPCell(new Phrase(String.valueOf(glWrite.getGlAccount().getGlAccountSideType())));
//                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                cell.setPaddingRight(5);
//                table.addCell(cell);
//            }

            PdfWriter.getInstance(document, out);
            document.open();
            Paragraph output_invoice = new Paragraph("Output invoice");
            output_invoice.setAlignment(Element.ALIGN_CENTER);
            output_invoice.getFont().setStyle("bold");
            output_invoice.getFont().setSize(20);
            table.setPaddingTop(5f);
            document.add(output_invoice);
            document.add(table);

            PdfPTable tableLower = new PdfPTable(1);
            tableLower.setWidthPercentage(80);
            tableLower.setWidths(new int[]{1});
            tableLower.setSpacingBefore(10);
            PdfPCell pdfPCellLeftLower = createPdfCellMain(outputInvoice);
            tableLower.addCell(pdfPCellLeftLower);
            document.add(tableLower);


            document.close();
        } catch (Exception ex){
            log.error("Nastala chyba", ex);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }

    private PdfPCell createPdfCell(Crm oneOurCrm, String header) {
        PdfPCell pdfPCell = new PdfPCell();
        pdfPCell.addElement(new Paragraph(header));
        pdfPCell.addElement(new Paragraph("Name: " + oneOurCrm.getName()));
        pdfPCell.addElement(new Paragraph("Dic: " + oneOurCrm.getDic()));
        pdfPCell.addElement(new Paragraph("Ico: " + oneOurCrm.getIco()));
        pdfPCell.addElement(new Paragraph("Street: " + oneOurCrm.getPrimaryAddress().getStreet()));
        pdfPCell.addElement(new Paragraph("City: " + oneOurCrm.getPrimaryAddress().getCity()));
        pdfPCell.addElement(new Paragraph("Zip code: " + oneOurCrm.getPrimaryAddress().getZipCode()));
        pdfPCell.addElement(new Paragraph("Bank: " + oneOurCrm.getBankAccount()));
        return pdfPCell;
    }

    private PdfPCell createPdfCellMain(OutputInvoice outputInvoice) {
        PdfPCell pdfPCell = new PdfPCell();
        pdfPCell.addElement(new Paragraph("Issue Date: " + outputInvoice.getAccountingDate()));
        pdfPCell.addElement(new Paragraph("Pay Date: " + outputInvoice.getPayDate()));
        pdfPCell.addElement(new Paragraph("Payment type: " + (outputInvoice.getPaymentType() != null ? outputInvoice.getPaymentType() : "bank transfer")));
        pdfPCell.addElement(new Paragraph("Transport type: " + (outputInvoice.getTransportType() != null ? outputInvoice.getTransportType() : "")));
        pdfPCell.addElement(new Paragraph("Variable symbol: " + outputInvoice.getDocumentNumber()));
        ArrayList<GoodsServiceItems> goodsServiceItems = new ArrayList<>(outputInvoice.getGoodsServiceItems());
        BigDecimal total = BigDecimal.ZERO;
        BigDecimal totalVat = BigDecimal.ZERO;
        for (int i = 0; i < goodsServiceItems.size(); i++) {
            if (goodsServiceItems.get(i).getGoodsServices() != null) {
                pdfPCell.addElement(new Paragraph("Element #" + (i + 1) + ": " + goodsServiceItems.get(i).getGoodsServices().getName() + ": " + goodsServiceItems.get(i).getPriceTotal()));
            } else {
                pdfPCell.addElement(new Paragraph("Element #" + (i + 1) + ": " + goodsServiceItems.get(i).getWarehouseGoodsServices().getGoodsServices().getName() + ": " + goodsServiceItems.get(i).getPriceTotal()));
            }
            total = total.add(goodsServiceItems.get(i).getPriceTotal());
            totalVat = totalVat.add(goodsServiceItems.get(i).getVatTotal());
        }
        pdfPCell.addElement(new Paragraph("Total VAT: " + totalVat));
        pdfPCell.addElement(new Paragraph("Total: " + total));
        return pdfPCell;
    }

    public ByteArrayInputStream inputCashReceiptPdf(InputCashReceipt inputCashReceiptDto, String ourCompany) throws DocumentException {


        Set<Crm> ourCrm = crmDao.findAllByNameIgnoreCase(ourCompany);
        Crm oneOurCrm = ourCrm.iterator().next();

        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {

            PdfPTable table = new PdfPTable(1);
            table.setWidthPercentage(80);
            table.setWidths(new int[]{1});
            table.setSpacingBefore(10);
            Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
            PdfPCell pdfLeftIssuer = createPdfCell(oneOurCrm, "Issuer");
            table.addCell(pdfLeftIssuer);
            PdfWriter.getInstance(document, out);
            document.open();
            Paragraph headline = new Paragraph("Income cash receipt");
            headline.setAlignment(Element.ALIGN_CENTER);
            table.setPaddingTop(5f);
            document.add(headline);
            document.add(table);

            PdfPTable tableLower = new PdfPTable(1);
            tableLower.setWidthPercentage(80);
            tableLower.setWidths(new int[]{1});
            tableLower.setSpacingBefore(10);
            PdfPCell pdfPCellLeftLower = createPdfCellMainInputCashReceipt(inputCashReceiptDto);
            tableLower.addCell(pdfPCellLeftLower);
            document.add(tableLower);


            document.close();
        } catch (Exception ex){
            log.error("Nastala chyba", ex);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }

    private PdfPCell createPdfCellMainInputCashReceipt(InputCashReceipt inputCashReceiptDto) {
        PdfPCell pdfPCell = new PdfPCell();
        pdfPCell.addElement(new Paragraph("Issue Date: " + inputCashReceiptDto.getAccountingDate()));
        pdfPCell.addElement(new Paragraph("Input cash receipt number: " + inputCashReceiptDto.getDocumentNumber()));
        pdfPCell.addElement(new Paragraph("Cashier: " + inputCashReceiptDto.getCashierTitle()));

        ArrayList<GoodsServiceItems> goodsServiceItems = new ArrayList<>(inputCashReceiptDto.getGoodsServiceItems());
        BigDecimal total = BigDecimal.ZERO;
        BigDecimal totalVat = BigDecimal.ZERO;
        for (int i = 0; i < goodsServiceItems.size(); i++) {
            GoodsServiceItems goodsServiceItems1 = goodsServiceItems.get(i);
            pdfPCell.addElement(new Paragraph("Element #" + (i + 1) + ": " + goodsServiceItems1.getGoodsServices().getName() + ": " + goodsServiceItems1.getPriceTotal()));
            total = total.add(goodsServiceItems1.getPriceTotal());
            totalVat = totalVat.add(goodsServiceItems1.getVatTotal());
        }
        pdfPCell.addElement(new Paragraph("Total VAT: " + totalVat));
        pdfPCell.addElement(new Paragraph("Total: " + total));
        return pdfPCell;
    }

    private PdfPCell createPdfCellMainOutputCashReceipt(OutputCashReceipt outputCashReceiptDto) {
        PdfPCell pdfPCell = new PdfPCell();
        pdfPCell.addElement(new Paragraph("Issue Date: " + outputCashReceiptDto.getAccountingDate()));
        pdfPCell.addElement(new Paragraph("Output cash receipt number: " + outputCashReceiptDto.getDocumentNumber()));
        pdfPCell.addElement(new Paragraph("Cashier: " + outputCashReceiptDto.getCashierTitle()));

        ArrayList<GoodsServiceItems> goodsServiceItems = new ArrayList<>(outputCashReceiptDto.getGoodsServiceItems());
        BigDecimal total = BigDecimal.ZERO;
        BigDecimal totalVat = BigDecimal.ZERO;
        for (int i = 0; i < goodsServiceItems.size(); i++) {
            GoodsServiceItems goodsServiceItems1 = goodsServiceItems.get(i);
            pdfPCell.addElement(new Paragraph("Element #" + (i + 1) + ": " + goodsServiceItems1.getGoodsServices().getName() + ": " + goodsServiceItems1.getPriceTotal()));
            total = total.add(goodsServiceItems1.getPriceTotal());
            totalVat = totalVat.add(goodsServiceItems1.getVatTotal());
        }
        pdfPCell.addElement(new Paragraph("Total VAT: " + totalVat));
        pdfPCell.addElement(new Paragraph("Total: " + total));
        return pdfPCell;
    }

    public ByteArrayInputStream outputCashReceiptPdf(OutputCashReceipt outputCashReceipt, String ourCompany) {
        Set<Crm> ourCrm = crmDao.findAllByNameIgnoreCase(ourCompany);
        Crm oneOurCrm = ourCrm.iterator().next();
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            PdfPTable table = new PdfPTable(1);
            table.setWidthPercentage(80);
            table.setWidths(new int[]{1});
            table.setSpacingBefore(10);
            Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
            PdfPCell pdfLeftIssuer = createPdfCell(oneOurCrm, "Issuer");
            table.addCell(pdfLeftIssuer);
            PdfWriter.getInstance(document, out);
            document.open();
            Paragraph headline = new Paragraph("Outcome cash receipt");
            headline.setAlignment(Element.ALIGN_CENTER);
            table.setPaddingTop(5f);
            document.add(headline);
            document.add(table);
            PdfPTable tableLower = new PdfPTable(1);
            tableLower.setWidthPercentage(80);
            tableLower.setWidths(new int[]{1});
            tableLower.setSpacingBefore(10);
            PdfPCell pdfPCellLeftLower = createPdfCellMainOutputCashReceipt(outputCashReceipt);
            tableLower.addCell(pdfPCellLeftLower);
            document.add(tableLower);
            document.close();
        } catch (Exception ex){
            log.error("Nastala chyba", ex);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }
}
