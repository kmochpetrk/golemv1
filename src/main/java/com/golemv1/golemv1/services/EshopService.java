package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.*;
import com.golemv1.golemv1.dtos.EshopOrderFullDto;
import com.golemv1.golemv1.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class EshopService {

    @Autowired
    private CrmDao crmDao;

    @Autowired
    private AccUnitDao accUnitDao;

    @Autowired
    private EshopOrderDao eshopOrderDao;

    @Autowired
    private EshopOrderItemDao eshopOrderItemDao;

    @Autowired
    private GoodsServicesDao goodsServicesDao;

    @Autowired
    private WarehouseDao warehouseDao;


    public EshopOrderFullDto saveNewOrder(EshopOrderFullDto eshopOrderFullDto, String company) {
        Set<Crm> crms = crmDao.findAllByIcoEquals(eshopOrderFullDto.getCrm().getIco());
        Crm crm = null;
        if (crms != null && !crms.isEmpty()) {
            crm = crms.iterator().next();
        } else {
            crm = new Crm();
            crm.setCrmType(CrmType.CUSTOMER);
            crm.setContactType(ContactType.EMAIL);
            crm.setEmail(eshopOrderFullDto.getCrm().getEmail());
            crm.setAccountingUnit(accUnitDao.findByName(company));
            crm.setName(eshopOrderFullDto.getCrm().getName());
            crm.setIco(eshopOrderFullDto.getCrm().getIco());
            crm.setDic(eshopOrderFullDto.getCrm().getDic());
            crm.setBankAccount(eshopOrderFullDto.getOurBankAccount());
            crm.setPhone(eshopOrderFullDto.getCrm().getPhone());
            crm.setPrimaryAddress(new Address());
            crm.getPrimaryAddress().setCity(eshopOrderFullDto.getCrm().getPrimaryAddress().getCity());
            crm.getPrimaryAddress().setDescNumber(eshopOrderFullDto.getCrm().getPrimaryAddress().getDescNumber());
            crm.getPrimaryAddress().setStreet(eshopOrderFullDto.getCrm().getPrimaryAddress().getStreet());
            crm.getPrimaryAddress().setZipCode(eshopOrderFullDto.getCrm().getPrimaryAddress().getZipCode());
            crm = crmDao.save(crm);
        }
        EshopOrder eshopOrder = new EshopOrder();
        eshopOrder.setCompany(eshopOrderFullDto.getCompany());
        eshopOrder.setCrm(crm);
        eshopOrder.setDeliveryType(eshopOrderFullDto.getDeliveryType());
        eshopOrder.setOINumber(eshopOrderFullDto.getoINumber());
        eshopOrder.setOurBankAccount(eshopOrderFullDto.getOurBankAccount());
        eshopOrder.setPaymentType(eshopOrderFullDto.getPaymentType());
        eshopOrder.setTotalPrice(eshopOrderFullDto.getTotalPrice());
        eshopOrder.setTotalVat(eshopOrderFullDto.getTotalVat());
        EshopOrder eshopOrderSaved = eshopOrderDao.save(eshopOrder);

        eshopOrderFullDto.getOrders().stream().forEach(a -> {
            EshopOrderItem eshopOrderItem = new EshopOrderItem();
            eshopOrderItem.setEshopOrder(eshopOrderSaved);
            eshopOrderItem.setGoodsServices(goodsServicesDao.findById(a.getEshopGoodsService().getGoodsServiceId()).get());
            eshopOrderItem.setWarehouse(warehouseDao.findById(a.getItem().getWarehouseId()).get());
            eshopOrderItem.setOrderedCount(a.getItem().getOrderedCount());
            eshopOrderItem.setTotalItemPrice(a.getItem().getTotalItemPrice());
            eshopOrderItem.setTotalItemVat(a.getItem().getTotalItemVat());
            eshopOrderItem.setUnitOutputPrice(a.getItem().getUnitOutputPrice());
            eshopOrderItem.setUnitOutputPriceWithVat(a.getItem().getUnitOutputPriceWithVat());
            eshopOrderItem.setVatPercent(a.getItem().getVatPercent());
            eshopOrderItemDao.save(eshopOrderItem);
        });
        return eshopOrderFullDto;
    }
}
