package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.WarehouseDao;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.Warehouse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service
@Slf4j
@Transactional
public class WarehousesService {

    @Autowired
    private WarehouseDao warehouseDao;

    @Autowired
    private AccUnitDao accUnitDao;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Warehouse createWarehouse(Warehouse warehouse, AccountingUnit accountingUnit) {
        Warehouse topByAccountingUnitOrderByAnalyticAccountDesc = this.warehouseDao.findTopByAccountingUnitAndAnalyticAccountNotNullOrderByAnalyticAccountDesc(accountingUnit);
        Long nextAnalyticAccountValue = null;
        if (topByAccountingUnitOrderByAnalyticAccountDesc != null && topByAccountingUnitOrderByAnalyticAccountDesc.getAnalyticAccount() != null) {
            nextAnalyticAccountValue = topByAccountingUnitOrderByAnalyticAccountDesc.getAnalyticAccount() + 1;
        } else {
            nextAnalyticAccountValue = 1L;
        }
        log.info("Next analytic account warehouse value " + nextAnalyticAccountValue);
        warehouse.setAnalyticAccount(nextAnalyticAccountValue);
        warehouse.setAccountingUnit(accountingUnit);
        return warehouseDao.save(warehouse);
    }

    public Warehouse getById(Long id) {
        return warehouseDao.findById(id).get();
    }

    public Page<Warehouse> findAll(Pageable pageable, String company) {
        AccountingUnit accUnit = accUnitDao.findByName(company);
        return warehouseDao.findAllByAccountingUnit(accUnit, pageable);
    }
}
