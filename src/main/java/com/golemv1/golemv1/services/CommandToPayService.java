package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.CommandToPayDao;
import com.golemv1.golemv1.daos.CommandToPayItemDao;
import com.golemv1.golemv1.daos.InputInvoiceDao;
import com.golemv1.golemv1.dtos.CommandToPayDto;
import com.golemv1.golemv1.dtos.CommandToPayItemDto;
import com.golemv1.golemv1.dtos.InputInvoiceDto;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.CommandToPay;
import com.golemv1.golemv1.entities.CommandToPayItem;
import com.golemv1.golemv1.entities.InputInvoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class CommandToPayService {

    @Autowired
    private CommandToPayDao commandToPayDao;

    @Autowired
    private CommandToPayItemDao commandToPayItemDao;

    @Autowired
    private AccUnitDao accUnitDao;

    @Autowired
    private InputInvoiceDao inputInvoiceDao;

    public Page<CommandToPayDto> getAllByCompany(String company, Pageable pageable) {
        AccountingUnit byName = accUnitDao.findByName(company);
        return commandToPayDao.findAllByAccountingUnit(byName, pageable).map(CommandToPayService::map);
    }

    public Page<CommandToPayDto> getAllByCompanyNotCtp(String company, Pageable pageable) {
        AccountingUnit byName = accUnitDao.findByName(company);
        return commandToPayDao.findAllByAccountingUnitAndPayedByBsIsNull(byName, pageable).map(CommandToPayService::map);
    }


    public CommandToPay createCommandToPay(String company, CommandToPayDto commandToPayDto) {
        CommandToPay commandToPay = new CommandToPay();
        AccountingUnit byName = accUnitDao.findByName(company);
        commandToPay.setAccountingUnit(byName);
        commandToPay.setDocumentNumber(commandToPayDto.getDocumentNumber());
        commandToPay.setId(commandToPayDto.getId());
        commandToPay.setOurBankAccount(commandToPayDto.getOurBankAccount());
        commandToPay.setCommandToPayItems(remap(commandToPayDto.getCommandToPayItems()));

        CommandToPay save = commandToPayDao.save(commandToPay);
        commandToPay.getCommandToPayItems().forEach(a -> {
            if (commandToPay.getInputInvoices() != null && !commandToPay.getInputInvoices().isEmpty()) {
                InputInvoice inputInvoice = inputInvoiceDao.findById(commandToPay.getInputInvoices().iterator().next().getId()).get();
                inputInvoice.setCommandToPay(save);
                inputInvoiceDao.save(inputInvoice);
            }
            a.setCommandToPay(save);
            commandToPayItemDao.save(a);
        });
        return commandToPay;
    }

    private Set<CommandToPayItem> remap(Set<CommandToPayItemDto> commandToPayItemDtos) {
        Set<CommandToPayItem> returnSet = new HashSet<>();
        commandToPayItemDtos.forEach(a -> {
            CommandToPayItem commandToPayItem = new CommandToPayItem();
            commandToPayItem.setAmount(a.getAmount());
            commandToPayItem.setConstantSymbol(a.getConstantSymbol());
            commandToPayItem.setId(a.getId());
            commandToPayItem.setMessageForReceiver(a.getMessageForReceiver());
            commandToPayItem.setOtherBankAccount(a.getOtherBankAccount());
            commandToPayItem.setSpecificSymbol(a.getSpecificSymbol());
            commandToPayItem.setVariableSymbol(a.getVariableSymbol());
            commandToPayItem.setOtherBankAccount(a.getOtherBankAccount());
            returnSet.add(commandToPayItem);
        });
        return returnSet;
    }


    private static CommandToPayDto map(CommandToPay commandToPay) {
        final CommandToPayDto commandToPayDto = new CommandToPayDto();
        commandToPayDto.setDocumentNumber(commandToPay.getDocumentNumber());
        commandToPayDto.setId(commandToPay.getId());
        commandToPayDto.setOurBankAccount(commandToPay.getOurBankAccount());
        commandToPayDto.setCommandToPayItems(mapItems(commandToPay.getCommandToPayItems()));
        commandToPayDto.setInputInvoices(mapInvoices(commandToPay.getInputInvoices()));
        return commandToPayDto;
    }

    private static Set<InputInvoiceDto> mapInvoices(Set<InputInvoice> inputInvoices) {
        Set<InputInvoiceDto> returnSet = new HashSet<>();
        inputInvoices.forEach(a -> {
            InputInvoiceDto inputInvoiceDto = new InputInvoiceDto();
            inputInvoiceDto.setCrmId(a.getCrm() != null ? a.getCrm().getId() : null);
            inputInvoiceDto.setDocumentNumber(a.getDocumentNumber());
            inputInvoiceDto.setId(a.getId());
            inputInvoiceDto.setIssueDate(a.getAccountingDate());
            inputInvoiceDto.setPayDate(a.getPayDate());
            inputInvoiceDto.setVariableSymbol(a.getVariableSymbol());
        });
        return returnSet;
    }

    private static Set<CommandToPayItemDto> mapItems(Set<CommandToPayItem> commandToPayItems) {
        Set<CommandToPayItemDto> returnSet = new HashSet();
        commandToPayItems.forEach(a -> {
            CommandToPayItemDto commandToPayItemDto = new CommandToPayItemDto();
            commandToPayItemDto.setAmount(a.getAmount());
            commandToPayItemDto.setConstantSymbol(a.getConstantSymbol());
            commandToPayItemDto.setId(a.getId());
            commandToPayItemDto.setMessageForReceiver(a.getMessageForReceiver());
            commandToPayItemDto.setOtherBankAccount(a.getOtherBankAccount());
            commandToPayItemDto.setSpecificSymbol(a.getSpecificSymbol());
            commandToPayItemDto.setVariableSymbol(a.getVariableSymbol());
            returnSet.add(commandToPayItemDto);
        });
        return returnSet;
    }

    public int getNextNumberCommandTopay(String company) {
        AccountingUnit byName = accUnitDao.findByName(company);
        byName.setCommandToPayNumber(byName.getCommandToPayNumber() + 1);
        AccountingUnit save = accUnitDao.save(byName);
        return Integer.valueOf(save.getCommandToPayNumber());
    }
}
