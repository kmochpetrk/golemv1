package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.*;
import com.golemv1.golemv1.dtos.AmortizationRequestDto;
import com.golemv1.golemv1.dtos.CommandToPayDto;
import com.golemv1.golemv1.dtos.CommandToPayItemDto;
import com.golemv1.golemv1.dtos.MonthlySalaryRequestDto;
import com.golemv1.golemv1.entities.*;
import com.golemv1.golemv1.mappers.MainMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class EmployeeService {

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private AccUnitDao accUnitDao;

    @Autowired
    private InternalDocumentService internalDocumentService;

    @Autowired
    private MainMapper mainMapper;

    @Autowired
    private InternalDocumentDao internalDocumentDao;

    @Autowired
    private GlWriteDao glWriteDao;

    @Autowired
    private CommandToPayService commandToPayService;

    @Autowired
    private CrmDao crmDao;


    public Employee createEmployee(Employee employee, String company) {
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        employee.setAccountingUnit(accountingUnit);
        return employeeDao.save(employee);
    }

    public Employee updateEmployee(Employee employee) {
        return employeeDao.save(employee);
    }

    public Employee getById(Long id) {
        return employeeDao.findById(id).get();
    }

    public void deleteById(Long id) {
        employeeDao.deleteById(id);
    }

    public Page<Employee> getAllEmployees(Pageable pageable, AccountingUnit accountingUnit) {
        return employeeDao.findAllByAccountingUnit(accountingUnit, pageable);
    }

    public int getNextNumber(String company) {
        AccountingUnit byName = accUnitDao.findByName(company);
        byName.setEmployeeNumber(byName.getEmployeeNumber() + 1);
        AccountingUnit save = accUnitDao.save(byName);
        return Integer.valueOf(save.getEmployeeNumber());
    }


    public Employee processMonthlySalary(MonthlySalaryRequestDto monthlySalaryRequestDto, String company) {
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        InternalDocument internalDocument = new InternalDocument();
        String documentNumber = "ID-" + internalDocumentService.getNextNumber(company);
        internalDocument.setDocumentNumber(documentNumber);
        internalDocument.setAccountingDate(LocalDate.now());
        internalDocument.setAccountingUnit(accountingUnit);
        internalDocument.setTaxDate(LocalDate.now());
        internalDocument.setGlWrites(mainMapper.mapGlWrites(monthlySalaryRequestDto.getAccounting()));

        final InternalDocument savedInternalDoc = internalDocumentDao.save(internalDocument);

        internalDocument.getGlWrites().forEach(item -> {
            item.setGlWholeWrite(savedInternalDoc);
            glWriteDao.save(item);
        });

        final List<Employee> savedInventoryItems = new ArrayList<>();

        monthlySalaryRequestDto.getMonthlySalaryDtos().forEach(a -> {
            Employee employee = employeeDao.findById(a.getEmployeeId()).get();
            if (employee.getMonthlySalaries() == null) {
                employee.setMonthlySalaries(new HashSet<>());
            }

            MonthlySalary monthlySalary = new MonthlySalary();
            monthlySalary.setAmount(a.getAmount());
            monthlySalary.setBookingNumber(savedInternalDoc.getDocumentNumber());
            monthlySalary.setEmployee(employee);
            monthlySalary.setPeriod(a.getPeriod());
            employee.getMonthlySalaries().add(monthlySalary);
            Employee save = employeeDao.save(employee);
            internalDocument.setDescriptionOfCase("monthly salary " + LocalDate.now() + ": " + employee.getSurname() + " " + employee.getFirstname());
            savedInventoryItems.add(save);

            //create command to pay
            CommandToPayDto commandToPayDto = new CommandToPayDto();
            commandToPayDto.setDocumentNumber("CTP-" + commandToPayService.getNextNumberCommandTopay(company));
            commandToPayDto.setOurBankAccount(crmDao.findAllByNameIgnoreCase(company).iterator().next().getBankAccount());
            Set<CommandToPayItemDto> commandToPayItemDtos = new HashSet<>();
            CommandToPayItemDto commandToPayItemDto = new CommandToPayItemDto();
            commandToPayItemDto.setVariableSymbol("Emp-" + employee.getEmployeeNumber() + "_" + LocalDate.now());
            commandToPayItemDto.setOtherBankAccount(employee.getPersonalBankAccount());
            commandToPayItemDto.setAmount(employee.getMonthlyGrossSalary());
            commandToPayItemDto.setSpecificSymbol("0308");
            commandToPayItemDto.setMessageForReceiver("Salary");
            commandToPayItemDtos.add(commandToPayItemDto);
            commandToPayDto.setCommandToPayItems(commandToPayItemDtos);
            commandToPayService.createCommandToPay(company, commandToPayDto);

        });
        return savedInventoryItems.get(0);
    }
}
