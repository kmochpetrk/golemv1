package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.*;
import com.golemv1.golemv1.dtos.BankStatementDto;
import com.golemv1.golemv1.entities.*;
import com.golemv1.golemv1.mappers.MainMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class BankStatementService {

    @Autowired
    private AccUnitDao accUnitDao;

    @Autowired
    private MainMapper mainMapper;

    @Autowired
    private BankStatementDao bankStatementDao;

    @Autowired
    private InputInvoiceDao inputInvoiceDao;

    @Autowired
    private OutputInvoicesDao outputInvoicesDao;

    @Autowired
    private CommandToPayDao commandToPayDao;

    @Autowired
    private GlWriteDao glWriteDao;

    public Integer getNextNumberBankStatement(String company) {
        AccountingUnit byName = accUnitDao.findByName(company);
        byName.setBankStatementNumber(byName.getBankStatementNumber() + 1);
        AccountingUnit save = accUnitDao.save(byName);
        return Integer.valueOf(save.getBankStatementNumber());
    }

    public BankStatement createBankStatement(BankStatementDto bankStatementDto, String company, boolean income) {
        BankStatement bankStatement = new BankStatement();
        AccountingUnit byName = accUnitDao.findByName(company);
        bankStatement.setAccountingUnit(byName);
        bankStatement.setIncome(income);
        bankStatement.setDocumentNumber(bankStatementDto.getDocumentNumber());
        bankStatement.setBankStatementNumber(bankStatementDto.getBankStatementNumber());
        bankStatement.setAmount(bankStatementDto.getAmountInput());
        bankStatement.setOurBankAccount(bankStatementDto.getOurBankAccount());
        OutputInvoice outputInvoice = null;
        InputInvoice inputInvoice = null;
        if (income) {
            if (bankStatementDto.getInvoiceId() != null) {
                outputInvoice = outputInvoicesDao.findById(bankStatementDto.getInvoiceId()).get();
                bankStatement.setOutputInvoice(outputInvoice);
            }
            bankStatement.setDescriptionOfCase("Bank statement income");

        } else {
            if (bankStatementDto.getInvoiceId() != null) {
                inputInvoice = inputInvoiceDao.findById(bankStatementDto.getInvoiceId()).get();
                bankStatement.setInputInvoice(inputInvoice);
            }
            bankStatement.setDescriptionOfCase("Bank statement outcome");
            if (bankStatementDto.getCtpId() != null) {
                bankStatement.setCptId(bankStatementDto.getCtpId());
                Optional<CommandToPay> byId = commandToPayDao.findById(bankStatement.getCptId());
                if (byId.isPresent()) {
                    CommandToPay commandToPay = byId.get();
                    commandToPay.setPayedByBs(bankStatementDto.getDocumentNumber());
                }
            }
        }
        bankStatement.setGlWrites(mainMapper.mapGlWrites(bankStatementDto.getGlWrites()));
        bankStatement.setBankAccountInput(bankStatementDto.getBankAccountInput());
        bankStatement.setIssueDate(bankStatementDto.getIssueDate());
        bankStatement.setAccountingDate(LocalDate.now());
        bankStatement.setVariableSymbolFromInvoice(bankStatementDto.getVariableSymbolFromInvoice());
        bankStatement.setVariableSymbolInput(bankStatementDto.getVariableSymbolInput());

        BankStatement savedBankStatement = bankStatementDao.save(bankStatement);
        Set<BankStatement> statements = new HashSet<>();
        statements.add(bankStatement);
        if (income) {
            if (outputInvoice != null) {
                //outputInvoice.setBankStatements(statements);
                outputInvoice.setBsId(statements.iterator().next().getId());
                outputInvoicesDao.save(outputInvoice);
            }
        } else {
            if (inputInvoice != null) {
                //inputInvoice.setBankStatements(statements);
                inputInvoice.setBsId(statements.iterator().next().getId());
                inputInvoiceDao.save(inputInvoice);
            }
        }
        bankStatement.getGlWrites().forEach(item -> {
            item.setGlWholeWrite(savedBankStatement);
            glWriteDao.save(item);
        });
        return savedBankStatement;
    }
}
