package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.*;
import com.golemv1.golemv1.dtos.BookingDto;
import com.golemv1.golemv1.dtos.GlWholeWriteDto;
import com.golemv1.golemv1.dtos.InputInvoiceDto;
import com.golemv1.golemv1.entities.*;
import com.golemv1.golemv1.mappers.MainMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class GeneralAccountingService {

    @Autowired
    private GlWholeWritingDao glWholeWritingDao;

    @Autowired
    private CrmDao crmDao;

    @Autowired
    private InputInvoiceDao inputInvoiceDao;

    @Autowired
    private OutputInvoicesDao outputInvoicesDao;

    @Autowired
    private GoodsInputReceiptDao goodsInputReceiptDao;

    @Autowired
    private GoodsOutputReceiptDao goodsOutputReceiptDao;

    @Autowired
    private GoodsInOutputReceiptService goodsInOutputReceiptService;

    @Autowired
    private OutputInvoiceService outputInvoiceService;

    @Autowired
    private InputInvoiceService inputInvoiceService;

    @Autowired
    private BankStatementDao bankStatementDao;

    @Autowired
    private GoodsServicesItemDao goodsServicesItemDao;

    @Autowired
    private BankStatementService bankStatementService;

    @Autowired
    private GlWriteDao glWriteDao;

    @Autowired
    private MainMapper mainMapper;

    @Autowired
    private AccUnitDao accUnitDao;

    @Autowired
    private InternalDocumentService internalDocumentService;

    @Autowired
    private InternalDocumentDao internalDocumentDao;


    public Page<GlWholeWriteDto> getPageableAllBookings(Pageable pageable, AccountingUnit accountingUnit, String docType) {
        Page<GlWholeWriteDto> all = null;
        if (docType.equals("ALL")) {
            all = glWholeWritingDao.findAllByAccountingUnitOrderByAccountingDateDesc(accountingUnit, pageable).map(glWW -> {
                return new GlWholeWriteDto(glWW.getId(),
                        MainMapper.remapGlWrites(glWW.getGlWrites()),
                        findGlWholeWriteType(glWW),
                        glWW.getAccountingDate(),
                        glWW.getTaxDate(),
                        glWW.getWrittenBy() != null ? (glWW.getWrittenBy().getFirstName() + " " + glWW.getWrittenBy().getSurname()) : "",
                        glWW.getCrm() != null ? glWW.getCrm().getName() : "",
                        glWW.getDocumentNumber(),
                        glWW.getCancelled(), glWW.getDescriptionOfCase());
            });
        } else {
            GlWholeWriteType glWholeWriteType = GlWholeWriteType.valueOf(docType);
            all = glWholeWritingDao.findAllByAccountingUnitAndGlWholeWriteTypeOrderByAccountingDateDesc(accountingUnit, glWholeWriteType, pageable).map(glWW -> {
                return new GlWholeWriteDto(glWW.getId(),
                        MainMapper.remapGlWrites(glWW.getGlWrites()),
                        findGlWholeWriteType(glWW),
                        glWW.getAccountingDate(),
                        glWW.getTaxDate(),
                        glWW.getWrittenBy() != null ? (glWW.getWrittenBy().getFirstName() + " " + glWW.getWrittenBy().getSurname()) : "",
                        glWW.getCrm() != null ? glWW.getCrm().getName() : "",
                        glWW.getDocumentNumber(),
                        glWW.getCancelled(), glWW.getDescriptionOfCase());
            });
        }
        return all;
    }

    public GlWholeWrite cancel(BookingDto bookingDto, String company) {
        GlWholeWrite glWholeWrite = glWholeWritingDao.findById(bookingDto.getId()).get();
        glWholeWrite.setCancelled(true);
        GlWholeWrite save = glWholeWritingDao.save(glWholeWrite);
        return save;
    }

    public GlWholeWrite createNewBooking(BookingDto bookingDto, String company) {
        final AccountingUnit accountingUnit = accUnitDao.findByName(company);
        final GlWholeWrite glWholeWrite = createMainEntity(bookingDto, company);
        glWholeWrite.setAccountingUnit(accountingUnit);
        if (bookingDto.getCrmId() != null) {
            glWholeWrite.setCrm(crmDao.findById(bookingDto.getCrmId()).get());
        }
        glWholeWrite.setGlWrites(mainMapper.mapGlWrites(bookingDto.getGlWrites()));
        glWholeWrite.setAccountingDate(LocalDate.now());
        if (glWholeWrite instanceof InputInvoice) {
            ((InputInvoice)glWholeWrite).setPayDate(bookingDto.getPayDate());
        }
        glWholeWrite.setDescriptionOfCase(bookingDto.getDescription());
        glWholeWrite.setVariableSymbol(bookingDto.getVariableSymbol());
        glWholeWrite.setTaxDate(bookingDto.getVatDate());
        glWholeWrite.setGoodsServiceItems(mainMapper.mapGoodsServiceItems(bookingDto.getWarehouseGoodsDtos(), true, false));
        GlWholeWrite glWholeWriteSaved = saveMainEntity(glWholeWrite);
        glWholeWrite.getGlWrites().forEach(item -> {
            item.setGlWholeWrite(glWholeWriteSaved);
            glWriteDao.save(item);
        });
        glWholeWrite.getGoodsServiceItems().forEach(item -> {
            item.setGlWholeWrite(glWholeWriteSaved);
            goodsServicesItemDao.save(item);
        });
        return glWholeWriteSaved;
    }

    // generic save
    private GlWholeWrite saveMainEntity(GlWholeWrite glWholeWrite) {
        if (glWholeWrite instanceof InputInvoice) {
            return inputInvoiceDao.save((InputInvoice) glWholeWrite);
        }
        if (glWholeWrite instanceof GoodsInputReceipt) {
            return goodsInputReceiptDao.save((GoodsInputReceipt) glWholeWrite);
        }
        if (glWholeWrite instanceof GoodsOutputReceipt) {
            return goodsOutputReceiptDao.save((GoodsOutputReceipt) glWholeWrite);
        }
        if (glWholeWrite instanceof OutputInvoice) {
            return outputInvoicesDao.save((OutputInvoice) glWholeWrite);
        }
        if (glWholeWrite instanceof BankStatement) {
            return bankStatementDao.save((BankStatement) glWholeWrite);
        }
        if (glWholeWrite instanceof InternalDocument) {
            return internalDocumentDao.save((InternalDocument) glWholeWrite);
        }
        throw new IllegalStateException("");
    }

    private GlWholeWrite createMainEntity(BookingDto bookingDto, String company) {
        switch (bookingDto.getGlWholeWriteType()) {
            case "INPUT_INVOICE":
                InputInvoice inputInvoice = new InputInvoice();
                inputInvoice.setDocumentNumber("II-" + inputInvoiceService.getNextNumber(company));
                if (bookingDto.getVariableSymbol() != null) {
                    inputInvoice.setVariableSymbol(bookingDto.getVariableSymbol());
                }
                inputInvoice.setTotal(bookingDto.getGlWrites().stream().reduce(BigDecimal.ZERO,(partial, addNew) ->
                                partial.add(addNew.getGlAccountSideType() == GlAccountSideType.DEBIT ? addNew.getAmount() : BigDecimal.ZERO),
                        BigDecimal::add));
//                inputInvoice.setTotalTax(bookingDto.getGlWrites().stream().reduce(BigDecimal.ZERO,(partial, addNew) ->
//                                partial.add(addNew.getAcctNo() == GlAccountType.PROFIT_LOSS ? addNew.getAmount() : BigDecimal.ZERO),
//                        BigDecimal::add));
                return inputInvoice;
            case "OUTPUT_INVOICE":
                OutputInvoice outputInvoice = new OutputInvoice();
                outputInvoice.setTotal(bookingDto.getGlWrites().stream().reduce(BigDecimal.ZERO,(partial, addNew) ->
                                partial.add(addNew.getGlAccountSideType() == GlAccountSideType.DEBIT ? addNew.getAmount() : BigDecimal.ZERO),
                        BigDecimal::add));
                outputInvoice.setDocumentNumber("OI-" + outputInvoiceService.getNextNumber(company));
                return outputInvoice;
            case "BANK_STATEMENT":
                BankStatement bankStatement = new BankStatement();
                bankStatement.setDocumentNumber("BS" + bankStatementService.getNextNumberBankStatement(company));
                return bankStatement;
            case "GOODS_INPUT_RECEIPT":
                GoodsInputReceipt goodsInputReceipt = new GoodsInputReceipt();
                goodsInputReceipt.setDocumentNumber("GIR" + goodsInOutputReceiptService.getNextNumberInputGoodsReceipt(company));
                return goodsInputReceipt;
            case "GOODS_OUTPUT_RECEIPT":
                GoodsOutputReceipt goodsOutputReceipt = new GoodsOutputReceipt();
                goodsOutputReceipt.setDocumentNumber("GOR" + goodsInOutputReceiptService.getNextNumberOutputGoodsReceipt(company));
                return goodsOutputReceipt;
            case "INTERNAL":
                InternalDocument internalDocument = new InternalDocument();
                internalDocument.setDocumentNumber("ID-" + internalDocumentService.getNextNumber(company));
                return internalDocument;
            default:
                throw new IllegalStateException("Nonsense type od document: " + bookingDto.getGlWholeWriteType());
        }
    }

    private GlWholeWriteType findGlWholeWriteType(GlWholeWrite glWW) {
        if (glWW instanceof BankStatement) {
            return GlWholeWriteType.BANK_STATEMENT;
        } else if (glWW instanceof InputInvoice) {
            return GlWholeWriteType.INPUT_INVOICE;
        } else if (glWW instanceof OutputInvoice) {
            return GlWholeWriteType.OUTPUT_INVOICE;
        } else if (glWW instanceof GoodsInputReceipt) {
            return GlWholeWriteType.GOODS_INPUT_RECEIPT;
        } else if (glWW instanceof GoodsOutputReceipt) {
            return GlWholeWriteType.GOODS_OUTPUT_RECEIPT;
        } else if (glWW instanceof InputCashReceipt) {
            return GlWholeWriteType.CASH_INPUT_RECEIPT;
        } else if (glWW instanceof OutputCashReceipt) {
            return GlWholeWriteType.CASH_OUTPUT_RECEIPT;
        } else if (glWW instanceof InternalDocument) {
            return GlWholeWriteType.INTERNAL;
        } else {
            log.error("Error variant: " + glWW.getClass().getName());
            throw new IllegalStateException("Error variant: " + glWW.getClass().getName());
        }

    }


}
