package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.*;
import com.golemv1.golemv1.dtos.InputCashReceiptDto;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.GoodsInputReceipt;
import com.golemv1.golemv1.entities.GoodsOutputReceipt;
import com.golemv1.golemv1.entities.InputCashReceipt;
import com.golemv1.golemv1.mappers.MainMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;

@Service
@Transactional
public class InputCashReceiptService {

    @Autowired
    private InputCashReceiptDao inputCashReceiptDao;

    @Autowired
    private MainMapper mainMapper;

    @Autowired
    private AccUnitDao accUnitDao;

    @Autowired
    private GlWriteDao glWriteDao;

    @Autowired
    private GoodsServicesItemDao goodsServicesItemDao;

    @Autowired
    private GoodsOutputReceiptDao goodsOutputReceiptDao;

    public int getNextNumber(String company) {
        AccountingUnit byName = accUnitDao.findByName(company);
        byName.setInputCashReceiptNumber(byName.getInputCashReceiptNumber() + 1);
        AccountingUnit save = accUnitDao.save(byName);
        return Integer.valueOf(save.getInputCashReceiptNumber());
    }

    public InputCashReceipt createInputCashReceipt(InputCashReceiptDto inputCashReceiptDto, String company) {
        final AccountingUnit accountingUnit = accUnitDao.findByName(company);
        final InputCashReceipt inputCashReceipt = new InputCashReceipt();
        inputCashReceipt.setAccountingUnit(accountingUnit);
        inputCashReceipt.setDocumentNumber(inputCashReceiptDto.getDocumentNumber());
        inputCashReceipt.setGlWrites(mainMapper.mapGlWrites(inputCashReceiptDto.getGlWrites()));
        inputCashReceipt.setAccountingDate(inputCashReceiptDto.getIssueDate());
        inputCashReceipt.setDescriptionOfCase("input cash receipt");
        inputCashReceipt.setCashierTitle(inputCashReceiptDto.getCashierTitle());
        inputCashReceipt.setGoodsServiceItems(mainMapper.mapGoodsServiceItems(inputCashReceiptDto.getWarehouseGoodsDtos(), true, false));
        inputCashReceipt.setIgrId(inputCashReceiptDto.getGorIds() != null && !inputCashReceiptDto.getGorIds().isEmpty() ? inputCashReceiptDto.getGorIds().iterator().next() : null);
        InputCashReceipt inputCashReceiptSaved = inputCashReceiptDao.save(inputCashReceipt);
        if (inputCashReceiptDto.getGorIds() != null && !inputCashReceiptDto.getGorIds().isEmpty()) {
            Long next = inputCashReceiptDto.getGorIds().iterator().next();
            GoodsOutputReceipt byId = goodsOutputReceiptDao.findById(next).get();
            byId.setIcrId(inputCashReceiptSaved.getId());
            goodsOutputReceiptDao.save(byId);
        }
        inputCashReceipt.getGlWrites().forEach(item -> {
            item.setGlWholeWrite(inputCashReceiptSaved);
            glWriteDao.save(item);
        });
        inputCashReceipt.getGoodsServiceItems().forEach(item -> {
            item.setGlWholeWrite(inputCashReceiptSaved);
            goodsServicesItemDao.save(item);
        });
        return inputCashReceiptSaved;
    }
}
