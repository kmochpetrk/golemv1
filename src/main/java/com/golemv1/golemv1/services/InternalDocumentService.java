package com.golemv1.golemv1.services;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.InputInvoiceDao;
import com.golemv1.golemv1.daos.InternalDocumentDao;
import com.golemv1.golemv1.entities.AccountingUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class InternalDocumentService {

    @Autowired
    private InternalDocumentDao internalDocumentDao;

    @Autowired
    private AccUnitDao accUnitDao;

    public Integer getNextNumber(String company) {
        AccountingUnit byName = accUnitDao.findByName(company);
        byName.setInternalDocumentNumber(byName.getInternalDocumentNumber() + 1);
        AccountingUnit save = accUnitDao.save(byName);
        return Integer.valueOf(save.getInternalDocumentNumber());
    }
}
