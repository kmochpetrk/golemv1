package com.golemv1.golemv1;

import com.golemv1.golemv1.daos.*;
import com.golemv1.golemv1.entities.*;
import com.golemv1.golemv1.entities.language.AccRule;
import com.golemv1.golemv1.entities.language.AccRuleElement;
import com.golemv1.golemv1.entities.language.EnglishWord;
import com.golemv1.golemv1.entities.language.Sentence;
import com.golemv1.golemv1.services.GlAccountService;
import com.golemv1.golemv1.services.GoodsInOutputReceiptService;
import com.golemv1.golemv1.services.WarehousesService;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Hibernate;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.resource.PathResourceResolver;
import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@SpringBootApplication
@EnableScheduling
@Slf4j
public class Golemv1Application {

    @Autowired
    private GlAccountService glAccountService;

    @Autowired
    private AccUnitDao accUnitDao;

    @Autowired
    private CrmDao crmDao;

    @Autowired
    private GoodsServicesDao goodsServicesDao;

    @Autowired
    private GoodsInputItemDao goodsInputItemDao;

    @Autowired
    private WarehouseGoodsServicesDao warehouseGoodsServicesDao;

    @Autowired
    private WarehouseDao warehouseDao;

    @Autowired
    private GoodsInOutputReceiptService goodsInOutputReceiptService;

    @Autowired
    private GoodsOutputReceiptDao goodsOutputReceiptDao;

    @Autowired
    private WarehousesService warehousesService;

    @Autowired
    private SentenceDao sentenceDao;

    @Autowired
    private EnglishWordDao englishWordDao;

    @Autowired
    private RuleElementDao ruleElementDao;

    @Autowired
    private RuleDao ruleDao;

    public static void main(String[] args) {
        SpringApplication.run(Golemv1Application.class, args);
    }

    //@Scheduled(initialDelay = 3000L, fixedRate = 24*60*60*1000)
    @Transactional
    public void createAccounts() {
//        Sentence next = sentenceDao.findAll().iterator().next();
//        Optional<Sentence> byId = sentenceDao.findById(next.getId());
//        Hibernate.initialize(byId.get().getAccRule().getRuleElements());
//        Hibernate.initialize(byId.get().getWords());
//        log.info("sentence count " + sentenceDao.count());

//        EnglishWord englishWord1 = new EnglishWord();
//        englishWord1.setWord("rent");
//        EnglishWord englishWord2 = new EnglishWord();
//        englishWord2.setWord("for");
//        EnglishWord englishWord3 = new EnglishWord();
//        englishWord3.setWord("car");
//
////        EnglishWord saveWord1 = englishWordDao.save(englishWord1);
////        EnglishWord saveWord2 = englishWordDao.save(englishWord2);
////        EnglishWord saveWord3 = englishWordDao.save(englishWord3);
//
//        Sentence sentence = new Sentence();
//        sentence.setWords(List.of(englishWord1, englishWord2, englishWord3));
//
//        AccRule accRule = new AccRule();
//        accRule.setGlWholeWriteType(GlWholeWriteType.INPUT_INVOICE);
//        AccRuleElement accRuleElement1 = new AccRuleElement();
//        accRuleElement1.setAccountGroupingType(GlAccountGroupingType.EXPENSES_OTHER);
//        accRuleElement1.setGlAccountSideType(GlAccountSideType.DEBIT);
//        accRuleElement1.setPercents(BigDecimal.valueOf(81));
//        accRuleElement1.setAccRule(accRule);
//
//        AccRuleElement accRuleElement2 = new AccRuleElement();
//        accRuleElement2.setAccountGroupingType(GlAccountGroupingType.TRADE_OTHER_PAYABLES);
//        accRuleElement2.setGlAccountSideType(GlAccountSideType.CREDIT);
//        accRuleElement2.setPercents(BigDecimal.valueOf(100));
//        accRuleElement2.setAccRule(accRule);
//
//        AccRuleElement accRuleElement3 = new AccRuleElement();
//        accRuleElement3.setAccountGroupingType(GlAccountGroupingType.VAT);
//        accRuleElement3.setGlAccountSideType(GlAccountSideType.DEBIT);
//        accRuleElement3.setPercents(BigDecimal.valueOf(19));
//        accRuleElement3.setAccRule(accRule);
//
//        accRule.setRuleElements(List.of(accRuleElement1, accRuleElement2, accRuleElement3));
//        sentence.setAccRule(accRule);
//        Sentence save = sentenceDao.save(sentence);
//        log.info("Saved sentence " + save);

//        Crm nextCrm = crmDao.findAll().iterator().next();
//        AccountingUnit golemv1 = accUnitDao.findByName("golemv1");
//        Iterator<GoodsOutputReceipt> iterator = goodsOutputReceiptDao.findAll().iterator();
//        while(iterator.hasNext()) {
//            GoodsOutputReceipt next1 = iterator.next();
//            next1.setCrm(nextCrm);
//            next1.setDocumentNumber("OGR-" + goodsInOutputReceiptService.getNextNumber("golemv1"));
//            goodsOutputReceiptDao.save(next1);
//        }

        //Iterable<Warehouse> all = warehouseDao.findAll();
//        AccountingUnit golemv1 = accUnitDao.findByName("golemv1");
//        for (Warehouse warehouse : all) {
//
//            warehousesService.createWarehouse(warehouse, golemv1);
//        }


        //SysOutOverSLF4J.sendSystemOutAndErrToSLF4J();
        //glAccountService.createAccounts();
//        AccountingUnit golemv1 = accUnitDao.findByName("golemv1");
//        if (golemv1 == null) {
//            AccountingUnit accountingUnit = new AccountingUnit();
//            accountingUnit.setName("golemv1");
//            accountingUnit.setIco("11111111111");
//            accountingUnit.setDic("1535551313");
//            golemv1 = accUnitDao.save(accountingUnit);
//
//        }
//        if (golemv1 != null && golemv1.getPrimaryAddress() == null) {
//            Address address = new Address();
//            address.setCity("Praha");
//            address.setDescNumber("512/8");
//            address.setStreet("Hluckova");
//            address.setZipCode("19900");
//            golemv1.setPrimaryAddress(address);
//            golemv1 = accUnitDao.save(golemv1);
//            log.info("save accUnit");
//        }
//
//        long count = goodsServicesDao.count();
//        if (count == 0) {
//            GoodsServices goodsServices = new GoodsServices();
//            goodsServices.setPacketOutputPrice(BigDecimal.TEN);
//            goodsServices.setPacketInputPrice(BigDecimal.ONE);
//            goodsServices.setVatPercents(BigDecimal.valueOf(19L));
//            goodsServices.setNumberOfUnitsInPacket(5L);
//            goodsServices.setAccountingUnit(golemv1);
//            goodsServices.setName("rohliky");
//            goodsServices.setUnit("jednice");
//            GoodsServices saveGS = goodsServicesDao.save(goodsServices);
//        }
//        long count1 = crmDao.count();
//        if (count1 < 10) {
//            createCrm(golemv1);
//        }
//
//        Warehouse warehouse = new Warehouse();
//        warehouse.setAccountingUnit(golemv1);
//        warehouse.setName("Sklad Hluckova");
//        Address address = new Address();
//        address.setZipCode("28800");
//        address.setStreet("Chelcickeho");
//        address.setDescNumber("582");
//        address.setCity("Kolin");
//        warehouse.setAddress(address);
//        Warehouse savedWarehouse = warehouseDao.save(warehouse);
//
//        //naskladneni start
//        WarehouseGoodsServices warehouseGoodsServicesRohliky = new WarehouseGoodsServices();
//        warehouseGoodsServicesRohliky.setGoodsServices(goodsServicesDao.findAll().iterator().next());
//        warehouseGoodsServicesRohliky.setWarehouse(savedWarehouse);
//        final GoodsInputItem goodsInputItem1 = new GoodsInputItem();
//        goodsInputItem1.setInputPrice(BigDecimal.valueOf(20));
//        goodsInputItem1.setNumberOfPackets(10000L);
//
//        //warehouseGoodsServicesRohliky.setGoodsInputs(Set.of(goodsInputItem1));
//        WarehouseGoodsServices savedWareGoodsSerRohliky = warehouseGoodsServicesDao.save(warehouseGoodsServicesRohliky);
//        goodsInputItem1.setWarehouseGoodsServices(savedWareGoodsSerRohliky);
//        goodsInputItemDao.save(goodsInputItem1);
    }

    private void createCrm(AccountingUnit accountingUnit) {
        Crm crm = new Crm();
        crm.setIco("111111111");
        crm.setDic("222222222");
        crm.setName("Puma");
        Address address = new Address();
        address.setCity("Kolin");
        address.setDescNumber("582");
        address.setStreet("Chelcickeho");
        address.setZipCode("28800");
        crm.setPrimaryAddress(address);
        crm.setContactType(ContactType.EMAIL);
        crm.setEmail("petr.kmoch@ibm.com");
        crm.setCrmType(CrmType.CUSTOMER);
        crm.setAccountingUnit(accountingUnit);
        crmDao.save(crm);
        Iterable<Crm> all = crmDao.findAll();
        if (all.iterator().hasNext()) {
            Crm next = all.iterator().next();
            log.info(next.getAccountingUnit().getPrimaryAddress().getStreet());
        }
    }

}
