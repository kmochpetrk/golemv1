package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.dtos.InternalOrderDto;
import com.golemv1.golemv1.model.InternalOrdersResponse;
import com.golemv1.golemv1.services.InternalOrderService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/internal-orders")
@CrossOrigin
@Slf4j
public class InternalOrderController {

    @Autowired
    private InternalOrderService internalOrderService;

    @RequestMapping
    public ResponseEntity<InternalOrdersResponse> getInternalOrders(HttpServletRequest request,
                                                                    Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        Page<InternalOrderDto> orders = internalOrderService.getOrders(pageable, company);
        return new ResponseEntity<>(new InternalOrdersResponse(orders), HttpStatus.OK);
    }
}
