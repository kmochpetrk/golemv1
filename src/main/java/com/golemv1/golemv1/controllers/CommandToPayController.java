package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.dtos.CommandToPayDto;
import com.golemv1.golemv1.entities.CommandToPay;
import com.golemv1.golemv1.entities.GlAccount;
import com.golemv1.golemv1.model.AccountsResponse;
import com.golemv1.golemv1.model.CommandToPaysResponse;
import com.golemv1.golemv1.model.NextNumberResponse;
import com.golemv1.golemv1.services.CommandToPayService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/command-to-pay")
@CrossOrigin
@Slf4j
public class CommandToPayController {

    @Autowired
    private CommandToPayService commandToPayService;

    @RequestMapping
    public ResponseEntity<CommandToPaysResponse> getCommandToPays(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        log.info("Attribute from filter: " + username);
        String company = JwtTokenUtils.getCompanyFromSub(username);
        Page<CommandToPayDto> commandToPayDtos = commandToPayService.getAllByCompany(company, pageable);
        //System.out.println("First: " +  commandToPayDtos.iterator().next().getDescription());


//        if (username.equals("golemv1:petrkmoch")) {
//            log.info("Right user!!!");
////            List<GlAccount> returnList = StreamSupport
////                    .stream(allAccounts.spliterator(), false)
////                    .collect(Collectors.toList());
////            AccountsResponse accountsResponse = new AccountsResponse();
////            accountsResponse.setAccounts(returnList);
//            return new ResponseEntity<CommandToPaysResponse>(new CommandToPaysResponse(commandToPayDtos), HttpStatus.OK);
//        }
        return new ResponseEntity<>(new CommandToPaysResponse(commandToPayDtos), HttpStatus.OK);
    }

    @RequestMapping(value = "/ctp-null")
    public ResponseEntity<CommandToPaysResponse> getCommandToPaysCtpNull(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        log.info("Attribute from filter: " + username);
        String company = JwtTokenUtils.getCompanyFromSub(username);
        Page<CommandToPayDto> commandToPayDtos = commandToPayService.getAllByCompanyNotCtp(company, pageable);
        //System.out.println("First: " +  commandToPayDtos.iterator().next().getDescription());


//        if (username.equals("golemv1:petrkmoch")) {
//            log.info("Right user!!!");
////            List<GlAccount> returnList = StreamSupport
////                    .stream(allAccounts.spliterator(), false)
////                    .collect(Collectors.toList());
////            AccountsResponse accountsResponse = new AccountsResponse();
////            accountsResponse.setAccounts(returnList);
//            return new ResponseEntity<CommandToPaysResponse>(new CommandToPaysResponse(commandToPayDtos), HttpStatus.OK);
//        }
        return new ResponseEntity<>(new CommandToPaysResponse(commandToPayDtos), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<CommandToPay> createCommandToPay(HttpServletRequest request, @RequestBody CommandToPayDto commandToPayDto) {
        final String username = (String) request.getAttribute("username");
        log.info("Attribute from filter: " + username);
        String company = JwtTokenUtils.getCompanyFromSub(username);
        CommandToPay commandToPay = commandToPayService.createCommandToPay(company, commandToPayDto);
        return new ResponseEntity<>(commandToPay, HttpStatus.OK);
    }

    @RequestMapping(value = "/nextnumber")
    public ResponseEntity<NextNumberResponse> getNextNumber(HttpServletRequest request) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        int nextNumber =  commandToPayService.getNextNumberCommandTopay(company);
        log.info("Next number: " + nextNumber);
        return new ResponseEntity<NextNumberResponse>(new NextNumberResponse("CTP-" + nextNumber), HttpStatus.OK);
    }
}
