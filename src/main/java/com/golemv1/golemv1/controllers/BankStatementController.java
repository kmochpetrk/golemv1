package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.dtos.BankStatementDto;
import com.golemv1.golemv1.dtos.InputInvoiceDto;
import com.golemv1.golemv1.entities.BankStatement;
import com.golemv1.golemv1.entities.InputInvoice;
import com.golemv1.golemv1.model.NextNumberResponse;
import com.golemv1.golemv1.services.BankStatementService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/bank-statement")
@CrossOrigin
@Slf4j
public class BankStatementController {

    @Autowired
    private BankStatementService bankStatementService;

    @RequestMapping(value = "/nextnumber")
    public ResponseEntity<NextNumberResponse> getNextNumber(HttpServletRequest request) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        int nextNumber =  bankStatementService.getNextNumberBankStatement(company);
        log.info("Next number: " + nextNumber);
        return new ResponseEntity<NextNumberResponse>(new NextNumberResponse("BS-" + nextNumber), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<BankStatement> createBankStatement(HttpServletRequest request, @RequestBody BankStatementDto bankStatementDto) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        BankStatement bankStatement = bankStatementService.createBankStatement(bankStatementDto, company, bankStatementDto.isIncome());
        return new ResponseEntity<>(bankStatement, HttpStatus.OK);
    }
}
