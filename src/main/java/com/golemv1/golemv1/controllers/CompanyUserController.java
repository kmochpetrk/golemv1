package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.entities.CompanyUser;
import com.golemv1.golemv1.services.CompanyUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@Slf4j
@RequestMapping("/api/create-company")
public class CompanyUserController {

    @Autowired
    private CompanyUserService companyUserService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<CompanyUser> createCompanyUser(@RequestBody CompanyUser companyUser) {
        return new ResponseEntity<>(companyUserService.createCompanyUser(companyUser), HttpStatus.OK);
    }

}
