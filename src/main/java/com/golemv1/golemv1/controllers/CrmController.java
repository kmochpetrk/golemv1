package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.CrmDao;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.Crm;
import com.golemv1.golemv1.model.CrmsResponse;
import com.golemv1.golemv1.services.CrmService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/crm")
@CrossOrigin
@Slf4j
public class CrmController {

    @Autowired
    private CrmDao crmDao;

    @Autowired
    private CrmService crmService;

    @Autowired
    private AccUnitDao accUnitDao;

    @RequestMapping
    public ResponseEntity<CrmsResponse> getAllGlAccounts(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        log.info("Attribute from filter: " + username);
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        Page<Crm> crms = crmDao.findAllByAccountingUnit(accountingUnit, pageable);
        //System.out.println("First: " +  crms.iterator().next().getName());



//        if (username.equals("golemv1:petrkmoch")) {
//            log.info("Right user!!!");
////            List<GlAccount> returnList = StreamSupport
////                    .stream(allAccounts.spliterator(), false)
////                    .collect(Collectors.toList());
////            AccountsResponse accountsResponse = new AccountsResponse();
////            accountsResponse.setAccounts(returnList);
//            return new ResponseEntity<CrmsResponse>(new CrmsResponse(crms), HttpStatus.OK);
//        }
        return new ResponseEntity<CrmsResponse>(new CrmsResponse(crms), HttpStatus.OK);
        //return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Crm> createAccount(HttpServletRequest request, @RequestBody Crm crm) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        Crm crmSaved = crmService.createCrm(crm, company);
        return new ResponseEntity<Crm>(crmSaved, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Crm> updateAccount(HttpServletRequest request, @RequestBody Crm crm, @PathVariable Long id) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        Crm crmSaved = crmService.updateCrm(crm, company);
        return new ResponseEntity<Crm>(crmSaved, HttpStatus.OK);
    }

    @RequestMapping("/{id}")
    public ResponseEntity<Crm> getAccountById(HttpServletRequest request, @PathVariable Long id) {
        Crm crmSaved = crmService.getById(id);
        return new ResponseEntity<Crm>(crmSaved, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Crm> deleteCrmById(HttpServletRequest request, @PathVariable Long id) {
        crmService.deleteById(id);
        return new ResponseEntity<Crm>(HttpStatus.OK);
    }

}
