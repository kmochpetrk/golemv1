package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.daos.*;
import com.golemv1.golemv1.dtos.BookingDto;
import com.golemv1.golemv1.dtos.GlWholeWriteDto;
import com.golemv1.golemv1.dtos.InputInvoiceDto;
import com.golemv1.golemv1.entities.*;
import com.golemv1.golemv1.entities.language.AccRule;
import com.golemv1.golemv1.entities.language.AccRuleElement;
import com.golemv1.golemv1.entities.language.EnglishWord;
import com.golemv1.golemv1.entities.language.Sentence;
import com.golemv1.golemv1.model.AccountsResponse;
import com.golemv1.golemv1.model.BookingsResponse;
import com.golemv1.golemv1.model.language.GeneralAccountingRequest;
import com.golemv1.golemv1.model.language.GeneralAccountingResponse;
import com.golemv1.golemv1.model.language.GeneralAccountingTrainRequest;
import com.golemv1.golemv1.model.language.GeneralAccountingTrainResponse;
import com.golemv1.golemv1.services.GeneralAccountingService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/general_accounting")
@CrossOrigin
@Slf4j
public class GeneralAccountingController {

    @Autowired
    private EnglishWordDao englishWordDao;

    @Autowired
    private RuleDao ruleDao;

    @Autowired
    private RuleElementDao ruleElementDao;

    @Autowired
    private SentenceDao sentenceDao;

    @Autowired
    private GeneralAccountingService generalAccountingService;

    @Autowired
    private AccUnitDao accUnitDao;

    @RequestMapping(value = "/bookings")
    public ResponseEntity<BookingsResponse> getAllBookings(HttpServletRequest request, Pageable pageable, @RequestParam String docType) {
        final String username = (String) request.getAttribute("username");
        log.info("Attribute from filter: " + username);
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        Page<GlWholeWriteDto> allBookings = generalAccountingService.getPageableAllBookings(pageable, accountingUnit, docType);
        //System.out.println("First: " +  allBookings.iterator().next().getDocumentNumber());



//        if (username.equals("golemv1:petrkmoch")) {
//            log.info("Right user!!!");
////            List<GlAccount> returnList = StreamSupport
////                    .stream(allAccounts.spliterator(), false)
////                    .collect(Collectors.toList());
////            AccountsResponse accountsResponse = new AccountsResponse();
////            accountsResponse.setAccounts(returnList);
//            return new ResponseEntity<BookingsResponse>(new BookingsResponse(allBookings), HttpStatus.OK);
//        }

        //return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        return new ResponseEntity<BookingsResponse>(new BookingsResponse(allBookings), HttpStatus.OK);
    }

    @RequestMapping(value = "/bookings", method = RequestMethod.POST)
    public ResponseEntity<GlWholeWrite> createGoodsServices(HttpServletRequest request, @RequestBody BookingDto bookingDto) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        GlWholeWrite glWholeWrite = generalAccountingService.createNewBooking(bookingDto, company);
        return new ResponseEntity<>(glWholeWrite, HttpStatus.OK);
    }

    @RequestMapping(value = "/bookings", method = RequestMethod.PUT)
    public ResponseEntity<GlWholeWrite> cancel(HttpServletRequest request, @RequestBody BookingDto bookingDto) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        GlWholeWrite glWholeWrite = generalAccountingService.cancel(bookingDto, company);
        return new ResponseEntity<>(glWholeWrite, HttpStatus.OK);
    }

    @RequestMapping(value = "/predict", method = RequestMethod.POST)
    public ResponseEntity<GeneralAccountingResponse> predict(@RequestBody GeneralAccountingRequest generalAccountingRequest) {
        String[] splitArray = generalAccountingRequest.getSentence().split("\\s+");
        Map<String, List<AccRule>> predictMap = new HashMap<>();
        for (String word: splitArray) {
            List<EnglishWord> allByWord = englishWordDao.findAllByWord(word);
            EnglishWord englishWord = (allByWord != null && !allByWord.isEmpty()) ? allByWord.get(0) : returnEmptyEnglishWord();
            List<Sentence> sentences = (generalAccountingRequest.getGlWholeWriteType() != null && englishWord.getSentences() != null && !englishWord.getSentences().isEmpty()) ?
                    englishWord.getSentences().stream().filter(sentence -> sentence.getAccRule().getGlWholeWriteType().equals(GlWholeWriteType.valueOf(generalAccountingRequest.getGlWholeWriteType()))).collect(Collectors.toList())
                    : englishWord.getSentences();
            List<AccRule> rules = new ArrayList<>();
            for (Sentence sentence : sentences) {
                rules.add(sentence.getAccRule());
            }
            predictMap.put(word, rules);
        }
        AccRule maxRule = null;
        int maxCommon1 = 0;
        for (String word1 : splitArray) {
            for (AccRule rule1 : predictMap.get(word1)) {
                int maxRuleCommon2 = rule1.getSentenceSet().size();
                int maxRuleCommon1 = 1;
                for (String word2 : splitArray) {
                    if (!word1.equals(word2)) {
                        for (AccRule rule2 : predictMap.get(word2)) {
                            if (rule1.getId().equals(rule2.getId())) {
                                maxRuleCommon1++;
                                maxRuleCommon2 = maxRuleCommon2 + rule2.getSentenceSet().size();
                                break;
                            }
                        }
                    }
                }
                if (maxRuleCommon1 > maxCommon1) {
                    maxRule = rule1;
                    maxCommon1 = maxRuleCommon1;
                    maxRuleCommon2 = maxRule.getSentenceSet().size();
                } else if (maxRuleCommon1 == maxCommon1) {
                    if (maxRuleCommon2 > rule1.getSentenceSet().size()) {
                        maxRule = rule1;
                        maxCommon1 = maxRuleCommon1;
                        maxRuleCommon2 = maxRule.getSentenceSet().size();
                    }
                }
            }
        }
        GeneralAccountingResponse generalAccountingResponse = new GeneralAccountingResponse();
        generalAccountingResponse.setDecided(true);
        generalAccountingResponse.setRule(maxRule);
        return new ResponseEntity<GeneralAccountingResponse>(generalAccountingResponse, HttpStatus.OK);
    }

    private EnglishWord returnEmptyEnglishWord() {
        final EnglishWord englishWord = new EnglishWord();
        englishWord.setSentences(new ArrayList<>());
        return englishWord;
    }

    @RequestMapping(value = "/train", method = RequestMethod.POST)
    public ResponseEntity<GeneralAccountingTrainResponse> train(@RequestBody GeneralAccountingTrainRequest generalAccountingTrainRequest) {

        List<EnglishWord> words = new ArrayList<>();

        String[] splitArray = generalAccountingTrainRequest.getSentence().split("\\s+");
        for (String word : splitArray) {
            List<EnglishWord> allByWord = englishWordDao.findAllByWord(word);
            EnglishWord englishWord;
            if (allByWord == null || allByWord.isEmpty()) {
                englishWord = new EnglishWord();
                englishWord.setWord(word);
                englishWord = englishWordDao.save(englishWord);
                //todo other languages
            } else {
                log.error("2 instance 1 slova " + word);
                englishWord = allByWord.get(0);
            }
            words.add(englishWord);
        }

        AccRule accRuleReturn = null;
        List<AccRule> allRules = ruleDao.findAll();
        for (AccRule accRule : allRules) {
            if (accRule.getGlWholeWriteType().equals(generalAccountingTrainRequest.getGlWholeWriteType())) {
                if (compareRules(generalAccountingTrainRequest.getRule(), accRule)) {
                    accRuleReturn = accRule;
                }
            }
        }
        if (accRuleReturn == null) {
            accRuleReturn = new AccRule();
            accRuleReturn.setGlWholeWriteType(generalAccountingTrainRequest.getGlWholeWriteType());
            accRuleReturn.setRuleElements(generalAccountingTrainRequest.getRule().getRuleElements());
            accRuleReturn = ruleDao.save(accRuleReturn);
            for (AccRuleElement ruleElement : generalAccountingTrainRequest.getRule().getRuleElements()) {
                ruleElement.setAccRule(accRuleReturn);
                ruleElementDao.save(ruleElement);
            }
        }
        Sentence sentence = new Sentence();
        sentence.setAccRule(accRuleReturn);
        sentence.setWords(words);
        Sentence savedSentence = sentenceDao.save(sentence);

        Optional<Sentence> readInstance = sentenceDao.findById(savedSentence.getId());

        ResponseEntity<GeneralAccountingTrainResponse> generalAccountingTrainResponseResponseEntity =
                new ResponseEntity<>(new GeneralAccountingTrainResponse(true, readInstance.get()), HttpStatus.OK);
        return generalAccountingTrainResponseResponseEntity;
    }


    private boolean compareRules(AccRule rule1, AccRule rule2) {
        if (rule1.getRuleElements().size() != rule2.getRuleElements().size()) {
            return false;
        }
        for (AccRuleElement accRuleElement1 : rule1.getRuleElements()) {
            boolean ruleSame = false;
            for (AccRuleElement accRuleElement2 : rule2.getRuleElements()) {
                if (accRuleElement1.getAccountGroupingType().equals(accRuleElement2.getAccountGroupingType()) &&
                    accRuleElement1.getGlAccountSideType().equals(accRuleElement2.getGlAccountSideType()) &&
                accRuleElement1.getPercents().compareTo(accRuleElement2.getPercents()) == 0) {
                    ruleSame = true;
                }
            }
            if (!ruleSame) {
                return false;
            }
        }
        return true;
    }

}
