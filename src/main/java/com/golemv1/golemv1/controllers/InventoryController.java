package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.dtos.AmortizationRequestDto;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.GlAccount;
import com.golemv1.golemv1.entities.InputInvoice;
import com.golemv1.golemv1.entities.InventoryItem;
import com.golemv1.golemv1.model.InputInvoicesResponse;
import com.golemv1.golemv1.model.InventoryListResponse;
import com.golemv1.golemv1.model.NextNumberResponse;
import com.golemv1.golemv1.services.InventoryService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/inventory")
@CrossOrigin
@Slf4j
public class InventoryController {

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private AccUnitDao accUnitDao;

    @RequestMapping
    public ResponseEntity<InventoryListResponse> getInputInvoices(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit byName = accUnitDao.findByName(company);
        Page<InventoryItem> all = inventoryService.getInventoryItems(byName, pageable);
        return new ResponseEntity<>(new InventoryListResponse(all), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<InventoryItem> createAccount(HttpServletRequest request, @RequestBody InventoryItem inventoryItem) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        InventoryItem accountSaved = inventoryService.createInventory(inventoryItem, company);
        return new ResponseEntity<>(inventoryItem, HttpStatus.OK);
    }

    @RequestMapping(value = "/amortization", method = RequestMethod.POST)
    public ResponseEntity<InventoryItem> createAmortization(HttpServletRequest request, @RequestBody AmortizationRequestDto amortizationRequestDto) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        InventoryItem inventoryItem = inventoryService.processAmortization(amortizationRequestDto, company);
        return new ResponseEntity<>(inventoryItem, HttpStatus.OK);
    }

    @RequestMapping(value = "/nextnumber")
    public ResponseEntity<NextNumberResponse> getNextNumber(HttpServletRequest request) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        int nextNumber =  inventoryService.getNextNumber(company);
        log.info("Next number: " + nextNumber);
        return new ResponseEntity<NextNumberResponse>(new NextNumberResponse("INV-" + nextNumber), HttpStatus.OK);
    }

}
