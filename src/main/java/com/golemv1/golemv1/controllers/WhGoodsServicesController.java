package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.daos.WarehouseDao;
import com.golemv1.golemv1.daos.WarehouseGoodsServicesDao;
import com.golemv1.golemv1.entities.Warehouse;
import com.golemv1.golemv1.entities.WarehouseGoodsServices;
import com.golemv1.golemv1.model.WarehouseGoodsServicesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/warehousegoodsservices")
public class WhGoodsServicesController {


    @Autowired
    private WarehouseGoodsServicesDao warehouseGoodsServicesDao;

    @Autowired
    private WarehouseDao warehouseDao;

    @RequestMapping
    public ResponseEntity<WarehouseGoodsServicesResponse> getWarehouses(HttpServletRequest request, Pageable pageable) {
        return new ResponseEntity<WarehouseGoodsServicesResponse>(new WarehouseGoodsServicesResponse(warehouseGoodsServicesDao.findAll(pageable)),
                HttpStatus.OK);
    }

    @RequestMapping("/{warehouseId}")
    public ResponseEntity<WarehouseGoodsServicesResponse> getWarehouses(HttpServletRequest request, Pageable pageable, @PathVariable Long warehouseId) {
        Optional<Warehouse> byId = warehouseDao.findById(warehouseId);
        Page<WarehouseGoodsServices> all = warehouseGoodsServicesDao.findAllByWarehouse(byId.get(), pageable);
        all.forEach(whGS -> {
            Long numberInCollect = whGS.getGoodsInputs().stream().map(input -> input.getNumberOfPackets()).reduce(0L, Long::sum);
            Long numberOutCollect = whGS.getGoodsOutputs().stream().map(input -> input.getNumberOfPackets()).reduce(0L, Long::sum);
            BigDecimal averageInPriceCollect = BigDecimal.ZERO;
            BigDecimal averageOutPriceCollect = BigDecimal.ZERO;
            if ((numberInCollect) > 0) {
                averageInPriceCollect = whGS.getGoodsInputs().stream().map(input
                        -> input.getInputPrice().multiply(BigDecimal.valueOf(input.getNumberOfPackets()))).reduce(BigDecimal.ZERO, BigDecimal::add).divide(BigDecimal.valueOf(numberInCollect), RoundingMode.HALF_UP);
            }
            if ((numberOutCollect) > 0) {
                averageOutPriceCollect = whGS.getGoodsOutputs().stream().map(input
                        -> input.getOutputPrice().multiply(BigDecimal.valueOf(input.getNumberOfPackets()))).reduce(BigDecimal.ZERO, BigDecimal::add).divide(BigDecimal.valueOf(numberOutCollect), RoundingMode.HALF_UP);
            }
            whGS.setCount(numberInCollect - numberOutCollect);
            whGS.setInputPrice(averageInPriceCollect);
            whGS.setOutputPrice(averageOutPriceCollect);
        });
        return new ResponseEntity<WarehouseGoodsServicesResponse>(new WarehouseGoodsServicesResponse(all), HttpStatus.OK);
    }
}
