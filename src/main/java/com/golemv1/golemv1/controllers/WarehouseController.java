package com.golemv1.golemv1.controllers;


import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.Warehouse;
import com.golemv1.golemv1.model.WarehousesResponse;
import com.golemv1.golemv1.services.JwtTokenUtils;
import com.golemv1.golemv1.services.WarehousesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/warehouse")
public class WarehouseController {

    @Autowired
    private WarehousesService warehousesService;

    @Autowired
    private AccUnitDao accUnitDao;

    @RequestMapping
    public ResponseEntity<WarehousesResponse> getWarehouses(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        return new ResponseEntity<WarehousesResponse>(new WarehousesResponse(warehousesService.findAll(pageable, company)),
                HttpStatus.OK);
    }

    @RequestMapping("/{id}")
    public ResponseEntity<Warehouse> getById(@PathVariable Long id) {
        return new ResponseEntity<Warehouse>(warehousesService.getById(id), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Warehouse> createWarehouse(HttpServletRequest request,@RequestBody Warehouse warehouse) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        final AccountingUnit accountingUnit = accUnitDao.findByName(company);
        return new ResponseEntity<Warehouse>(warehousesService.createWarehouse(warehouse, accountingUnit), HttpStatus.OK);
    }


}
