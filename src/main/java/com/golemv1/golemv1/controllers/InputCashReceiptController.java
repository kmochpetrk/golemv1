package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.InputCashReceiptDao;
import com.golemv1.golemv1.dtos.InputCashReceiptDto;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.InputCashReceipt;
import com.golemv1.golemv1.entities.OutputInvoice;
import com.golemv1.golemv1.model.InputCashReceiptResponse;
import com.golemv1.golemv1.model.NextNumberResponse;
import com.golemv1.golemv1.services.GeneratePdfReportService;
import com.golemv1.golemv1.services.InputCashReceiptService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import com.itextpdf.text.DocumentException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.util.Optional;

@RestController
@RequestMapping("/api/input-cash-receipt")
@CrossOrigin
@Slf4j
public class InputCashReceiptController {

    @Autowired
    private InputCashReceiptService inputCashReceiptService;

    @Autowired
    private AccUnitDao accUnitDao;

    @Autowired
    private InputCashReceiptDao inputCashReceiptDao;

    @Autowired
    private GeneratePdfReportService generatePdfReportService;

    @RequestMapping(value = "/nextnumber")
    public ResponseEntity<NextNumberResponse> getNextNumber(HttpServletRequest request) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        int nextNumber =  inputCashReceiptService.getNextNumber(company);
        log.info("Next number: " + nextNumber);
        return new ResponseEntity<>(new NextNumberResponse("ICR-" + nextNumber), HttpStatus.OK);
    }

    @RequestMapping
    public ResponseEntity<InputCashReceiptResponse> getInputInvoices(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit byName = accUnitDao.findByName(company);
        Page<InputCashReceipt> all = inputCashReceiptDao.findAllByAccountingUnit(byName, pageable);
        return new ResponseEntity<>(new InputCashReceiptResponse(all), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<InputCashReceipt> createGoodsServices(HttpServletRequest request, @RequestBody InputCashReceiptDto inputCashReceiptDto) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        InputCashReceipt inputCashReceipt = inputCashReceiptService.createInputCashReceipt(inputCashReceiptDto, company);
        return new ResponseEntity<>(inputCashReceipt, HttpStatus.OK);
    }

    @RequestMapping(value = "/pdf/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> createPdf(HttpServletRequest request, @PathVariable Long id) throws DocumentException {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        Optional<InputCashReceipt> inputCashReceipt = inputCashReceiptDao.findById(id);
        ByteArrayInputStream bis = generatePdfReportService.inputCashReceiptPdf(inputCashReceipt.get(), company);
        var headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=invoice.pdf");
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
}
