package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.InputInvoiceDao;
import com.golemv1.golemv1.dtos.*;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.GoodsServiceItems;
import com.golemv1.golemv1.entities.InputInvoice;
import com.golemv1.golemv1.model.InputInvoicesMediumResponse;
import com.golemv1.golemv1.model.InputInvoicesResponse;
import com.golemv1.golemv1.model.InputInvoicesSmallResponse;
import com.golemv1.golemv1.model.NextNumberResponse;
import com.golemv1.golemv1.services.GoodsInOutputReceiptService;
import com.golemv1.golemv1.services.InputInvoiceService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/inputinvoice")
@CrossOrigin
@Slf4j
public class InputInvoiceController {

    @Autowired
    private InputInvoiceDao inputInvoicesDao;

    @Autowired
    private GoodsInOutputReceiptService goodsInOutputReceiptService;

    @Autowired
    private InputInvoiceService inputInvoiceService;

    @Autowired
    private AccUnitDao accUnitDao;

    @RequestMapping(value = "/nextnumber")
    public ResponseEntity<NextNumberResponse> getNextNumber(HttpServletRequest request) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        int nextNumber =  inputInvoiceService.getNextNumber(company);
        log.info("Next number: " + nextNumber);
        return new ResponseEntity<NextNumberResponse>(new NextNumberResponse("II-" + nextNumber), HttpStatus.OK);
    }

    @RequestMapping
    public ResponseEntity<InputInvoicesResponse> getInputInvoices(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit byName = accUnitDao.findByName(company);
        Page<InputInvoice> all = inputInvoicesDao.findAllByAccountingUnit(byName, pageable);
        return new ResponseEntity<>(new InputInvoicesResponse(all), HttpStatus.OK);
    }

    @RequestMapping(value = "/not-paid")
    public ResponseEntity<InputInvoicesMediumResponse> getInputInvoicesNotPaid(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit byName = accUnitDao.findByName(company);
        Page<InputInvoice> all = inputInvoicesDao.findAllByAccountingUnitAndCommandToPayIsNullAndBsIdIsNullAndPayDateIsBefore(byName, LocalDate.now(), pageable);
        Page<InputInvoiceMediumDto> all2 = all.map(this::mapIi);
        return new ResponseEntity<>(new InputInvoicesMediumResponse(all2), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<InputInvoice> createInputInvoice(HttpServletRequest request, @RequestBody InputInvoiceDto inputInvoiceDto) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        InputInvoice inputInvoice = goodsInOutputReceiptService.createInputInvoice(inputInvoiceDto, company);
        return new ResponseEntity<>(inputInvoice, HttpStatus.OK);
    }

    @RequestMapping(value = "/small")
    public ResponseEntity<InputInvoicesSmallResponse> getOutputInvoicesSmall(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit byName = accUnitDao.findByName(company);
        Page<InputInvoiceSmallDto> all = inputInvoicesDao.findAllByAccountingUnitAndCommandToPayIsNull(byName, pageable).map(bigInvoice -> new InputInvoiceSmallDto(
                bigInvoice.getAccountingDate(), bigInvoice.getVariableSymbol(), bigInvoice.getCrm().getName(),
                (bigInvoice.getGoodsServiceItems() == null || bigInvoice.getGoodsServiceItems().isEmpty()) ? bigInvoice.getTotal() : bigInvoice.getGoodsServiceItems().stream().reduce(BigDecimal.ZERO,(partial, addNew) ->
                        partial.add(addNew.getPriceTotal() != null ? addNew.getPriceTotal() : BigDecimal.ZERO),
                        BigDecimal::add), bigInvoice.getId(), bigInvoice.getBankAccount() != null ? bigInvoice.getBankAccount() : (bigInvoice.getCrm() != null ? bigInvoice.getCrm().getBankAccount() : ""), bigInvoice.getDocumentNumber()));
        return new ResponseEntity<>(new InputInvoicesSmallResponse(all), HttpStatus.OK);
    }

    @RequestMapping(value = "/small2/on-command")
    public ResponseEntity<InputInvoicesSmallResponse> getOutputInvoicesSmallOnCommand(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit byName = accUnitDao.findByName(company);
        Page<InputInvoiceSmallDto> all = inputInvoicesDao.findAllByAccountingUnitAndCommandToPayIsNotNullAndBsIdIsNull(byName, pageable).map(bigInvoice -> new InputInvoiceSmallDto(
                bigInvoice.getAccountingDate(), bigInvoice.getVariableSymbol(), bigInvoice.getCrm().getName(),
                bigInvoice.getGoodsServiceItems().stream().reduce(BigDecimal.ZERO,(partial, addNew) ->
                                partial.add(addNew.getPriceTotal() != null ? addNew.getPriceTotal() : BigDecimal.ZERO),
                        BigDecimal::add), bigInvoice.getId(), bigInvoice.getCrm() != null ? bigInvoice.getCrm().getBankAccount() : "", bigInvoice.getDocumentNumber()));
        return new ResponseEntity<>(new InputInvoicesSmallResponse(all), HttpStatus.OK);
    }

    private InputInvoiceMediumDto mapIi(InputInvoice inputInvoice) {
        InputInvoiceMediumDto inputInvoiceMediumDto = new InputInvoiceMediumDto();
        inputInvoiceMediumDto.setBsId(inputInvoice.getBsId());
        inputInvoiceMediumDto.setTransportType(inputInvoice.getTransportType());
        inputInvoiceMediumDto.setPaymentType(inputInvoice.getPaymentType());
        inputInvoiceMediumDto.setCrmName(inputInvoice.getCrm().getName());
        inputInvoiceMediumDto.setGORIds(inputInvoice.getGoodsInputReceipts().stream().map(a -> a.getId()).collect(Collectors.toList()));
        inputInvoiceMediumDto.setIssueDate(inputInvoice.getAccountingDate());
        inputInvoiceMediumDto.setPayDate(inputInvoice.getPayDate());
        inputInvoiceMediumDto.setId(inputInvoice.getId());
        inputInvoiceMediumDto.setTotal(inputInvoice.getGoodsServiceItems().stream().reduce(BigDecimal.ZERO,(partial, addNew) ->
                        partial.add(addNew.getPriceTotal() != null ? addNew.getPriceTotal() : BigDecimal.ZERO),
                BigDecimal::add));
        inputInvoiceMediumDto.setTotalTax(inputInvoice.getGoodsServiceItems().stream().reduce(BigDecimal.ZERO,(partial, addNew) ->
                        partial.add(addNew.getVatTotal() != null ? addNew.getVatTotal() : BigDecimal.ZERO),
                BigDecimal::add));
        inputInvoiceMediumDto.setVariableSymbol(inputInvoice.getDocumentNumber());
        inputInvoiceMediumDto.setWarehouseGoodsServicesDtos(inputInvoice.getGoodsServiceItems().stream().map(a -> createWareHouseGoodsServiceDto(a)).collect(Collectors.toList()));
        return inputInvoiceMediumDto;
    }

    private WarehouseGoodsServicesDto createWareHouseGoodsServiceDto(GoodsServiceItems a) {
        WarehouseGoodsServicesDto warehouseGoodsServicesDto = new WarehouseGoodsServicesDto();
        warehouseGoodsServicesDto.setAmountTotal(a.getPriceTotal());
        warehouseGoodsServicesDto.setVatTotal(a.getVatTotal());
        warehouseGoodsServicesDto.setGoodsServicesId(a.getGoodsServices().getId());
        warehouseGoodsServicesDto.setGoodsServicesName(a.getGoodsServices().getName());
        warehouseGoodsServicesDto.setWarehouseId(a.getWarehouseGoodsServices() != null && a.getWarehouseGoodsServices().getWarehouse() != null ? a.getWarehouseGoodsServices().getWarehouse().getId() : null);
        warehouseGoodsServicesDto.setWarehouseName(a.getWarehouseGoodsServices() != null && a.getWarehouseGoodsServices().getWarehouse() != null ? a.getWarehouseGoodsServices().getWarehouse().getName() : null);
        warehouseGoodsServicesDto.setNumberOfUnits(a.getNumberOfUnits());
        return warehouseGoodsServicesDto;

    }
}
