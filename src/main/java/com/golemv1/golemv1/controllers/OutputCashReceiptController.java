package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.OutputCashReceiptDao;
import com.golemv1.golemv1.dtos.OutputCashReceiptDto;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.InputCashReceipt;
import com.golemv1.golemv1.entities.OutputCashReceipt;
import com.golemv1.golemv1.model.NextNumberResponse;
import com.golemv1.golemv1.model.OutputCashReceiptResponse;
import com.golemv1.golemv1.services.GeneratePdfReportService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import com.golemv1.golemv1.services.OutputCashReceiptService;
import com.itextpdf.text.DocumentException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.util.Optional;

@RestController
@RequestMapping("/api/output-cash-receipt")
@CrossOrigin
@Slf4j
public class OutputCashReceiptController {

    @Autowired
    private OutputCashReceiptService outputCashReceiptService;

    @Autowired
    private AccUnitDao accUnitDao;

    @Autowired
    private OutputCashReceiptDao outputCashReceiptDao;

    @Autowired
    private GeneratePdfReportService generatePdfReportService;

    @RequestMapping(value = "/nextnumber")
    public ResponseEntity<NextNumberResponse> getNextNumber(HttpServletRequest request) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        int nextNumber =  outputCashReceiptService.getNextNumber(company);
        log.info("Next number: " + nextNumber);
        return new ResponseEntity<>(new NextNumberResponse("OCR-" + nextNumber), HttpStatus.OK);
    }

    @RequestMapping
    public ResponseEntity<OutputCashReceiptResponse> getInputInvoices(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit byName = accUnitDao.findByName(company);
        Page<OutputCashReceipt> all = outputCashReceiptDao.findAllByAccountingUnit(byName, pageable);
        return new ResponseEntity<>(new OutputCashReceiptResponse(all), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<OutputCashReceipt> createGoodsServices(HttpServletRequest request, @RequestBody OutputCashReceiptDto outputCashReceiptDto) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        OutputCashReceipt inputCashReceipt = outputCashReceiptService.createOutputCashReceipt(outputCashReceiptDto, company);
        return new ResponseEntity<>(inputCashReceipt, HttpStatus.OK);
    }


    @RequestMapping(value = "/pdf/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> createPdf(HttpServletRequest request, @PathVariable Long id) throws DocumentException {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        Optional<OutputCashReceipt> outputCashReceipt = outputCashReceiptDao.findById(id);
        ByteArrayInputStream bis = generatePdfReportService.outputCashReceiptPdf(outputCashReceipt.get(), company);
        var headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=invoice.pdf");
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
}
