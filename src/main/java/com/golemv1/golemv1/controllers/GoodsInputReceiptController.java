package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.dtos.GoodsInputReceiptDto;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.GoodsInputReceipt;
import com.golemv1.golemv1.model.GoodsInputReceiptResponse;
import com.golemv1.golemv1.model.GoodsOutputReceiptsResponse;
import com.golemv1.golemv1.model.NextNumberResponse;
import com.golemv1.golemv1.services.GoodsInOutputReceiptService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping("/api/goodsinputreceipt")
public class GoodsInputReceiptController {

    @Autowired
    private GoodsInOutputReceiptService goodsInOutputReceiptService;

    @Autowired
    private AccUnitDao accUnitDao;

    @RequestMapping
    public ResponseEntity<GoodsInputReceiptResponse> getGoodsInputReceipts(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit byName = accUnitDao.findByName(company);
        Page<GoodsInputReceipt> all = goodsInOutputReceiptService.findAllByAccountingUnit(byName, pageable);
        return new ResponseEntity<GoodsInputReceiptResponse>(new GoodsInputReceiptResponse(all), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<GoodsInputReceipt> createGoodsServices(HttpServletRequest request, @RequestBody GoodsInputReceiptDto goodsServices) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        GoodsInputReceipt goodsServicesSaved = goodsInOutputReceiptService.createGoodsInputReceipt(goodsServices, company);
        return new ResponseEntity<GoodsInputReceipt>(goodsServicesSaved, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<GoodsInputReceipt> updateAccount(HttpServletRequest request, @RequestBody GoodsInputReceipt goodsServices, @PathVariable Long id) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        goodsServices.setAccountingUnit(accUnitDao.findByName(company));
        GoodsInputReceipt accountSaved = goodsInOutputReceiptService.updateGoodsInputReceipt(goodsServices);
        return new ResponseEntity<GoodsInputReceipt>(accountSaved, HttpStatus.OK);
    }

    @RequestMapping("/{id}")
    public ResponseEntity<GoodsInputReceipt> getAccountById(HttpServletRequest request, @PathVariable Long id) {
        GoodsInputReceipt accountSaved = goodsInOutputReceiptService.getById(id);
        return new ResponseEntity<GoodsInputReceipt>(accountSaved, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<GoodsInputReceipt> deleteGoodsServicesById(HttpServletRequest request, @PathVariable Long id) {
        goodsInOutputReceiptService.deleteById(id);
        return new ResponseEntity<GoodsInputReceipt>(HttpStatus.OK);
    }

    @RequestMapping(value = "/nextnumber")
    public ResponseEntity<NextNumberResponse> getNextNumber(HttpServletRequest request) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        int nextNumber =  goodsInOutputReceiptService.getNextNumberInputGoodsReceipt(company);
        log.info("Next number: " + nextNumber);
        return new ResponseEntity<NextNumberResponse>(new NextNumberResponse("IGR-" + nextNumber), HttpStatus.OK);
    }

    @RequestMapping(value = "/notinvoiced")
    public ResponseEntity<GoodsInputReceiptResponse> getAllNotInvoiced(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        return new ResponseEntity<GoodsInputReceiptResponse>(new GoodsInputReceiptResponse(goodsInOutputReceiptService.findAllNotInvoicedGoodsInputReceipts(pageable, company)),
                HttpStatus.OK);
    }
}
