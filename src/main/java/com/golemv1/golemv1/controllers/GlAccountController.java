package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.entities.*;
import com.golemv1.golemv1.model.AccountsResponse;
import com.golemv1.golemv1.services.GlAccountService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/api/glaccount")
@CrossOrigin
@Slf4j
public class GlAccountController {

    @Autowired
    private GlAccountService glAccountService;

    @Autowired
    private AccUnitDao accUnitDao;

    @RequestMapping
    public ResponseEntity<AccountsResponse> getAllGlAccounts(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        log.info("Attribute from filter: " + username);
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        Page<GlAccount> allAccounts = glAccountService.getAllAccounts(pageable, accountingUnit);
        //System.out.println("First: " +  allAccounts.iterator().next().getDescription());



//        if (username.equals("golemv1:petrkmoch")) {
//            log.info("Right user!!!");
////            List<GlAccount> returnList = StreamSupport
////                    .stream(allAccounts.spliterator(), false)
////                    .collect(Collectors.toList());
////            AccountsResponse accountsResponse = new AccountsResponse();
////            accountsResponse.setAccounts(returnList);
//            return new ResponseEntity<AccountsResponse>(new AccountsResponse(allAccounts), HttpStatus.OK);
//        }

        return new ResponseEntity<AccountsResponse>(new AccountsResponse(allAccounts), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<GlAccount> createAccount(HttpServletRequest request, @RequestBody GlAccount glAccount) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        GlAccount accountSaved = glAccountService.createAccount(glAccount, company);
        return new ResponseEntity<GlAccount>(accountSaved, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<GlAccount> updateAccount(HttpServletRequest request, @RequestBody GlAccount glAccount, @PathVariable Long id) {
        GlAccount accountSaved = glAccountService.updateAccount(glAccount);
        return new ResponseEntity<GlAccount>(accountSaved, HttpStatus.OK);
    }

    @RequestMapping("/{id}")
    public ResponseEntity<GlAccount> getAccountById(HttpServletRequest request, @PathVariable Long id) {
        GlAccount accountSaved = glAccountService.getById(id);
        return new ResponseEntity<GlAccount>(accountSaved, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<GlAccount> deleteAccountById(HttpServletRequest request, @PathVariable Long id) {
        glAccountService.deleteById(id);
        return new ResponseEntity<GlAccount>(HttpStatus.OK);
    }

    @RequestMapping(value = "/groups")
    public ResponseEntity<List<String>> getGlAccountGroup(@RequestParam String type, @RequestParam String side) {
        GlAccountType glAccountType = GlAccountType.valueOf(type);
        GlAccountSideType glAccountSideType = GlAccountSideType.valueOf(side);
        Set<GlAccountGroupingType> byTypeAndSide = GlAccountGroupingType.getByTypeAndSide(glAccountType, glAccountSideType);
        List<String> collect = byTypeAndSide.stream().map(element -> element.name()).collect(Collectors.toList());
        return new ResponseEntity<>(collect, HttpStatus.OK);
    }

    @RequestMapping(value = "/bygroup/{group}")
    public ResponseEntity<Set<GlAccount>> getAccountsByGroup(@PathVariable String group, HttpServletRequest request) {
        GlAccountGroupingType glAccountGroupingType = GlAccountGroupingType.valueOf(group.toUpperCase());
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        Set<GlAccount> accounts  = glAccountService.getByGroup(company, glAccountGroupingType);
        return new ResponseEntity<Set<GlAccount>>(accounts, HttpStatus.OK);
    }
}
