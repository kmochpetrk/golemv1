package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.GoodsServicesDao;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.GoodsServices;
import com.golemv1.golemv1.model.GoodsServicesResponse;
import com.golemv1.golemv1.model.NextNumberResponse;
import com.golemv1.golemv1.services.GoodsServicesService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/goodsservices")
@CrossOrigin
@Slf4j
public class GoodsServicesController {

    @Autowired
    private GoodsServicesDao goodsServicesDao;

    @Autowired
    private GoodsServicesService goodsServicesService;

    @Autowired
    private AccUnitDao accUnitDao;

    @RequestMapping
    public ResponseEntity<GoodsServicesResponse> getGoodsServices(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit byName = accUnitDao.findByName(company);
        Page<GoodsServices> all = goodsServicesDao.findAllByAccountingUnit(byName, pageable);
        return new ResponseEntity<GoodsServicesResponse>(new GoodsServicesResponse(all), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<GoodsServices> createGoodsServices(HttpServletRequest request, @RequestBody GoodsServices goodsServices) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        GoodsServices goodsServicesSaved = goodsServicesService.createGoodsServices(goodsServices, company);
        return new ResponseEntity<GoodsServices>(goodsServices, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<GoodsServices> updateAccount(HttpServletRequest request, @RequestBody GoodsServices goodsServices, @PathVariable Long id) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        goodsServices.setAccountingUnit(accUnitDao.findByName(company));
        GoodsServices accountSaved = goodsServicesService.updateGoodsServices(goodsServices);
        return new ResponseEntity<GoodsServices>(accountSaved, HttpStatus.OK);
    }

    @RequestMapping("/{id}")
    public ResponseEntity<GoodsServices> getAccountById(HttpServletRequest request, @PathVariable Long id) {
        GoodsServices accountSaved = goodsServicesService.getById(id);
        return new ResponseEntity<GoodsServices>(accountSaved, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<GoodsServices> deleteGoodsServicesById(HttpServletRequest request, @PathVariable Long id) {
        goodsServicesService.deleteById(id);
        return new ResponseEntity<GoodsServices>(HttpStatus.OK);
    }

    @RequestMapping(value = "/nextnumber")
    public ResponseEntity<NextNumberResponse> getNextNumber(HttpServletRequest request) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        int nextNumber =  goodsServicesService.getNextNumber(company);
        log.info("Next number: " + nextNumber);
        return new ResponseEntity<NextNumberResponse>(new NextNumberResponse("GS-" + nextNumber), HttpStatus.OK);
    }
}
