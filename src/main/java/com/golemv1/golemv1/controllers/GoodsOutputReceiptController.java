package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.dtos.GoodsOutputReceiptDto;
import com.golemv1.golemv1.entities.GoodsOutputReceipt;
import com.golemv1.golemv1.model.GoodsOutputReceiptsResponse;
import com.golemv1.golemv1.model.NextNumberResponse;
import com.golemv1.golemv1.services.GoodsInOutputReceiptService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping("/api/goodsoutputreceipt")
public class GoodsOutputReceiptController {

    @Autowired
    private GoodsInOutputReceiptService goodsInOutputReceiptService;

    @Autowired
    private AccUnitDao accUnitDao;


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<GoodsOutputReceipt> createGoodsServices(HttpServletRequest request, @RequestBody GoodsOutputReceiptDto goodsServices) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        GoodsOutputReceipt goodsServicesSaved = goodsInOutputReceiptService.createGoodsOutputReceipt(goodsServices, company);
        return new ResponseEntity<GoodsOutputReceipt>(goodsServicesSaved, HttpStatus.OK);
    }

    @RequestMapping
    public ResponseEntity<GoodsOutputReceiptsResponse> getAll(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        return new ResponseEntity<GoodsOutputReceiptsResponse>(new GoodsOutputReceiptsResponse(goodsInOutputReceiptService.findAllGoodsOutputReceipts(pageable, company)),
                HttpStatus.OK);
    }

    @RequestMapping(value = "/notinvoiced")
    public ResponseEntity<GoodsOutputReceiptsResponse> getAllNotInvoiced(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        return new ResponseEntity<GoodsOutputReceiptsResponse>(new GoodsOutputReceiptsResponse(goodsInOutputReceiptService.findAllNotInvoicedGoodsOutputReceipts(pageable, company)),
                HttpStatus.OK);
    }

    @RequestMapping(value = "/nextnumber")
    public ResponseEntity<NextNumberResponse> getNextNumber(HttpServletRequest request) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        int nextNumber =  goodsInOutputReceiptService.getNextNumberOutputGoodsReceipt(company);
        log.info("Next number: " + nextNumber);
        return new ResponseEntity<NextNumberResponse>(new NextNumberResponse("OGR-" + nextNumber), HttpStatus.OK);
    }
}
