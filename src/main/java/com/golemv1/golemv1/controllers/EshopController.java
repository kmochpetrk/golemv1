package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.CompanyUserDao;
import com.golemv1.golemv1.daos.GoodsServicesDao;
import com.golemv1.golemv1.daos.WarehouseGoodsServicesDao;
import com.golemv1.golemv1.dtos.EshopOrderFullDto;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.CompanyUser;
import com.golemv1.golemv1.entities.GoodsServices;
import com.golemv1.golemv1.entities.WarehouseGoodsServices;
import com.golemv1.golemv1.model.*;
import com.golemv1.golemv1.services.EshopService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import com.golemv1.golemv1.services.OutputInvoiceService;
import com.golemv1.golemv1.services.WarehousesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.groupingBy;

@RestController
@RequestMapping("/api/eshop")
@CrossOrigin
@Slf4j
public class EshopController {

    @Autowired
    private WarehouseGoodsServicesDao warehouseGoodsServicesDao;

    @Autowired
    private AccUnitDao accUnitDao;

    @Autowired
    private GoodsServicesDao goodsServicesDao;

    @Autowired
    private OutputInvoiceService outputInvoiceService;

    @Autowired
    private EshopService eshopService;

    @Autowired
    private CompanyUserDao companyUserDao;

    @RequestMapping(value = "/{company}")
    public ResponseEntity<EshopModelResponse> getGoodsWithWarehouse(@PathVariable(required = false) String company, Pageable pageable) {
        if (company == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        Page<GoodsServices> allByAccountingUnit = goodsServicesDao.findAllByAccountingUnit(accountingUnit, pageable);
        Iterable<WarehouseGoodsServices> all = warehouseGoodsServicesDao.findAllByGoodsServices_AccountingUnit(accountingUnit);
        Stream<WarehouseGoodsServices> stream = StreamSupport.stream(all.spliterator(), false);
        Map<Long, List<WarehouseGoodsServices>> collect = stream.collect(groupingBy(warehouseGoodsServices -> warehouseGoodsServices.getGoodsServices().getId()));
        List<EshopModel> eshops = new ArrayList<>();
        for (GoodsServices goodsServices : allByAccountingUnit) {
            EshopModel eshopModel = new EshopModel();
            eshopModel.setGoodsServiceId(goodsServices.getId());
            eshopModel.setGoodsServiceNumber(goodsServices.getGoodsServicesNumber());
            eshopModel.setName(goodsServices.getName());
            eshopModel.setItems(new ArrayList<EshopItem>());
            List<WarehouseGoodsServices> warehouseGoodsServices = collect.get(goodsServices.getId());
            if (warehouseGoodsServices != null) {
                for (WarehouseGoodsServices warehouseGoodsServicesOne : warehouseGoodsServices) {
                    EshopItem eshopItem = new EshopItem();
                    eshopItem.setAddress(warehouseGoodsServicesOne.getWarehouse().getAddress());
                    eshopItem.setCount(count(warehouseGoodsServicesOne));
                    eshopItem.setUnitOutputPrice(warehouseGoodsServicesOne.getGoodsServices().getPacketOutputPrice());
                    eshopItem.setVatPercent(warehouseGoodsServicesOne.getGoodsServices().getVatPercents());
                    eshopItem.setWhName(warehouseGoodsServicesOne.getWarehouse().getName());
                    eshopItem.setWarehouseId(warehouseGoodsServicesOne.getWarehouse().getId());
                    eshopItem.setUnitOutputPriceWithVat(
                            warehouseGoodsServicesOne.getGoodsServices().getPacketOutputPrice().add(
                                    (warehouseGoodsServicesOne.getGoodsServices().getVatPercents()
                                            .divide(BigDecimal.valueOf(100L))
                                            .multiply(warehouseGoodsServicesOne.getGoodsServices().getPacketOutputPrice()))));

                    eshopModel.getItems().add(eshopItem);
                }

            }

            eshops.add(eshopModel);

        }
        PageImpl<EshopModel> page = new PageImpl<>(eshops, allByAccountingUnit.getPageable(), allByAccountingUnit.getTotalElements());
        return new ResponseEntity<>(new EshopModelResponse(page), HttpStatus.OK);
    }

    private Long count(WarehouseGoodsServices warehouseGoodsServicesOne) {
        long sumIn = warehouseGoodsServicesOne.getGoodsInputs().stream().mapToLong(input -> input.getNumberOfPackets()).sum();
        long sumOut = warehouseGoodsServicesOne.getGoodsOutputs().stream().mapToLong(output -> output.getNumberOfPackets()).sum();
        return sumIn - sumOut;
    }


    @RequestMapping()
    public ResponseEntity<Map<Tuple, List<WarehouseGoodsServices>>> getGoodsWithWarehouseWoCompany(@PathVariable(required = false) String company) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/nextnumber/{company}")
    public ResponseEntity<NextNumberResponse> getNextNumber(@PathVariable String company) {
        int nextNumber =  outputInvoiceService.getNextNumber(company);
        log.info("Next number eshop: " + nextNumber);
        return new ResponseEntity<NextNumberResponse>(new NextNumberResponse("OI-" + nextNumber), HttpStatus.OK);
    }

    @RequestMapping(value = "/bankaccount/{company}")
    public ResponseEntity<BankAccountResponse> getBankAccount(@PathVariable String company) {
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        log.info("Bank account: " + accountingUnit.getBankAccount());
        return new ResponseEntity<BankAccountResponse>(new BankAccountResponse(accountingUnit.getBankAccount()), HttpStatus.OK);
    }

    @RequestMapping(value = "/order/{company}", method = RequestMethod.POST)
    public ResponseEntity<EshopOrderFullDto> createEshopOrderFullDto(@RequestBody EshopOrderFullDto eshopOrderFullDto,
                                                                     @PathVariable String company) {
        EshopOrderFullDto eshopOrderFullDto1 = eshopService.saveNewOrder(eshopOrderFullDto, company);
        return new ResponseEntity<>(eshopOrderFullDto, HttpStatus.OK);
    }

    @RequestMapping(value = "/companies")
    public ResponseEntity<CompaniesResponse> getCompanies() {
        List<String> result = new ArrayList<String>();
        companyUserDao.findAll().forEach(a -> {
            result.add(a.getCompany());
        });
        return new ResponseEntity<>(new CompaniesResponse(result), HttpStatus.OK);
    }

}
