package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.dtos.AmortizationRequestDto;
import com.golemv1.golemv1.dtos.MonthlySalaryDto;
import com.golemv1.golemv1.dtos.MonthlySalaryRequestDto;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.Employee;
import com.golemv1.golemv1.entities.InventoryItem;
import com.golemv1.golemv1.model.EmployeesResponse;
import com.golemv1.golemv1.model.NextNumberResponse;
import com.golemv1.golemv1.services.EmployeeService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/employee")
@CrossOrigin
@Slf4j
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private AccUnitDao accUnitDao;

    @RequestMapping
    public ResponseEntity<EmployeesResponse> getAllGlAccounts(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        log.info("Attribute from filter: " + username);
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit accountingUnit = accUnitDao.findByName(company);
        Page<Employee> allEmployees = employeeService.getAllEmployees(pageable, accountingUnit);

        return new ResponseEntity<>(new EmployeesResponse(allEmployees), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Employee> createAccount(HttpServletRequest request, @RequestBody Employee employee) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        Employee employeeSaved = employeeService.createEmployee(employee, company);
        return new ResponseEntity<Employee>(employeeSaved, HttpStatus.OK);
    }

    @RequestMapping(value = "/nextnumber")
    public ResponseEntity<NextNumberResponse> getNextNumber(HttpServletRequest request) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        int nextNumber =  employeeService.getNextNumber(company);
        log.info("Next number: " + nextNumber);
        return new ResponseEntity<>(new NextNumberResponse("EMP-" + nextNumber), HttpStatus.OK);
    }

    @RequestMapping(value = "/monthly-salary", method = RequestMethod.POST)
    public ResponseEntity<Employee> createMonthlySalary(HttpServletRequest request, @RequestBody MonthlySalaryRequestDto monthlySalaryDto) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        Employee employee = employeeService.processMonthlySalary(monthlySalaryDto, company);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }
}
