package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.dtos.AccountingStatementItemDto;
import com.golemv1.golemv1.model.BalanceSheetResponse;
import com.golemv1.golemv1.model.ProfitLossResponse;
import com.golemv1.golemv1.services.AccountingStatementsService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/profit-loss")
@CrossOrigin
@Slf4j
public class ProfitAndLossController {

    @Autowired
    private AccountingStatementsService accountingStatementsService;

    @RequestMapping
    public ResponseEntity<ProfitLossResponse> getProfitLoss(HttpServletRequest request) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        List<AccountingStatementItemDto> profitAndLoss = accountingStatementsService.getProfitAndLoss(company);
        return new ResponseEntity<>(new ProfitLossResponse(profitAndLoss), HttpStatus.OK);
    }
}
