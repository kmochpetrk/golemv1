package com.golemv1.golemv1.controllers;

import com.golemv1.golemv1.daos.AccUnitDao;
import com.golemv1.golemv1.daos.OutputInvoicesDao;
import com.golemv1.golemv1.dtos.OutputInvoiceMediumDto;
import com.golemv1.golemv1.dtos.OutputInvoiceSmallDto;
import com.golemv1.golemv1.dtos.OutputInvoiceDto;
import com.golemv1.golemv1.dtos.WarehouseGoodsServicesDto;
import com.golemv1.golemv1.entities.AccountingUnit;
import com.golemv1.golemv1.entities.GoodsServiceItems;
import com.golemv1.golemv1.entities.OutputInvoice;
import com.golemv1.golemv1.model.NextNumberResponse;
import com.golemv1.golemv1.model.OutputInvoicesMediumResponse;
import com.golemv1.golemv1.model.OutputInvoicesResponse;
import com.golemv1.golemv1.model.OutputInvoicesSmallResponse;
import com.golemv1.golemv1.services.GeneratePdfReportService;
import com.golemv1.golemv1.services.GoodsInOutputReceiptService;
import com.golemv1.golemv1.services.JwtTokenUtils;
import com.golemv1.golemv1.services.OutputInvoiceService;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfDocument;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/outputinvoice")
@CrossOrigin
@Slf4j
public class OutputInvoiceController {

    @Autowired
    private OutputInvoicesDao outputInvoicesDao;

    @Autowired
    private GoodsInOutputReceiptService goodsInOutputReceiptService;

    @Autowired
    private OutputInvoiceService outputInvoiceService;

    @Autowired
    private AccUnitDao accUnitDao;


    @Autowired
    private GeneratePdfReportService generatePdfReportService;


    @RequestMapping
    public ResponseEntity<OutputInvoicesResponse> getOutputInvoices(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit byName = accUnitDao.findByName(company);
        Page<OutputInvoice> all = outputInvoicesDao.findAllByAccountingUnit(byName, pageable);
        return new ResponseEntity<>(new OutputInvoicesResponse(all), HttpStatus.OK);
    }

    @RequestMapping(value = "/not-paid")
    public ResponseEntity<OutputInvoicesMediumResponse> getOutputInvoicesNotPaid(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit byName = accUnitDao.findByName(company);
        Page<OutputInvoice> all = outputInvoicesDao.findAllByAccountingUnitAndBsIdIsNullAndPayDateIsBefore(byName, LocalDate.now(), pageable);
        Page<OutputInvoiceMediumDto> all2 = all.map(this::mapOi);
        return new ResponseEntity<>(new OutputInvoicesMediumResponse(all2), HttpStatus.OK);
    }

    @RequestMapping(value = "/small")
    public ResponseEntity<OutputInvoicesSmallResponse> getOutputInvoicesSmall(HttpServletRequest request, Pageable pageable) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        AccountingUnit byName = accUnitDao.findByName(company);
        Page<OutputInvoiceSmallDto> all = outputInvoicesDao.findAllByAccountingUnitAndBsIdIsNull(byName, pageable).map(bigInvoice -> new OutputInvoiceSmallDto(
                bigInvoice.getAccountingDate(), bigInvoice.getDocumentNumber(), bigInvoice.getCrm().getName(),
                (bigInvoice.getGoodsServiceItems() == null || bigInvoice.getGoodsServiceItems().isEmpty()) ? bigInvoice.getTotal() : bigInvoice.getGoodsServiceItems().stream().reduce(BigDecimal.ZERO,(partial, addNew) -> partial.add(addNew.getPriceTotal() != null ? addNew.getPriceTotal() : BigDecimal.ZERO), BigDecimal::add),
                bigInvoice.getId()));
        return new ResponseEntity<>(new OutputInvoicesSmallResponse(all), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<OutputInvoice> createGoodsServices(HttpServletRequest request, @RequestBody OutputInvoiceDto outputInvoiceDto) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        OutputInvoice outputInvoice = goodsInOutputReceiptService.createOutputInvoice(outputInvoiceDto, company);
        return new ResponseEntity<>(outputInvoice, HttpStatus.OK);
    }

    @RequestMapping(value = "/nextnumber")
    public ResponseEntity<NextNumberResponse> getNextNumber(HttpServletRequest request) {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        int nextNumber =  outputInvoiceService.getNextNumber(company);
        log.info("Next number: " + nextNumber);
        return new ResponseEntity<NextNumberResponse>(new NextNumberResponse("OI-" + nextNumber), HttpStatus.OK);
    }

    @RequestMapping(value = "/pdf/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> citiesReport(HttpServletRequest request, @PathVariable Long id) throws DocumentException {
        final String username = (String) request.getAttribute("username");
        String company = JwtTokenUtils.getCompanyFromSub(username);
        Optional<OutputInvoice> byId = outputInvoicesDao.findById(id);
        ByteArrayInputStream bis = generatePdfReportService.outputInvoice(byId.get(), company);
        var headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=invoice.pdf");
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }


    private OutputInvoiceMediumDto mapOi(OutputInvoice outputInvoice) {
        OutputInvoiceMediumDto outputInvoiceMediumDto = new OutputInvoiceMediumDto();
        outputInvoiceMediumDto.setBsId(outputInvoice.getBsId());
        outputInvoiceMediumDto.setTransportType(outputInvoice.getTransportType());
        outputInvoiceMediumDto.setPaymentType(outputInvoice.getPaymentType());
        outputInvoiceMediumDto.setCrmName(outputInvoice.getCrm().getName());
        outputInvoiceMediumDto.setGORIds(outputInvoice.getGoodsOutputReceipts().stream().map(a -> a.getId()).collect(Collectors.toList()));
        outputInvoiceMediumDto.setIssueDate(outputInvoice.getAccountingDate());
        outputInvoiceMediumDto.setPayDate(outputInvoice.getPayDate());
        outputInvoiceMediumDto.setId(outputInvoice.getId());
        outputInvoiceMediumDto.setVariableSymbol(outputInvoice.getDocumentNumber());
        outputInvoiceMediumDto.setTotal(outputInvoice.getGoodsServiceItems().stream().reduce(BigDecimal.ZERO,(partial, addNew) ->
                        partial.add(addNew.getPriceTotal() != null ? addNew.getPriceTotal() : BigDecimal.ZERO),
                BigDecimal::add));
        outputInvoiceMediumDto.setTotalTax(outputInvoice.getGoodsServiceItems().stream().reduce(BigDecimal.ZERO,(partial, addNew) ->
                        partial.add(addNew.getVatTotal() != null ? addNew.getVatTotal() : BigDecimal.ZERO),
                BigDecimal::add));
        outputInvoiceMediumDto.setWarehouseGoodsServicesDtos(outputInvoice.getGoodsServiceItems().stream().map(a -> createWareHouseGoodsServiceDto(a)).collect(Collectors.toList()));
        return outputInvoiceMediumDto;
    }

    private WarehouseGoodsServicesDto createWareHouseGoodsServiceDto(GoodsServiceItems a) {
        WarehouseGoodsServicesDto warehouseGoodsServicesDto = new WarehouseGoodsServicesDto();
        warehouseGoodsServicesDto.setAmountTotal(a.getPriceTotal());
        warehouseGoodsServicesDto.setVatTotal(a.getVatTotal());
        warehouseGoodsServicesDto.setGoodsServicesId(a.getGoodsServices() != null ? a.getGoodsServices().getId() : null);
        warehouseGoodsServicesDto.setGoodsServicesName(a.getGoodsServices() != null ? a.getGoodsServices().getName() : null);
        warehouseGoodsServicesDto.setWarehouseId(a.getWarehouseGoodsServices() != null && a.getWarehouseGoodsServices().getWarehouse() != null ? a.getWarehouseGoodsServices().getWarehouse().getId() : null);
        warehouseGoodsServicesDto.setWarehouseName(a.getWarehouseGoodsServices() != null && a.getWarehouseGoodsServices().getWarehouse() != null ? a.getWarehouseGoodsServices().getWarehouse().getName() : null);
        warehouseGoodsServicesDto.setNumberOfUnits(a.getNumberOfUnits());
        return warehouseGoodsServicesDto;

    }


}
