package com.golemv1.golemv1.model;

import com.golemv1.golemv1.entities.GoodsServices;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
@AllArgsConstructor
public class GoodsServicesResponse {
    private Page<GoodsServices> goodsServices;
}
