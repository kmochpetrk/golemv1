package com.golemv1.golemv1.model;

import com.golemv1.golemv1.entities.Address;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class EshopItem {
    private Long warehouseId;
    private String whName;
    private Address address;
    private Long count;
    private BigDecimal unitOutputPrice;
    private BigDecimal unitOutputPriceWithVat;
    private BigDecimal vatPercent;
}
