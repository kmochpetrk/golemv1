package com.golemv1.golemv1.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Tuple {
    private Long id;
    private String name;
}
