package com.golemv1.golemv1.model;

import com.golemv1.golemv1.entities.GlAccount;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@AllArgsConstructor
public class AccountsResponse {
    private Page<GlAccount> accounts;
}
