package com.golemv1.golemv1.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
@AllArgsConstructor
public class EshopModelResponse {
    private Page<EshopModel> goodsServices;
}
