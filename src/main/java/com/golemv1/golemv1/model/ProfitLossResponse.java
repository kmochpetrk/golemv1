package com.golemv1.golemv1.model;

import com.golemv1.golemv1.dtos.AccountingStatementItemDto;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class ProfitLossResponse {
    private List<AccountingStatementItemDto> profitAndLoss;
}
