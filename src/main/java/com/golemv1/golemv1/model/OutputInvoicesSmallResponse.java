package com.golemv1.golemv1.model;

import com.golemv1.golemv1.dtos.OutputInvoiceSmallDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
@AllArgsConstructor
public class OutputInvoicesSmallResponse {
    private Page<OutputInvoiceSmallDto> outputSmallInvoices;
}
