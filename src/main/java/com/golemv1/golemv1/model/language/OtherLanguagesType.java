package com.golemv1.golemv1.model.language;

public enum OtherLanguagesType {
    CZECH,
    GERMAN,
    SLOVAK,
    SPANISH
}
