package com.golemv1.golemv1.model.language;

import com.golemv1.golemv1.entities.language.Sentence;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GeneralAccountingTrainResponse {
    private Boolean trained;
    private Sentence sentenceSaved;
}
