package com.golemv1.golemv1.model.language;

import com.golemv1.golemv1.entities.language.AccRule;
import lombok.Data;

import java.util.List;

@Data
public class GeneralAccountingResponse {
    private Boolean decided = false;
    private String inputSentence;
    private String question;
    private List<String> variants;
    private AccRule rule;
}
