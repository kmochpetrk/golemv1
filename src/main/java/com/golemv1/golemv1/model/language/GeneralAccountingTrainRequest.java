package com.golemv1.golemv1.model.language;

import com.golemv1.golemv1.entities.GlWholeWriteType;
import com.golemv1.golemv1.entities.language.AccRule;
import lombok.Data;

@Data
public class GeneralAccountingTrainRequest {
    private String sentence;
    private AccRule rule;
    private GlWholeWriteType glWholeWriteType;
}
