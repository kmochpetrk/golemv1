package com.golemv1.golemv1.model.language;

import lombok.Data;

@Data
public class GeneralAccountingRequest {
    private String sentence;
    private String glWholeWriteType;
}
