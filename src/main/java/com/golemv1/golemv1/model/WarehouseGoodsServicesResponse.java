package com.golemv1.golemv1.model;

import com.golemv1.golemv1.entities.GlAccount;
import com.golemv1.golemv1.entities.WarehouseGoodsServices;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
@AllArgsConstructor
public class WarehouseGoodsServicesResponse {
    private Page<WarehouseGoodsServices> warehouseGoodsServices;
}
