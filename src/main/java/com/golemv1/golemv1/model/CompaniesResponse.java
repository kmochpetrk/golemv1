package com.golemv1.golemv1.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class CompaniesResponse {
    private List<String> companies;
}
