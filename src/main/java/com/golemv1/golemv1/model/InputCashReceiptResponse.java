package com.golemv1.golemv1.model;

import com.golemv1.golemv1.entities.InputCashReceipt;
import com.golemv1.golemv1.entities.InputInvoice;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
@AllArgsConstructor
public class InputCashReceiptResponse {
    private Page<InputCashReceipt> inputCashReceipts;
}
