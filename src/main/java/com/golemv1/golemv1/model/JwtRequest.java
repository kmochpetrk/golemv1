package com.golemv1.golemv1.model;

import lombok.Data;

@Data
public class JwtRequest {
    private String username;
    private String password;
    private String accUnit;
}
