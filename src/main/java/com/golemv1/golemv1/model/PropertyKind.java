package com.golemv1.golemv1.model;

public enum PropertyKind {
    TANGIBLE,
    INTANGIBLE
}
