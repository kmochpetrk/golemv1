package com.golemv1.golemv1.model;

import com.golemv1.golemv1.entities.GoodsInputReceipt;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
@AllArgsConstructor
public class GoodsInputReceiptResponse {
    private Page<GoodsInputReceipt> goodsInputReceipts;
}
