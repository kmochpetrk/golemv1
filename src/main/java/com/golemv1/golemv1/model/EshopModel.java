package com.golemv1.golemv1.model;

import lombok.Data;

import java.util.List;

@Data
public class EshopModel {
    private Long goodsServiceId;
    private String goodsServiceNumber;
    private String name;
    private List<EshopItem> items;

}
