package com.golemv1.golemv1.model;

import com.golemv1.golemv1.entities.OutputCashReceipt;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
@AllArgsConstructor
public class OutputCashReceiptResponse {
    private Page<OutputCashReceipt> outputCashReceipts;
}
