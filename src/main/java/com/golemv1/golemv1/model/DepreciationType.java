package com.golemv1.golemv1.model;

public enum DepreciationType {
    REGULAR,
    ACCELERATED
}
