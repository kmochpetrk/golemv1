package com.golemv1.golemv1.model;

import com.golemv1.golemv1.dtos.InputInvoiceSmallDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
@AllArgsConstructor
public class InputInvoicesSmallResponse {
    private Page<InputInvoiceSmallDto> inputSmallInvoices;
}
