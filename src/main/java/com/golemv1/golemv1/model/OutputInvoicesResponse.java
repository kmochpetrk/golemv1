package com.golemv1.golemv1.model;

import com.golemv1.golemv1.entities.GlAccount;
import com.golemv1.golemv1.entities.OutputInvoice;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
@AllArgsConstructor
public class OutputInvoicesResponse {
    private Page<OutputInvoice> outputInvoices;
}
