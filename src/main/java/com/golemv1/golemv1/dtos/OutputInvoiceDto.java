package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@Data
public class OutputInvoiceDto {
    private Long id;
    private List<GlWriteDto> glWrites;
    private List<WarehouseGoodsDto> warehouseGoodsDtos;
    private List<Long> GORIds;
    private LocalDate issueDate;
    private LocalDate payDate;
    private String documentNumber;
    private Long crmId;
    private String paymentType;
    private String transportType;
}
