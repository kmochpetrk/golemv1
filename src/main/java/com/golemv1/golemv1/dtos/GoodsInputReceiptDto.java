package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class GoodsInputReceiptDto {
    private Long id;
    private List<GlWriteDto> glWrites;
    private List<WarehouseGoodsDto> warehouseGoodsDtos;
    private Long crmId;
    private String documentNumber;
}
