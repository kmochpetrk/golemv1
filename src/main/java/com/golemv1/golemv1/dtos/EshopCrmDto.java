package com.golemv1.golemv1.dtos;

import com.golemv1.golemv1.daos.CrmType;
import com.golemv1.golemv1.entities.Address;
import com.golemv1.golemv1.entities.ContactType;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@AllArgsConstructor
@Data
public class EshopCrmDto {
    private String ico;
    private String dic;

    private String name;
    private String phone;


    private String email;

    private Address primaryAddress;
}
