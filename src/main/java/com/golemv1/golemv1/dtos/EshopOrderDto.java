package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class EshopOrderDto {
    private EshopGoodsService eshopGoodsService;
    private EshopOrderItemDto item;
}
