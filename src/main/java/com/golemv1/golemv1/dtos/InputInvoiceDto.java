package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
public class InputInvoiceDto {
    private Long id;
    private List<GlWriteDto> glWrites;
    private List<WarehouseGoodsDto> warehouseGoodsDtos;
    private Set<Long> gorIds = new HashSet<>();
    private LocalDate issueDate;
    private LocalDate payDate;
    private String documentNumber;
    private Long crmId;
    private String variableSymbol;
    private String paymentType;
    private String transportType;
    private String bankAccount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<GlWriteDto> getGlWrites() {
        return glWrites;
    }

    public void setGlWrites(List<GlWriteDto> glWrites) {
        this.glWrites = glWrites;
    }

    public List<WarehouseGoodsDto> getWarehouseGoodsDtos() {
        return warehouseGoodsDtos;
    }

    public void setWarehouseGoodsDtos(List<WarehouseGoodsDto> warehouseGoodsDtos) {
        this.warehouseGoodsDtos = warehouseGoodsDtos;
    }

    public Set<Long> getGorIds() {
        return gorIds;
    }

    public void setGorIds(Set<Long> gorIds) {
        this.gorIds = gorIds;
    }

    public LocalDate getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(LocalDate issueDate) {
        this.issueDate = issueDate;
    }

    public LocalDate getPayDate() {
        return payDate;
    }

    public void setPayDate(LocalDate payDate) {
        this.payDate = payDate;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Long getCrmId() {
        return crmId;
    }

    public void setCrmId(Long crmId) {
        this.crmId = crmId;
    }

    public String getVariableSymbol() {
        return variableSymbol;
    }

    public void setVariableSymbol(String variableSymbol) {
        this.variableSymbol = variableSymbol;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }
}
