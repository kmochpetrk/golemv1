package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@Data
public class BookingDto {
    private Long id;
    private List<GlWriteDto> glWrites;
    private List<WarehouseGoodsDto> warehouseGoodsDtos;
    private LocalDate issueDate;
    private LocalDate payDate;
    private LocalDate vatDate;
    private String documentNumber;
    private String glWholeWriteType;
    private String description;
    private Long crmId;
    private Boolean cancelled;
    private String variableSymbol;
}
