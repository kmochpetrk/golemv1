package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class InternalOrderItemDto {
    private Long id;

    private String goodsServicesName;
    private String goodsServicesNumber;
    private Long goodsServicesId;

    private Integer orderedCount;
    private BigDecimal totalItemVat;
    private BigDecimal totalItemPrice;

    private String warehouseName;
    private Long warehouseId;
    private String warehouseAddress;

    private BigDecimal unitOutputPrice;

    private BigDecimal unitOutputPriceWithVat;

    private Integer vatPercent;
}
