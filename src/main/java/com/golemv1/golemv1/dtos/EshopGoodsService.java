package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class EshopGoodsService {
    private Long goodsServiceId;
    private String goodsServiceNumber;
    private String name;
}
