package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
public class BankStatementDto {
    private String variableSymbolFromInvoice;
    private String bankStatementNumber;
    private LocalDate issueDate;
    private Long invoiceId;
    private Long ctpId;
    private String documentNumber;
    private String variableSymbolInput;
    private String bankAccountInput;
    private BigDecimal amountInput;
    private List<GlWriteDto> glWrites;
    private String ourBankAccount;
    private boolean income;
}
