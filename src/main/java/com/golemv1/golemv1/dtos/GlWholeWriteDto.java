package com.golemv1.golemv1.dtos;

import com.golemv1.golemv1.entities.GlWholeWriteType;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.Set;

@Data
@AllArgsConstructor
public class GlWholeWriteDto {
    private Long id;

    private Set<GlWriteDto> glWrites;

    private GlWholeWriteType glWholeWriteType;

    private LocalDate accountingDate;

    private LocalDate taxDate;

    private String writtenBy;

    private String crm;

//    @OneToMany(mappedBy = "glWholeWrite", fetch = FetchType.EAGER)
//    //@Cascade(org.hibernate.annotations.CascadeType.ALL)
//    private Set<GoodsServiceItems> goodsServiceItems;

    private String documentNumber;

    private Boolean cancelled;

    private String descriptionOfCase;
}
