package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InputInvoiceMediumDto {
    private LocalDate issueDate;
    private String variableSymbol;
    private String crmName;
    private Long id;
    private String paymentType;
    private String transportType;

    private List<Long> GORIds;
    private LocalDate payDate;

    private BigDecimal total;
    private BigDecimal totalTax;

    private Long BsId;

    private List<WarehouseGoodsServicesDto> warehouseGoodsServicesDtos;
}
