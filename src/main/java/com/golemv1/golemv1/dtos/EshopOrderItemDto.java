package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@AllArgsConstructor
@Data
public class EshopOrderItemDto {
    private Integer orderedCount;
    private BigDecimal totalItemVat;
    private BigDecimal totalItemPrice;
    private Long warehouseId;
    private BigDecimal unitOutputPrice;
    private BigDecimal unitOutputPriceWithVat;
    private Integer vatPercent;
}
