package com.golemv1.golemv1.dtos;

import com.golemv1.golemv1.entities.Address;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Set;

@AllArgsConstructor
public class EshopOrderFullDto {

    private String paymentType;
    private String deliveryType;
    private BigDecimal totalVat;
    private BigDecimal totalPrice;
    private String oINumber;
    private String ourBankAccount;

    private String company;

    private Set<EshopOrderDto> orders;
    private EshopCrmDto crm;

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public BigDecimal getTotalVat() {
        return totalVat;
    }

    public void setTotalVat(BigDecimal totalVat) {
        this.totalVat = totalVat;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getoINumber() {
        return oINumber;
    }

    public void setoINumber(String oINumber) {
        this.oINumber = oINumber;
    }

    public String getOurBankAccount() {
        return ourBankAccount;
    }

    public void setOurBankAccount(String ourBankAccount) {
        this.ourBankAccount = ourBankAccount;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Set<EshopOrderDto> getOrders() {
        return orders;
    }

    public void setOrders(Set<EshopOrderDto> orders) {
        this.orders = orders;
    }

    public EshopCrmDto getCrm() {
        return crm;
    }

    public void setCrm(EshopCrmDto crm) {
        this.crm = crm;
    }
}
