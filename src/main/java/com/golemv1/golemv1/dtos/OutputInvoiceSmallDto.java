package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
public class OutputInvoiceSmallDto {
    private LocalDate issueDate;
    private String variableSymbol;
    private String crmName;
    private BigDecimal amount;
    private Long id;
}
