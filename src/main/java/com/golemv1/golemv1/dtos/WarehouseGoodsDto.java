package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class WarehouseGoodsDto {
    private Long count;
    private Long goodsServiceId;
    private BigDecimal unitPrice;
    private BigDecimal vatTotal;
    private BigDecimal priceTotal;
    private Long warehouseId;
}
