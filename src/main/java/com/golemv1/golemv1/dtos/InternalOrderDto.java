package com.golemv1.golemv1.dtos;

import com.golemv1.golemv1.entities.Address;
import com.golemv1.golemv1.entities.Crm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
public class InternalOrderDto {
    private Long id;

    private String paymentType;
    private String deliveryType;
    private BigDecimal totalVat;
    private BigDecimal totalPrice;
    private String oinumber;
    private String ourBankAccount;
    private String company;

    private Address address;

    private Set<InternalOrderItemDto> ordersItems;

    private String crmName;
    private String CrmIco;
    private Long crmId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public BigDecimal getTotalVat() {
        return totalVat;
    }

    public void setTotalVat(BigDecimal totalVat) {
        this.totalVat = totalVat;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getOinumber() {
        return oinumber;
    }

    public void setOinumber(String oinumber) {
        this.oinumber = oinumber;
    }

    public String getOurBankAccount() {
        return ourBankAccount;
    }

    public void setOurBankAccount(String ourBankAccount) {
        this.ourBankAccount = ourBankAccount;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<InternalOrderItemDto> getOrdersItems() {
        return ordersItems;
    }

    public void setOrdersItems(Set<InternalOrderItemDto> ordersItems) {
        this.ordersItems = ordersItems;
    }

    public String getCrmName() {
        return crmName;
    }

    public void setCrmName(String crmName) {
        this.crmName = crmName;
    }

    public String getCrmIco() {
        return CrmIco;
    }

    public void setCrmIco(String crmIco) {
        CrmIco = crmIco;
    }

    public Long getCrmId() {
        return crmId;
    }

    public void setCrmId(Long crmId) {
        this.crmId = crmId;
    }
}
