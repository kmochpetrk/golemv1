package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@Data
public class InputCashReceiptDto {
    private Long id;
    private String cashierTitle;
    private List<GlWriteDto> glWrites;
    private List<WarehouseGoodsDto> warehouseGoodsDtos;
    private LocalDate issueDate;
    private String documentNumber;
    private Set<Long> gorIds = new HashSet<>();
}
