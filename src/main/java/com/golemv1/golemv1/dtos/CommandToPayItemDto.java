package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommandToPayItemDto {

    private Long id;
    private String variableSymbol;
    private String messageForReceiver;
    private String otherBankAccount;
    private String specificSymbol;
    private String constantSymbol;
    private BigDecimal amount;
}
