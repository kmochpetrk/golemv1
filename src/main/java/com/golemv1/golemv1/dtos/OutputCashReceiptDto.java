package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@Data
public class OutputCashReceiptDto {
    private Long id;
    private String cashierTitle;
    private List<GlWriteDto> glWrites;
    private List<Long> GORIds;
    private List<WarehouseGoodsDto> warehouseGoodsDtos;
    private LocalDate issueDate;
    private String documentNumber;
}
