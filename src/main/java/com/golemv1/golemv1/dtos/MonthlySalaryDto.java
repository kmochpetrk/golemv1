package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
public class MonthlySalaryDto {
    private Long employeeId;
    private BigDecimal amount;
    private String period;

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
