package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;


@AllArgsConstructor
public class AmortizationRequestDto {
    private List<DepreciationItemDto> depreciations;
    private List<GlWriteDto> accounting;

    public List<DepreciationItemDto> getDepreciations() {
        return depreciations;
    }

    public void setDepreciations(List<DepreciationItemDto> depreciations) {
        this.depreciations = depreciations;
    }

    public List<GlWriteDto> getAccounting() {
        return accounting;
    }

    public void setAccounting(List<GlWriteDto> accounting) {
        this.accounting = accounting;
    }
}
