package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class AccountingStatementItemDto {
    private String acctNo;
    private BigDecimal debit;
    private BigDecimal credit;
}
