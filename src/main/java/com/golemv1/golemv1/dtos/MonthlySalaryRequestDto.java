package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class MonthlySalaryRequestDto {
    private List<MonthlySalaryDto> monthlySalaryDtos;
    private List<GlWriteDto> accounting;

    public List<MonthlySalaryDto> getMonthlySalaryDtos() {
        return monthlySalaryDtos;
    }

    public void setMonthlySalaryDtos(List<MonthlySalaryDto> monthlySalaryDtos) {
        this.monthlySalaryDtos = monthlySalaryDtos;
    }

    public List<GlWriteDto> getAccounting() {
        return accounting;
    }

    public void setAccounting(List<GlWriteDto> accounting) {
        this.accounting = accounting;
    }
}
