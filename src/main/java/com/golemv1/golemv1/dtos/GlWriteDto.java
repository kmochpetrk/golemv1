package com.golemv1.golemv1.dtos;

import com.golemv1.golemv1.entities.GlAccount;
import com.golemv1.golemv1.entities.GlAccountSideType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GlWriteDto {

    private BigDecimal amount;
    private Long glAccountId;
    private String acctNo;
    private GlAccountSideType glAccountSideType;
    private String analyticPart;

}
