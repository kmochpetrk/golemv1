package com.golemv1.golemv1.dtos;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class WarehouseGoodsServicesDto {
    private Long warehouseId;
    private String warehouseName;
    private Long goodsServicesId;
    private BigDecimal vatTotal;
    private BigDecimal amountTotal;
    private Long numberOfUnits;
    private String goodsServicesName;
}
