package com.golemv1.golemv1.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommandToPayDto {

    private Long id;
    private String documentNumber;
    private String ourBankAccount;
    private Set<InputInvoiceDto> inputInvoices;
    private Set<CommandToPayItemDto> commandToPayItems;
}
